#include <iostream>
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

using namespace std;


class StringEqualCheck {
	private :
		std::string strVal1; 
		std::string strVal2; 
		bool check();
		
	public :
		StringEqualCheck(std::string strVal1, std::string strVal2);
		bool result;
	
};

StringEqualCheck::StringEqualCheck(std::string strVal1, std::string strVal2) {
	this->strVal1 = strVal1;
	this->strVal2 = strVal2;
	this->result = check();
}

bool StringEqualCheck::check() {
	if(strVal1.length() != strVal2.length()) {
		return false;
	}
	
	std::string strVal1 = this->strVal1;
	std::string strVal2 = this->strVal2;
	
	int n1 = strVal1.length(); 
	char char_array1[n1 + 1];
	strcpy(char_array1, strVal1.c_str());
	
	int n2 = strVal2.length(); 
	char char_array2[n2 + 1];
	strcpy(char_array2, strVal2.c_str());
	
	for(int i = 0 ; i < n1 ; i++) {
		char a1 = char_array1[i];
		char a2 = char_array2[i];
		if(a1 != a2) {
			cout << "index : " << i << " ,字元: " << a1 << " != "  << a2 << endl;
			return false;
		}
	}
	return true;
} 

class StringUtil {
	public :
		int println(std::string message);
		bool equals(std::string strVal1, std::string strVal2);
		char* stringToCharArray(std::string strVal);
		int showCharArray(std::string label, char* arry);
		std::string lowerCase(std::string strVal);
		std::string upperCase(std::string strVal);
	private :
};

bool StringUtil::equals(std::string strVal1, std::string strVal2) {
	StringEqualCheck mStringEqualCheck(strVal1, strVal2);
	return mStringEqualCheck.result;
}

int StringUtil::println(std::string message) {
	cout << message << endl;
}

int StringUtil::showCharArray(std::string label, char* arry) {
	cout << label << " : ";
	int n = strlen(arry); 
	for(int ii = 0 ; ii < n; ii ++) {
		cout << ii << "=[" << arry[ii] << "],";
	}
	cout << endl;
	return 0;
}

char* StringUtil::stringToCharArray(std::string strVal2) {
	int n = strVal2.length(); 
	char char_array[n + 1] = {0};
	strcpy(char_array, strVal2.c_str());
	return char_array;
} 


std::string StringUtil::lowerCase(std::string strVal) {
	int n = strVal.length() + 1;
	char char_array[n];
	char char_array_ok[n];
	strcpy(char_array, strVal.c_str());
	for(int i = 0 ; i < n ; i ++) {
		char_array_ok[i] = tolower(char_array[i]);
	} 
	std::string str(char_array_ok);
	cout << "轉換結果" << str << endl; 
	return str;
}

std::string StringUtil::upperCase(std::string strVal) {
	int n = strVal.length() + 1;
	char char_array[n];
	char char_array_ok[n];
	strcpy(char_array, strVal.c_str());
	for(int i = 0 ; i < n ; i ++) {
		char_array_ok[i] = toupper(char_array[i]);
	} 
	std::string str(char_array_ok);
	cout << "轉換結果" << str << endl; 
	return str;
}
 
 



int main(){
	StringUtil myObj;
	
	bool testResult = myObj.equals("aaa", "aac");
	
	cout << testResult << endl;
	
	std::string resultstring1 = myObj.lowerCase("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
	
	std::string resultstring2 = myObj.upperCase(resultstring1);
		
	return 0;
}

