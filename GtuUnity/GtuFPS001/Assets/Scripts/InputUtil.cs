using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using UnityEngine.InputSystem;
using System.Reflection;
using System.Diagnostics;
using System;
using Log = LogLib.Log;

namespace MyInputUtil {
    public class InputUtil 
    {
        private static ArrayList keyCodeLst = new ArrayList();
        private static Dictionary<string,KeyCode> keyCodeMap = new Dictionary<string,KeyCode>();

        public static ArrayList getKeyCodeLst() {
            if(keyCodeLst.Count == 0) {
                Type myType = typeof(KeyCode);
                FieldInfo[] myField = myType.GetFields(BindingFlags.Public | BindingFlags.Static);
                for(int i = 0; i < myField.Length; i++){
                    Type t2 = myField[i].FieldType;
                    if(t2.Equals(myType)){
                        keyCodeLst.Add(myField[i].Name);
                        keyCodeMap.Add(myField[i].Name, (KeyCode)myField[i].GetValue(null));
                    }
                }
            }
            return keyCodeLst;
        }

        public static bool isKeyStringCorrect(string keyCodeString) {
            ArrayList keyCodeLst = getKeyCodeLst();
            if(keyCodeLst.Contains(keyCodeString)){
                return true;
            }
            string keyCodeString2 = keyCodeString.ToLower();
            for(int i = 0 ; i < keyCodeLst.Count ; i ++) {
                string compare1 = keyCodeLst[i].ToString();
                string compare2 = compare1.ToLower();
                //if(keyCodeString2.Contains(compare2) || compare2.Contains(keyCodeString2)) {
                if(compare2.Contains(keyCodeString2)) {
                    Console.WriteLine ("你的意思是按鍵 : " + compare1 + " ?");
                }
            }
            return false;
        }

        public static void showAllKey() {
            ArrayList keyCodeLst = getKeyCodeLst();
            for(int i = 0 ; i < keyCodeLst.Count ; i ++) {
                string keyCode = keyCodeLst[i].ToString();
                Console.WriteLine(i + " : " + keyCode);    
            }
        }

        /*
        * 把這段放在 void OnGUI()
        */
        public static void showPressKeyCode() {
            if (Event.current.isKey) {
                Console.WriteLine("你按了按鍵為[1Z] : " + Event.current.keyCode);
            }
            if(Input.anyKey || Input.anyKeyDown) {
                ArrayList keyCodeLst = getKeyCodeLst();
                bool findOk = false;
                for(int i = 0 ; i < keyCodeLst.Count ; i ++) {
                    string keyCode = keyCodeLst[i].ToString();
                    if("None".Equals(keyCode)) {
                        continue;
                    }
                    if (Event.current.Equals(Event.KeyboardEvent(keyCode))) {
                        Console.WriteLine("你按了按鍵為[1A] : " + keyCode);
                        findOk = true;
                        break;
                    }
                    if(keyCodeMap.ContainsKey(keyCode)) {
                        KeyCode k1 = keyCodeMap[keyCode];
                        try{
                            if (Input.GetKey(k1) || Input.GetKeyDown(k1) || Input.GetKeyUp(k1)) {
                                Console.WriteLine("你按了按鍵為[2] : " + keyCode);
                                findOk = true;
                                break;
                            }
                        }catch(System.ArgumentException e) {   
                        }
                    }
                    try{
                        if (Input.GetButton(keyCode) || Input.GetButtonDown(keyCode) || Input.GetButtonUp(keyCode)) {
                            Console.WriteLine("你按了按鍵為[3] : " + keyCode);
                            findOk = true;
                            break;
                        }
                    }catch(System.ArgumentException e) {   
                    }
                }
                if(!findOk) {
                    Console.WriteLine("找不到此按鍵 : " + Input.inputString);
                }
            }
        }

        /*
        * 把這段放在 void OnGUI()
        */
        public static bool isKeyPress2(string keyCodeString) {
            if(!isKeyStringCorrect(keyCodeString)) {
                Console.WriteLine ("輸入keyCode錯誤 : " + keyCodeString);
                throw new System.ArgumentException("輸入keyCode錯誤 : " + keyCodeString);
            }
            if (Event.current.Equals(Event.KeyboardEvent(keyCodeString))) {
                return true;
            }
            return false;
        }

        /*
        * 把這段放在 void Update()
        */
        public static bool isKeyPress(string keyCodeString, int type) {
            if(!isKeyStringCorrect(keyCodeString)) {
                Console.WriteLine ("輸入keyCode錯誤 : " + keyCodeString);
                throw new System.ArgumentException("輸入keyCode錯誤 : " + keyCodeString);
            }
            try{
                if (type == 1 && Input.GetButton(keyCodeString)) {
                    Console.WriteLine("你按了按鍵為[3] : " + keyCodeString);
                    return true;
                }
                if (type == 1 && Input.GetButtonDown(keyCodeString)) {
                    Console.WriteLine("你按了按鍵為[3] : " + keyCodeString);
                    return true;
                }
                if (type == 1 && Input.GetButtonUp(keyCodeString)) {
                    Console.WriteLine("你按了按鍵為[3] : " + keyCodeString);
                    return true;
                }
            }catch(System.ArgumentException e) {   
            }
            KeyCode k1 = keyCodeMap[keyCodeString];
            try{
                if (type == 1 && Input.GetKey(k1)) {
                    Console.WriteLine("你按了按鍵為[2] : " + keyCodeString);
                    return true;
                }
                if (type == 2 && Input.GetKeyDown(k1)) {
                    Console.WriteLine("你按了按鍵為[2] : " + keyCodeString);
                    return true;
                }
                if (type == 3 && Input.GetKeyUp(k1)) {
                    Console.WriteLine("你按了按鍵為[2] : " + keyCodeString);
                    return true;
                }
            }catch(System.ArgumentException e) {   
            }
            return false;
        }
    }
}