using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Log = LogLib.Log;
// using UnityEngine.InputSystem;
using InputUtil = MyInputUtil.InputUtil;

using System.Collections;
using System.Linq;


public class P2 : MonoBehaviour
{

    public float sensitivity = 1f;
    public float moveSensitivity = 10f;
    public GameObject player;
    public GameObject fullBody;

    Camera camera;
    Camera cameram;

    private static Dictionary<string,KeyCode> keyCodeMap = new Dictionary<string,KeyCode>();

    // Start is called before the first frame update
    void Start()
    {
        //Log.showAll(typeof(System.Reflection.MethodInfo));
        // Log.showAll(transform);
        // Log.showAll(typeof(Event));
        // Log.showAll(fullBody.transform.rotation);
        // InputUtil.showAllKey();


        camera = GameObject.Find("Camera").GetComponent<Camera>();
        cameram = Camera.main;

        Collider collider = player.GetComponent<Collider>();

        childrenAttach();
    }

    private void childrenAttach() {
        Transform transform1 = player.transform;
        for(int i = 0; i < transform1.childCount ; i++){
            Transform transform2 = transform1.GetChild(i);
            transform1 = transform2.parent;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnGUI() {
        // InputUtil.showPressKeyCode();
        UnityEngine.Quaternion quat = camera.transform.rotation;
            // Log.debug("x[" + quat[0]+"]");//x 垂直移動 0~1 0~-1 為180度 
            // Log.debug("y[" + quat[1]+ "]");//y 水平移動 0~1 0~-1 為180度
            // Log.debug("z[" + quat[2]+ "]");
            // Log.debug("w[" + quat[3]+ "]");

        if(InputUtil.isKeyPress("Space", 3)){
            Log.debug("===========================PRESSS SPACE");
            Log.debug("-----------------------" + Input.mousePosition);
            Log.debug("-----------------------" + Time.deltaTime);
        }
        if(InputUtil.isKeyPress("Alpha1", 3)) {
            cameram.enabled = true;
            camera.enabled = false;
        }       
        if(InputUtil.isKeyPress("Alpha2", 3)) {
            cameram.enabled = false;
            camera.enabled = true;
        }       
    }

    private void processMove() {
        bool isMove = false;
        Vector3 vec = new Vector3();
        if(InputUtil.isKeyPress("UpArrow", 1)) {
            vec+=new Vector3(0,0,1);
            isMove = true;
        }
        if(InputUtil.isKeyPress("DownArrow", 1)) {
            vec+=new Vector3(0,0,-1);
            isMove = true;
        }
        if(InputUtil.isKeyPress("LeftArrow", 1)) {
            vec+=new Vector3(-1,0,0);
            isMove = true;
        }
        if(InputUtil.isKeyPress("RightArrow", 1 )) {
            vec+=new Vector3(1,0,0);
            isMove = true;
        }
        if(isMove) {
            System.Single mup1 = Time.deltaTime * moveSensitivity;
            vec *= mup1;
            // player.transform.Translate(vec);
            fullBodyMove(vec);
        }
    }

    private void fullBodyMove(Vector3 vec) {
        UnityEngine.Quaternion rotation = player.transform.rotation;
        Transform transform1 = fullBody.transform;
        for(int i = 0; i < transform1.childCount ; i++){
            Transform transform2 = transform1.GetChild(i);
            transform2.rotation = rotation;
            transform2.Translate(vec);
        }
    }

    private void processMouseControl() {
        float rotateHorizontal = Input.GetAxis ("Mouse X");
        float rotateVertical = Input.GetAxis ("Mouse Y");
        player.transform.RotateAround (player.transform.position, -Vector3.up, rotateHorizontal * sensitivity); //use transform.Rotate(-transform.up * rotateHorizontal * sensitivity) instead if you dont want the camera to rotate around the player
        //player.transform.RotateAround (Vector3.zero, transform.right, rotateVertical * sensitivity); // again, use transform.Rotate(transform.right * rotateVertical * sensitivity) if you don't want the camera to rotate around the player
        player.transform.Rotate(transform.right * rotateVertical * sensitivity);
    }

    void FixedUpdate ()
    {
        processMove();
        processMouseControl();
    }
}
