package com.janna;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.janna")
@SpringBootApplication(exclude = {})
@ServletComponentScan("com.janna.servlet") 
public class SpringBpptApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBpptApplication.class, args);
    }
}