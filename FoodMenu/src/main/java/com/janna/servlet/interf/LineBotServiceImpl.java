package com.janna.servlet.interf;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineBotServiceImpl {

    public static class UserSimpleText {
        String destination;
        String replyToken;
        String userType;
        String userId;
        String messageId;
        String message;

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }

        public String getReplyToken() {
            return replyToken;
        }

        public void setReplyToken(String replyToken) {
            this.replyToken = replyToken;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getMessageId() {
            return messageId;
        }

        public void setMessageId(String messageId) {
            this.messageId = messageId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public UserSimpleText getUserText(JSONObject jsonObject) throws JSONException {
        // "destination": "U67077301674022f7bc7a5618344f6d54",
        // "events": [{
        // "mode": "active",
        // "replyToken": "ed876f740d4c401c91415cbf97999f5c",
        // "source": {
        // "type": "user",
        // "userId": "Ua6cc63377f739333b2baaaead0bc5a29"
        // },
        // "type": "message",
        // "message": {
        // "id": "14330075735325",
        // "text": "Ii",
        // "type": "text"
        // },
        // "timestamp": 1625304144391
        // }]

        JSONObject event = jsonObject.getJSONArray("events").getJSONObject(0);
        String destination = jsonObject.getString("destination");

        String replyToken = event.getString("replyToken");
        if (!"message".equals(event.getString("type"))) {
            return null;
        }

        String userType = event.getJSONObject("source").getString("type");
        String userId = event.getJSONObject("source").getString("userId");

        JSONObject messageObj = event.getJSONObject("message");
        String messageId = messageObj.getString("id");

        if (!messageObj.has("text")) {
            return null;
        }
        String message = messageObj.getString("text");

        UserSimpleText u = new UserSimpleText();
        u.destination = destination;
        u.message = message;
        u.messageId = messageId;
        u.replyToken = replyToken;
        u.userId = userId;
        u.userType = userType;
        return u;
    }

    public JSONObject sendText(String destination, String replyToken, String userId, String messageContent) throws JSONException {
        JSONObject root = new JSONObject();
        root.put("destination", destination);

        JSONArray events = new JSONArray();
        JSONObject event = new JSONObject();
        events.put(event);
        root.put("events", events);

        event.put("replyToken", replyToken);
        event.put("type", "message");
        event.put("mode", "active");
        event.put("timestamp", System.currentTimeMillis());

        JSONObject source = new JSONObject();
        source.put("type", "user");
        source.put("userId", userId);
        event.put("source", source);

        JSONObject message = new JSONObject();
        message.put("id", "333333");
        message.put("type", "text");
        message.put("text", messageContent);
        event.put("message", message);

        JSONArray emojis = new JSONArray();
        JSONObject mention = new JSONObject();
        event.put("emojis", emojis);
        event.put("mention", mention);
        return root;
    }

    private static final String CHANNEL_ACCESS_TOKEN = "/m2RRzqdZ55TkpvcmeZo3EnhqJbz4ApSgKWfO9kQLhM1guVNEsekUH4RvNVMZPv+nFy+hfKthWprNwNUnq/n3hJMg79oxIxjoNjJDrXHq5mjji4/zXIiflO4qYHQUYZSdA3zS1bER1lZUgTW69ysagdB04t89/1O/w1cDnyilFU=";

    public void reply(String replyToken, String messageContent) throws IOException, JSONException {
        String url = "https://api.line.me/v2/bot/message/reply";
        String content = "";

        JSONObject reqBody = new JSONObject();
        reqBody.put("replyToken", replyToken);
        JSONArray messages = new JSONArray();
        JSONObject message = new JSONObject();
        messages.put(message);
        reqBody.put("messages", messages);
        message.put("type", "text");
        message.put("text", messageContent);
        content = reqBody.toString(4);
        System.out.println(content);

        doPost(url, content, "utf8", new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                HttpURLConnection conn = (HttpURLConnection) e.getSource();
                conn.setRequestProperty("Authorization", "Bearer {" + CHANNEL_ACCESS_TOKEN + "}");
            }
        });
    }

    final static int MAX_SIZE = Integer.MAX_VALUE;

    public static String doPost(String urlStr, String postData, String encode, ActionListener mListener) throws IOException {
        StringBuffer response = new StringBuffer();
        URL url = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        InputStreamReader isr = null;
        OutputStream os = null;
        char[] buff = new char[4096];
        int size = 0;
        int r = 0;

        try {
            url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            if (conn == null)
                return "";
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json; charset=" + encode);
            conn.setRequestProperty("Content-Length", "" + postData.getBytes().length);
            if (mListener != null) {
                mListener.actionPerformed(new ActionEvent(conn, -1, ""));
            }

            os = conn.getOutputStream();
            OutputStreamWriter wr = new OutputStreamWriter(os);
            wr.write(postData);
            wr.flush();

            is = conn.getInputStream();
            isr = new InputStreamReader(is, encode);
            while ((r = isr.read(buff)) > 0) {
                response.append(buff, 0, r);
                size += r;
                if (size >= MAX_SIZE) {
                    break;
                }
            }
            return response.toString();

        } finally {
            safeClose(is, os);
        }
    }

    private static void safeClose(InputStream is, OutputStream os) {
        if (is != null) {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        if (os != null) {
            try {
                os.close();
            } catch (IOException e) {
            }
        }
    }
}
