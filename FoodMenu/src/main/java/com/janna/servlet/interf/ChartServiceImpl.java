package com.janna.servlet.interf;

import java.awt.Color;
import java.awt.Font;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.chart.ui.RectangleInsets;
import org.jfree.data.general.Dataset;
import org.jfree.data.time.Month;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

public class ChartServiceImpl implements ChartService {

    public String getChartTitle() {
        return "每月" + " " + "AFPY報表";
    }

    public Dataset getDataSet() throws Exception {
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        TimeSeries timeseries = new TimeSeries("Series1");
        timeseries.add(new Month(2, 2001), 181.8D);// 這裡用的是Month.class,同樣還有Day.class
                                                   // Year.class 等等
        timeseries.add(new Month(3, 2001), 167.3D);
        timeseries.add(new Month(4, 2001), 153.8D);
        timeseries.add(new Month(5, 2001), 167.6D);
        timeseries.add(new Month(6, 2001), 158.8D);
        timeseries.add(new Month(7, 2001), 148.3D);
        timeseries.add(new Month(8, 2001), 153.9D);
        timeseries.add(new Month(9, 2001), 142.7D);
        timeseries.add(new Month(10, 2001), 123.2D);
        timeseries.add(new Month(11, 2001), 131.8D);
        timeseries.add(new Month(12, 2001), 139.6D);
        timeseries.add(new Month(1, 2002), 142.9D);
        timeseries.add(new Month(2, 2002), 138.7D);
        timeseries.add(new Month(3, 2002), 137.3D);
        timeseries.add(new Month(4, 2002), 143.9D);
        timeseries.add(new Month(5, 2002), 139.8D);
        timeseries.add(new Month(6, 2002), 137D);
        timeseries.add(new Month(7, 2002), 132.8D);
        dataset.addSeries(timeseries);
        TimeSeries timeseries1 = new TimeSeries("Series2");
        timeseries1.add(new Month(2, 2001), 129.6D);
        timeseries1.add(new Month(3, 2001), 123.2D);
        timeseries1.add(new Month(4, 2001), 117.2D);
        timeseries1.add(new Month(5, 2001), 124.1D);
        timeseries1.add(new Month(6, 2001), 122.6D);
        timeseries1.add(new Month(7, 2001), 119.2D);
        timeseries1.add(new Month(8, 2001), 116.5D);
        timeseries1.add(new Month(9, 2001), 112.7D);
        timeseries1.add(new Month(10, 2001), 101.5D);
        timeseries1.add(new Month(11, 2001), 106.1D);
        timeseries1.add(new Month(12, 2001), 110.3D);
        timeseries1.add(new Month(1, 2002), 111.7D);
        timeseries1.add(new Month(2, 2002), 111D);
        timeseries1.add(new Month(3, 2002), 109.6D);
        timeseries1.add(new Month(4, 2002), 113.2D);
        timeseries1.add(new Month(5, 2002), 111.6D);
        timeseries1.add(new Month(6, 2002), 108.8D);
        timeseries1.add(new Month(7, 2002), 101.6D);
        dataset.addSeries(timeseries1);
        return dataset;
    }

    private void applyUsingChinese() {
        // 建立主題樣式
        StandardChartTheme standardChartTheme = new StandardChartTheme("CN");
        // 設定標題字型
        standardChartTheme.setExtraLargeFont(new Font("隸書", Font.BOLD, 20));
        // 設定圖例的字型
        standardChartTheme.setRegularFont(new Font("宋書", Font.PLAIN, 15));
        // 設定軸向的字型
        standardChartTheme.setLargeFont(new Font("宋書", Font.PLAIN, 15));
        // 應用主題樣式
        ChartFactory.setChartTheme(standardChartTheme);
    }

    public JFreeChart getJFreeChart(Dataset dataset, String chartTitle) throws Exception {
        // 應用中文
        applyUsingChinese();

        JFreeChart jfreechart = ChartFactory.createTimeSeriesChart(//
                "合法 &; General Unit Trust Prices", //
                "日期", //
                "暗示的話發神經提防",//
                (XYDataset)getDataSet(), //
                true, true, false);
        jfreechart.setBackgroundPaint(Color.white);
        TextTitle textTitle = jfreechart.getTitle();
        textTitle.setFont(new Font("宋體", Font.BOLD, 20));
        LegendTitle legend = jfreechart.getLegend();
        if (legend != null) {
            legend.setItemFont(new Font("宋體", Font.BOLD, 20));
        }
        XYPlot xyplot = (XYPlot) jfreechart.getPlot(); // 獲得 plot : XYPlot!!
        ValueAxis domainAxis = xyplot.getDomainAxis();
        domainAxis.setTickLabelFont(new Font("宋體", Font.BOLD, 20));// 設定x軸座標上的字型
        domainAxis.setLabelFont(new Font("宋體", Font.BOLD, 20));// 設定x軸座標上的標題的字型
        ValueAxis rangeAxis = xyplot.getRangeAxis();
        rangeAxis.setTickLabelFont(new Font("宋體", Font.BOLD, 20));// 設定y軸座標上的字型
        rangeAxis.setLabelFont(new Font("宋體", Font.BOLD, 20));// 設定y軸座標上的標題的字型
        xyplot.setBackgroundPaint(Color.lightGray);
        xyplot.setDomainGridlinePaint(Color.white);
        xyplot.setRangeGridlinePaint(Color.white);
        xyplot.setAxisOffset(new RectangleInsets(5D, 5D, 5D, 5D));
        xyplot.setDomainCrosshairVisible(true);
        xyplot.setRangeCrosshairVisible(true);
        // ChartFrame frame = new ChartFrame("折線圖 ", jfreechart, true);
        // frame.pack();
        // frame.setVisible(true);
        return jfreechart;
    }

    public float getQuality() {
        return 1;
    }

    public int getWidth() {
        return 1000;
    }

    public int getHeight() {
        return 600;
    }

    @Override
    public boolean hasData() {
        return true;
    }
}
