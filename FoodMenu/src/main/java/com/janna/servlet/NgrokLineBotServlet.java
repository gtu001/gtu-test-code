package com.janna.servlet;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.janna.dao.LineBotMessage;
import com.janna.dao.LineBotMessageDao;
import com.janna.servlet.interf.LineBotServiceImpl;
import com.janna.servlet.interf.LineBotServiceImpl.UserSimpleText;

@WebServlet("/NgrokLineBotServlet")
public class NgrokLineBotServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private Logger logger = Logger.getLogger(getClass());

    private static Map<String, String> MAPPING = new HashMap<String, String>();

    private LineBotServiceImpl mLineBotServiceImpl = new LineBotServiceImpl();
    private LineBotMessageDao mLineBotMessageDao = new LineBotMessageDao();

    public static DataSource getTestDataSource_LineBotSqlite() {
        BasicDataSource ds2 = new BasicDataSource();
        ds2.setUrl("jdbc:sqlite:/d:/my_tool/lineBot.db");
        ds2.setUsername("sa");
        ds2.setPassword("");
        ds2.setDriverClassName("org.sqlite.JDBC");
        return ds2;
    }

    private static Connection getConnection() {
        try {
            return getTestDataSource_LineBotSqlite().getConnection();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public NgrokLineBotServlet() {
        super();

        logger.info("------------------start");

        List<Map<String, Object>> lst = mLineBotMessageDao.findAll(this.getConnection());
        List<LineBotMessage> lst2 = new ArrayList<LineBotMessage>();
        for (Map<String, Object> map : lst) {
            LineBotMessage vo = new LineBotMessage();
            vo.toBean(map);
            lst2.add(vo);
        }
        for (LineBotMessage vo : lst2) {
            MAPPING.put(vo.getMessage(), vo.getReplyMessage());
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    public void updateLineBot(String message, String replyMessage) {
        List<Map<String, Object>> qlst = mLineBotMessageDao.queryByMessage(message, getConnection());
        LineBotMessage vo = new LineBotMessage();
        if (qlst.isEmpty()) {
            vo.setCreateDate(new java.sql.Date(System.currentTimeMillis()));
            vo.setMessage(message);
            vo.setReplyMessage(replyMessage);
            int result = mLineBotMessageDao.insert(vo.toMap(), getConnection(), true);
            System.out.println("insert : " + result);
        } else {
            vo.toBean(qlst.get(0));
            vo.setReplyMessage(replyMessage);
            vo.setUpdateDate(new java.sql.Date(System.currentTimeMillis()));
            Map<String, Object> valueMap = vo.toMap();
            int result = mLineBotMessageDao.update(valueMap, valueMap, getConnection(), true);
            System.out.println("update : " + result);
        }
    }

    private byte[] toByteArray(InputStream in) {
        try {
            byte[] arrayOfByte = new byte[4096];
            BufferedInputStream input = new BufferedInputStream(in);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int i;
            while ((i = input.read(arrayOfByte, 0, arrayOfByte.length)) != -1) {
                baos.write(arrayOfByte, 0, i);
            }
            baos.close();
            input.close();
            return baos.toByteArray();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("------------------NgrokServlet start");
        System.out.println("# parameter ------------------------------ start");
        for (Enumeration enu = request.getParameterNames(); enu.hasMoreElements();) {
            String key = (String) enu.nextElement();
            String[] values = request.getParameterValues(key);
            System.out.println("\t" + key + "\t" + Arrays.toString(values));
        }
        System.out.println("# parameter ------------------------------ end");
        System.out.println("# attribute ------------------------------ start");
        for (Enumeration enu = request.getAttributeNames(); enu.hasMoreElements();) {
            String key = (String) enu.nextElement();
            Object value = request.getAttribute(key);
            System.out.println("\t" + key + "\t" + value);
        }
        System.out.println("# attribute ------------------------------ end");
        System.out.println("# session ------------------------------ start");
        for (Enumeration enu = request.getSession().getAttributeNames(); enu.hasMoreElements();) {
            String key = (String) enu.nextElement();
            Object value = request.getSession().getAttribute(key);
            System.out.println("\t" + key + "\t" + value);
        }
        System.out.println("# session ------------------------------ end");
        System.out.println("# header ------------------------------ start");
        for (Enumeration enu = request.getHeaderNames(); enu.hasMoreElements();) {
            String key = (String) enu.nextElement();
            Object value = request.getHeader(key);
            System.out.println("\t" + key + "\t" + value);
        }
        System.out.println("# header ------------------------------ end");
        System.out.println("# allContent ------------------------------ start");

        byte[] arry2 = this.toByteArray(request.getInputStream());
        StringBuffer sb = new StringBuffer();

        sb.append(new String(arry2, "UTF8"));

        JSONObject jsonObject = null;
        try {
            if (sb.length() > 0) {
                jsonObject = new JSONObject(sb.toString());
                String prettyContent = jsonObject.toString(4);
                System.out.println(prettyContent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("# allContent ------------------------------ end");
        String rtnMessage = "ok";
        if (jsonObject != null) {
            try {
                UserSimpleText userSimpleText = mLineBotServiceImpl.getUserText(jsonObject);
                ReflectionToStringBuilder.toString(userSimpleText, ToStringStyle.MULTI_LINE_STYLE);

                String message = userSimpleText.getMessage();
                message = StringUtils.defaultString(message);

                for (String msgKey : MAPPING.keySet()) {
                    if (StringUtils.equalsIgnoreCase(msgKey, message)) {
                        String replyMessage = MAPPING.get(msgKey);
                        mLineBotServiceImpl.reply(userSimpleText.getReplyToken(), replyMessage);
                    }
                }

                if ("機器人會說啥".equals(message)) {
                    StringBuffer sb1 = new StringBuffer();
                    for (String msgKey : MAPPING.keySet()) {
                        String replyMessage = MAPPING.get(msgKey);
                        sb1.append(msgKey + "=" + replyMessage + "\r\n");
                    }
                    mLineBotServiceImpl.reply(userSimpleText.getReplyToken(), sb1.toString());
                }

                if (message.contains("=")) {
                    String[] arry = message.split("=", -1);
                    MAPPING.put(arry[0], arry[1]);
                    this.updateLineBot(arry[0], arry[1]);
                    String formatMessage = String.format("有人說\"%s\",我就回\"%s\",我懂我懂!", arry[0], arry[1]);
                    mLineBotServiceImpl.reply(userSimpleText.getReplyToken(), formatMessage);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        response.getWriter().write(rtnMessage);
        System.out.println("------------------NgrokServlet end");
    }
}
