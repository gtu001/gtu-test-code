package com.janna.config;

import org.apache.log4j.Logger;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
public class FilterAppender {

    private Logger logger = Logger.getLogger(getClass());

    @Bean
    public FilterRegistrationBean someFilterRegistration() {
        logger.info("------------addFilter");
        FilterRegistrationBean registration = new FilterRegistrationBean();
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        registration.setFilter(filter);
        registration.addUrlPatterns("/*");
        registration.setName("CharacterEncoding");
        registration.setOrder(1);
        return registration;
    }

}
