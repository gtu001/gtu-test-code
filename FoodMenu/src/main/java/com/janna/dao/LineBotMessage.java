package com.janna.dao;

import java.util.LinkedHashMap;
import java.util.Map;

public class LineBotMessage {
    // JAVA Mode
    // seq/INTEGER4/2147483647
    private java.lang.Integer seq;
    // message/TEXT12/2147483647
    private java.lang.String message;
    // reply_message/TEXT12/2147483647
    private java.lang.String replyMessage;
    // create_date/DATETIME91/2147483647
    private java.sql.Date createDate;
    // update_date/DATETIME91/2147483647
    private java.sql.Date updateDate;
    // Orign Mode

    public Map<String, Object> toMap() {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("SEQ", this.seq);
        map.put("MESSAGE", this.message);
        map.put("REPLY_MESSAGE", this.replyMessage);
        map.put("CREATE_DATE", this.createDate);
        map.put("UPDATE_DATE", this.updateDate);
        return map;
    }

    private static java.sql.Date longToDate(Long value) {
        if (value == null) {
            return null;
        }
        return new java.sql.Date(value);
    }

    public void toBean(Map<String, Object> map) {
        this.seq = (java.lang.Integer) map.get("SEQ");
        this.message = (java.lang.String) map.get("MESSAGE");
        this.replyMessage = (java.lang.String) map.get("REPLY_MESSAGE");
        this.createDate = (java.sql.Date) longToDate((Long) map.get("CREATE_DATE"));
        this.updateDate = (java.sql.Date) longToDate((Long) map.get("UPDATE_DATE"));
    }

    public java.lang.Integer getSeq() {
        return seq;
    }

    public void setSeq(java.lang.Integer seq) {
        this.seq = seq;
    }

    public java.lang.String getMessage() {
        return message;
    }

    public void setMessage(java.lang.String message) {
        this.message = message;
    }

    public java.lang.String getReplyMessage() {
        return replyMessage;
    }

    public void setReplyMessage(java.lang.String replyMessage) {
        this.replyMessage = replyMessage;
    }

    public java.sql.Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(java.sql.Date createDate) {
        this.createDate = createDate;
    }

    public java.sql.Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(java.sql.Date updateDate) {
        this.updateDate = updateDate;
    }
}
