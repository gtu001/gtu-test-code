package com.janna.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LineBotMessageDao {
    public interface LineBotMessage_schema {
        public static final String SEQ = "SEQ";
        public static final String MESSAGE = "MESSAGE";
        public static final String REPLY_MESSAGE = "REPLY_MESSAGE";
        public static final String CREATE_DATE = "CREATE_DATE";
        public static final String UPDATE_DATE = "UPDATE_DATE";
    }

    public int insert(Map<String, Object> valMap, Connection conn, boolean commit) {
        PreparedStatement stmt = null;
        try {
            conn.setAutoCommit(false);
            String sql = "INSERT INTO line_bot_message  (MESSAGE,REPLY_MESSAGE,CREATE_DATE,UPDATE_DATE) VALUES ( ?,?,?,?) ";
            stmt = conn.prepareStatement(sql);

            // stmt.setObject(1, valMap.get("SEQ"));
            stmt.setObject(1, valMap.get("MESSAGE"));
            stmt.setObject(2, valMap.get("REPLY_MESSAGE"));
            stmt.setObject(3, valMap.get("CREATE_DATE"));
            stmt.setObject(4, valMap.get("UPDATE_DATE"));

            int result = stmt.executeUpdate();
            System.out.println("insert result : " + result);
            if (commit) {
                conn.commit();
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            throw new RuntimeException(e);
        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (commit) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int delete(Map<String, Object> pkMap, Connection conn, boolean commit) {
        PreparedStatement stmt = null;
        try {
            conn.setAutoCommit(false);
            String sql = "DELETE FROM line_bot_message WHERE  SEQ=?";
            stmt = conn.prepareStatement(sql);

            stmt.setObject(1, pkMap.get("SEQ"));

            int result = stmt.executeUpdate();
            System.out.println("delete result : " + result);
            if (commit) {
                conn.commit();
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            throw new RuntimeException(e);
        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (commit) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int update(Map<String, Object> valMap, Map<String, Object> pkMap, Connection conn, boolean commit) {
        PreparedStatement stmt = null;
        try {
            conn.setAutoCommit(false);
            String sql = "UPDATE line_bot_message SET SEQ=?,MESSAGE=?,REPLY_MESSAGE=?,CREATE_DATE=?,UPDATE_DATE=? WHERE  SEQ=?";
            stmt = conn.prepareStatement(sql);

            stmt.setObject(1, valMap.get("SEQ"));
            stmt.setObject(2, valMap.get("MESSAGE"));
            stmt.setObject(3, valMap.get("REPLY_MESSAGE"));
            stmt.setObject(4, valMap.get("CREATE_DATE"));
            stmt.setObject(5, valMap.get("UPDATE_DATE"));
            stmt.setObject(6, pkMap.get("SEQ"));

            int result = stmt.executeUpdate();
            System.out.println("update result : " + result);
            if (commit) {
                conn.commit();
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            throw new RuntimeException(e);
        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (commit) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Map<String, Object>> queryByMessage(String message, Connection conn) {
        List<Map<String, Object>> rsList = new ArrayList<Map<String, Object>>();
        java.sql.ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            String sql = "SELECT * FROM line_bot_message WHERE  MESSAGE = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setObject(1, message);

            rs = stmt.executeQuery();
            java.sql.ResultSetMetaData mdata = rs.getMetaData();
            int cols = mdata.getColumnCount();
            List<String> colList = new ArrayList<String>();
            for (int i = 1; i <= cols; i++) {
                colList.add(mdata.getColumnName(i).toUpperCase());
            }

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                for (String col : colList) {
                    map.put(col, rs.getObject(col));
                }
                rsList.add(map);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rsList;
    }

    public List<Map<String, Object>> findAll(Connection conn) {
        List<Map<String, Object>> rsList = new ArrayList<Map<String, Object>>();
        java.sql.ResultSet rs = null;
        PreparedStatement stmt = null;
        try {
            String sql = "SELECT * FROM line_bot_message WHERE  1=1 ";
            stmt = conn.prepareStatement(sql);

            rs = stmt.executeQuery();
            java.sql.ResultSetMetaData mdata = rs.getMetaData();
            int cols = mdata.getColumnCount();
            List<String> colList = new ArrayList<String>();
            for (int i = 1; i <= cols; i++) {
                colList.add(mdata.getColumnName(i).toUpperCase());
            }

            while (rs.next()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                for (String col : colList) {
                    map.put(col, rs.getObject(col));
                }
                rsList.add(map);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return rsList;
    }

    public boolean truncate(Connection conn) {
        PreparedStatement stmt = null;
        try {
            conn.setAutoCommit(false);
            String sql = "truncate table line_bot_message ";
            stmt = conn.prepareStatement(sql);

            boolean result = stmt.execute();
            System.out.println("truncate result : " + result);
            // conn.commit();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                conn.rollback();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            throw new RuntimeException(e);
        } finally {
            try {
                stmt.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
