<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%-- <script src="${pageContext.request.contextPath}/js/jquery-2.1.1.js"></script> --%>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script
	src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
<link
	href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/themes/ui-lightness/jquery-ui.css"
	rel="stylesheet" type="text/css" />

<script
	src="${pageContext.request.contextPath}/js/jquery.fileDownload.js"></script>
<script type="text/javascript">
	function loadJFreeChart001() {
		var img = $("<img />").attr('src', '/FoodMenu/JFreeChartServlet1')
		    .on('load', function() {
		        if (!this.complete || typeof this.naturalWidth == "undefined" || this.naturalWidth == 0) {
		            alert('broken image!');
		        } else {
		            $("#jfreeChart001Div").append(img);
		        }
		    });
	}
	
	function loadJFreeChart002() {
		var image = document.querySelector("#image001");
		var downloadingImage = new Image();
		downloadingImage.onload = function(){
		    image.src = this.src;   
		};
		downloadingImage.src = '/FoodMenu/JFreeChartServlet1';
	}
	
	function test(){
		loadJFreeChart001();
	}
</script>
<title></title>
</head>
<body>
	<div id="jfreeChart001Div">
	</div>
	<input type="button" value="test" onclick="test()" />
</body>
</html>