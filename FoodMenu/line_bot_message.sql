                         
    create table IF NOT EXISTS  line_bot_message (
        seq INTEGER PRIMARY KEY AUTOINCREMENT,
        message text  not null,
        reply_message text not null,
        create_date  datetime default current_timestamp,
        update_date  datetime 
    )
    