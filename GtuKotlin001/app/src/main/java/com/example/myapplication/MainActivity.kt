package com.example.myapplication

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telephony.SmsManager
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.common.ClipboardHelper
import com.example.myapplication.common.LayoutViewHelper


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)

        val layout: LinearLayout? = LayoutViewHelper.createContentView_simple(this)

        //初始Btn狀態紐
        val btn4 = Button(this)
        btn4.setText("開啟QRCode掃描")
        layout?.addView(btn4)
        btn4.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                doScanQRCode();
            }
        });

        this.doScanQRCode()
    }

    private fun doScanQRCode() {
        try {
            val intent = Intent("com.google.zxing.client.android.SCAN")
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE") // "PRODUCT_MODE for bar codes
            startActivityForResult(intent, 0)
        } catch (e: Exception) {
            val marketUri = Uri.parse("market://details?id=com.google.zxing.client.android")
            val marketIntent = Intent(Intent.ACTION_VIEW, marketUri)
            startActivity(marketIntent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                ClipboardHelper.ClipboardHelper.copyToClipboard(this, "");
                val contents = data?.getStringExtra("SCAN_RESULT")
                goToSms("1922", contents)
            }
            if (resultCode == RESULT_CANCELED) {
                //handle cancel
            }
        } else {
        }
    }

    private fun goToSms(phoneNumber: String, message: String?) {
        val sendIntent = Intent(Intent.ACTION_VIEW)
        sendIntent.data = Uri.parse("sms:$phoneNumber") // This ensures only SMS apps respond
        sendIntent.putExtra("sms_body", message)
        if (sendIntent.resolveActivity(packageManager) != null) {
            startActivity(sendIntent)
        }
    }

    fun sendSMS(phoneNo: String?, msg: String?) {
        try {
            val smsManager = SmsManager.getDefault()
            smsManager.sendTextMessage(phoneNo, null, msg, null, null)
            Toast.makeText(
                applicationContext, "Message Sent",
                Toast.LENGTH_LONG
            ).show()
        } catch (ex: Exception) {
            Toast.makeText(
                applicationContext, ex.message.toString(),
                Toast.LENGTH_LONG
            ).show()
            ex.printStackTrace()
        }
    }

    private fun initServices() {}

    override fun onDestroy() {
        super.onDestroy()
    }
}