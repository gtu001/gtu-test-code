package com.example.myapplication.common

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView


class LayoutViewHelper {
    companion object {
        fun setViewHeight(view: View, height: Int) {
            val lp: ViewGroup.LayoutParams = view.getLayoutParams()
            lp.height = height
            view.setLayoutParams(lp)
        }

        fun createContentView_simple(activity: Activity): LinearLayout? {
            val layout = LinearLayout(activity)
            layout.isFocusable = true
            layout.isFocusableInTouchMode = true
            activity.setContentView(layout)
            layout.orientation = LinearLayout.VERTICAL
            return layout
        }

        fun createContentView(activity: Activity): LinearLayout? {
            val layout = LinearLayout(activity)
            layout.isFocusable = true
            layout.isFocusableInTouchMode = true
            val scroll = ScrollView(activity)
            activity.setContentView(scroll)
            layout.orientation = LinearLayout.VERTICAL
            scroll.addView(
                layout,  //
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,  // 設定與螢幕同寬
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            ) // 高度隨內容而定
            return layout
        }
    }
}