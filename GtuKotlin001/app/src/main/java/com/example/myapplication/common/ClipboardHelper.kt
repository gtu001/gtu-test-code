package com.example.myapplication.common

import android.content.ClipData
import android.content.Context
import android.os.Build
import android.text.ClipboardManager
import android.util.Log

class ClipboardHelper {
    object ClipboardHelper {
        fun copyToClipboard(context: Context, str: String?) {
            val sdk = Build.VERSION.SDK_INT
            if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                val clipboard =
                    context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                clipboard.text = str
                Log.e("version", "1 version")
            } else {
                val clipboard =
                    context.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
                val clip = ClipData.newPlainText("text label", str)
                clipboard.setPrimaryClip(clip)
                Log.e("version", "2 version")
            }
        }

        fun copyFromClipboard(context: Context): String {
            val sdk = Build.VERSION.SDK_INT
            return if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                val clipboard =
                    context.getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
                val sb = StringBuffer()
                for (i in 0 until clipboard.primaryClip!!.itemCount) {
                    sb.append(clipboard.primaryClip!!.getItemAt(i).text)
                }
                sb.toString()
            } else {
                val clipboard =
                    context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val text = clipboard.text
                text?.toString() ?: ""
            }
        }
    }
}