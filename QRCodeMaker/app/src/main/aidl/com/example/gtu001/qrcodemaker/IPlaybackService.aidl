// IPlaybackService.aidl
package com.example.gtu001.qrcodemaker;


interface IPlaybackService {

    void stop();

    void play();

    void pause();

    boolean openFile(String path);

    long getDuration();

    long getPosition();

    void seek(long pos);

    boolean isPlaying();

//--------------------------------------custom
    void setReplayMode(String currentName, int currentPosition, inout List<String> nameLst, inout List<String> pathLst, boolean isRandom);

    String getCurrentName();

    void previousOrNext(int song);

    Map getCurrentBean();

    void stopAll();
}
