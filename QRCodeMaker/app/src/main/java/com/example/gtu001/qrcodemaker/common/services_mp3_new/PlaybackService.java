
package com.example.gtu001.qrcodemaker.common.services_mp3_new;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.PixelFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.example.gtu001.qrcodemaker.Mp3Bean;
import com.example.gtu001.qrcodemaker.R;
import com.example.gtu001.qrcodemaker.common.Log;
import com.example.gtu001.qrcodemaker.util.RandomUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlaybackService extends Service {

    private static final String TAG = PlaybackService.class.getSimpleName();

    public static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0";

    private final IBinder mNinder = new ServiceStub(this);

    private MediaPlayback mediaPlayback;

    private AudioManager mAudioManager;

    private MediaPlaybackHandler mPlayerHandler;

    private AudioManager.OnAudioFocusChangeListener mAudioFocusListener = new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(final int focusChange) {
            mPlayerHandler.obtainMessage(FOCUS_CHANGE, focusChange, 0).sendToTarget();
        }
    };

    private static final int FOCUS_CHANGE = 10;
    private static final int FADE_DOWN = 11;
    private static final int FADE_UP = 12;
    private static final int SERVER_DIED = 13;

    private boolean mPausedByTransientLossOfFocus = false;
    private boolean mIsSupposedToBePlaying = false;


    private List<Mp3Bean> totalLst;//所有音樂
    private boolean isRandomSong; //隨機音樂
    private String currentPath;//當前音樂路徑;


    WindowManager.LayoutParams wmParams;

    /**
     * 初始化root view 配置
     */
    private void initWmParams() {
        wmParams = new WindowManager.LayoutParams();
        // 设置window type
        // 會出錯 改用右邊這兩個 LayoutParams.TYPE_TOAST or TYPE_APPLICATION_PANEL

        //wmParams.type = LayoutParams.TYPE_PHONE; //LayoutParams.TYPE_PHONE;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            wmParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            wmParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        }

        // 设置图片格式，效果为背景透明
        wmParams.format = PixelFormat.RGBA_8888;

        // 设置浮动窗口不可聚焦（实现操作除浮动窗口外的其他可见窗口的操作）
        // wmParams.flags = LayoutParams.FLAG_NOT_TOUCH_MODAL |
        // LayoutParams.FLAG_NOT_FOCUSABLE;

        // FLAG_NOT_TOUCH_MODAL：
        // 当窗口可以获得焦点（没有设置 FLAG_NOT_FOCUSALBE
        // 选项）时，仍然将窗口范围之外的点设备事件（鼠标、触摸屏）发送给后面的窗口处理。否则它将独占所有的点设备事件，而不管它们是不是发生在窗口范围内。
        //
        // FLAG_WATCH_OUTSIDE_TOUCH：
        // 如果你设置了FLAG_NOT_TOUCH_MODAL，那么当触屏事件发生在窗口之外事，可以通过设置此标志接收到一个MotionEvent.ACTION_OUTSIDE事件。
        // 注意，你不会收到完整的down/move/up事件，只有第一次down事件时可以收到ACTION_OUTSIDE。

        wmParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;

        // 调整悬浮窗显示的停靠位置为左侧置顶 (x,y左標起點位置)
        wmParams.gravity = Gravity.LEFT | Gravity.TOP;

        // 以屏幕左上角为原点，设置x、y初始值，相对于gravity
//        MagnifierPosEnum.RIGHT_TOP.apply(mWindowManager, wmParams);

        // 设置悬浮窗口长宽数据
        wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
    }

    private void createDot() {
        initWmParams();

        WindowManager mWindowManager = (WindowManager) getApplication().getSystemService(getApplication().WINDOW_SERVICE);

        LayoutInflater inflater = LayoutInflater.from(getApplication());
        RelativeLayout contentView = (RelativeLayout) inflater.inflate(R.layout.activity_float_english_info, null);

        mWindowManager.addView(contentView, wmParams);
    }


    @Override
    public void onCreate() {
        super.onCreate();

        createDot();

        // Start up the thread running the service. Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block. We also make it
        // background priority so CPU-intensive work will not disrupt the UI.
        final HandlerThread thread = new HandlerThread("MediaPlaybackHandler",
                android.os.Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Initialize the handlers
        mPlayerHandler = new MediaPlaybackHandler(this, thread.getLooper());

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        Log.i("PlabackService", "MediaPlayback class instantiated");
        mediaPlayback = new MediaPlayback(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                onMyCompletion();
            }
        });
        mediaPlayback.setHandler(mPlayerHandler);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.w("PlaybackService", "Destroying service");
        mediaPlayback.release();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mNinder;
    }


    /**
     * Provides an interface for dealing with playback of audio files
     */
    private class MediaPlayback implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnInfoListener
            , MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnBufferingUpdateListener {

        MediaPlayer.OnCompletionListener mOnCompletionListener;

        private MediaPlayback(MediaPlayer.OnCompletionListener mOnCompletionListener) {
            this.mOnCompletionListener = mOnCompletionListener;
        }

        private MediaPlayer mPlayer = new MediaPlayer();

        private Handler mHandler;

        private float mVolume;

        private boolean mIsPlayerInitialised = false;

        int resumePosition = 0;

        public void setHandler(Handler handler) {
            mHandler = handler;
        }

        public void start() {
            mPlayer.start();
        }

        public void stop() {
            Log.v(TAG, "[MediaPlayback] mPlayer = " + mPlayer);
            mPlayer.reset();
            mIsSupposedToBePlaying = false;
        }

        /**
         * You CANNOT use this player after calling release
         */
        public void release() {
            stop();
            mPlayer.release();
        }

        public void pause() {
            mPlayer.pause();
        }

        public long getDuration() {
            if (mPlayer != null && mIsPlayerInitialised) {
                return mPlayer.getDuration();
            }
            return -1;
        }

        public long getPosition() {
            if (mPlayer != null && mIsPlayerInitialised) {
                return mPlayer.getCurrentPosition();
            }
            return 0;
        }

        public void seek(long whereTo) {
            mPlayer.seekTo((int) whereTo);
        }

        public void setVolume(float volume) {
            mPlayer.setVolume(volume, volume);
            this.mVolume = volume;
        }

        public float getVolume() {
            return mVolume;
        }

        public void setDataSource(String path) {
            if (mPlayer == null) {
                mPlayer = new MediaPlayer();
            }
            mIsPlayerInitialised = setDataSource(mPlayer, path);
        }

        private boolean setDataSource(MediaPlayer mediaPlayer, String path) {
            try {
                Uri uri = Uri.parse(path);
                Map<String, String> headerMap = new HashMap<String, String>();
                headerMap.put("User-Agent", DEFAULT_USER_AGENT);
                mediaPlayer.reset();
                mediaPlayer.setOnPreparedListener(this);
                mediaPlayer.setOnCompletionListener(this);
                mediaPlayer.setOnErrorListener(this);
                mediaPlayer.setOnBufferingUpdateListener(this);
                mediaPlayer.setOnSeekCompleteListener(this);
                mediaPlayer.setOnInfoListener(this);

                mediaPlayer.setDataSource(getApplicationContext(), uri, headerMap);
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.prepare();
            } catch (IOException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
                return false;
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
                return false;
            } catch (SecurityException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
                return false;
            }
            return true;
        }

        private boolean setDataSource_ORIGN(MediaPlayer mediaPlayer, String path) {
            try {
                AssetFileDescriptor afd = getAssets().openFd(path);
                mediaPlayer.reset();
                mediaPlayer.setOnPreparedListener(null);
                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.prepare();
                mediaPlayer.setOnCompletionListener(this);
                mediaPlayer.setOnErrorListener(this);
            } catch (IOException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
                return false;
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
                return false;
            } catch (SecurityException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
                return false;
            }
            return true;
        }

        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            mPlayer.release();
            mPlayer = null;
            if (mOnCompletionListener != null) {
                mOnCompletionListener.onCompletion(mediaPlayer);
            }
        }

        //--------------------------------------------new ↓↓↓↓↓↓

        private void playMedia() {
            if (!mPlayer.isPlaying()) {
                mPlayer.start();
            }
        }

        private void stopMedia() {
            if (mPlayer == null) {
                return;
            }
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
            }
        }

        private void pauseMedia() {
            if (mPlayer.isPlaying()) {
                mPlayer.pause();
                resumePosition = mPlayer.getCurrentPosition();
            }
        }

        private void resumeMedia() {
            if (!mPlayer.isPlaying()) {
                mPlayer.seekTo(resumePosition);
                mPlayer.start();
            }
        }

        //--------------------------------------------new ↑↑↑↑↑↑

        @Override
        public void onPrepared(MediaPlayer mp) {
            //Invoked when the media source is ready for playback.
            playMedia();
        }

        @Override
        public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
            Log.line("PlaybackService", "Error: " + what);
            switch (what) {
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED: {

                    mIsPlayerInitialised = false;
                    mPlayer.release();
                    mPlayer = new MediaPlayer();

                    mHandler.sendMessageDelayed(mHandler.obtainMessage(SERVER_DIED), 2000);
                    return true;
                }
                case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:
                    Log.line("MediaPlayer Error", "MEDIA ERROR NOT VALID FOR PROGRESSIVE PLAYBACK " + extra);
                    break;
                case MediaPlayer.MEDIA_ERROR_UNKNOWN:
                    Log.line("MediaPlayer Error", "MEDIA ERROR UNKNOWN " + extra);
                    break;
            }
            return false;
        }

        @Override
        public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
            Log.line(TAG, "onBufferingUpdate");
        }

        @Override
        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i1) {
            Log.line(TAG, "onInfo");
            return false;
        }

        @Override
        public void onSeekComplete(MediaPlayer mediaPlayer) {
            Log.line(TAG, "onSeekComplete");
        }
    }

    public void stop() {
        Log.v(TAG, "[stop] " + "mediaPlayback = " + mediaPlayback);
        //Todo: Fade down nicely
        synchronized (this) {
            if (mediaPlayback != null && mediaPlayback.mIsPlayerInitialised) {
                mediaPlayback.stop();
            }
        }
    }

    public void play() {
        synchronized (this) {
            mAudioManager.requestAudioFocus(mAudioFocusListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
            if (mediaPlayback.mIsPlayerInitialised) {
                mediaPlayback.start();
                mPlayerHandler.removeMessages(FADE_DOWN);
                mPlayerHandler.sendEmptyMessage(FADE_UP);
                mIsSupposedToBePlaying = true;
            }
        }
    }

    public void pause() {

        //Todo: Fade down nicely

        synchronized (this) {
            mPlayerHandler.removeMessages(FADE_UP);
            if (mIsSupposedToBePlaying) {
                mediaPlayback.pause();
                mIsSupposedToBePlaying = false;
                mPausedByTransientLossOfFocus = false;
            }
        }
    }

    public boolean openFile(String path) {
        synchronized (this) {
            if (path == null) {
                return false;
            }
            //↓↓↓↓↓↓↓
            currentPath = path;
            //↑↑↑↑↑↑↑
            mediaPlayback.setDataSource(path);
            if (mediaPlayback.mIsPlayerInitialised) {
                return true;
            }
            stop();
            return false;
        }
    }

    public long getDuration() {
        synchronized (this) {
            if (mediaPlayback != null && mediaPlayback.mIsPlayerInitialised) {
                return mediaPlayback.getDuration();
            }
        }
        return -1;
    }

    public long getPosition() {
        synchronized (this) {
            if (mediaPlayback != null && mediaPlayback.mIsPlayerInitialised) {
                return mediaPlayback.getPosition();
            }
        }
        return 0;
    }

    public void seek(long pos) {
        synchronized (this) {
            if (mediaPlayback != null && mediaPlayback.mIsPlayerInitialised) {
                if (pos < 0) {
                    pos = 0;
                } else if (pos > mediaPlayback.getDuration()) {
                    pos = mediaPlayback.getDuration();
                }
                mediaPlayback.seek(pos);
            }
        }
    }

    private final class MediaPlaybackHandler extends android.os.Handler {

        private final WeakReference<PlaybackService> _service;
        private float mCurrentVolume = 0.8f;

        public MediaPlaybackHandler(final PlaybackService service, final Looper looper) {
            super(looper);
            _service = new WeakReference<>(service);
            MediaPlayback mediaPlayback = _service.get().mediaPlayback;
            if (mediaPlayback != null) {
                mCurrentVolume = mediaPlayback.getVolume();
            }
        }

        @Override
        public void handleMessage(Message msg) {
            final PlaybackService service = _service.get();
            if (service == null) {
                return;
            }

            switch (msg.what) {
                case FOCUS_CHANGE: {
                    switch (msg.arg1) {
                        case FADE_DOWN:
                            mCurrentVolume -= .05f;
                            if (mCurrentVolume > .2f) {
                                sendEmptyMessageDelayed(FADE_DOWN, 10);
                            } else {
                                mCurrentVolume = .2f;
                            }
                            service.mediaPlayback.setVolume(mCurrentVolume);
                            break;
                        case FADE_UP:
                            //Todo: Only fade up to original volume
                            mCurrentVolume += .01f;
                            if (mCurrentVolume < 1.0f) {
                                sendEmptyMessageDelayed(FADE_UP, 10);
                            } else {
                                mCurrentVolume = 1.0f;
                            }
                            service.mediaPlayback.setVolume(mCurrentVolume);
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
                            Log.line(TAG, "-----" + "AudioFocusLoss");
                            if (service.mIsSupposedToBePlaying) {
                                service.mPausedByTransientLossOfFocus = false;
                            }
                            service.pause();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                            Log.line(TAG, "-----" + "AudioFocusLoss_TransientCanDuck");
                            removeMessages(FADE_UP);
                            sendEmptyMessage(FADE_DOWN);
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                            Log.line(TAG, "-----" + "AudioFocusLoss_Transient");
                            if (service.mIsSupposedToBePlaying) {
                                service.mPausedByTransientLossOfFocus = true;
                            }
                            service.pause();
                            break;
                        case AudioManager.AUDIOFOCUS_GAIN:
                            if (!service.mIsSupposedToBePlaying
                                    && service.mPausedByTransientLossOfFocus) {
                                service.mPausedByTransientLossOfFocus = false;
                                mCurrentVolume = 0f;
                                service.mediaPlayback.setVolume(mCurrentVolume);
                                service.play(); // also queues a fade-in
                            } else {
                                removeMessages(FADE_DOWN);
                                sendEmptyMessage(FADE_UP);
                            }
                            break;
                        case SERVER_DIED:
                            Log.line(TAG, "service died!!");
                            break;
                        default:
                            //App.log("PlaybackService: " +  "Unknown audio focus change code");
                    }
                    break;
                }
            }
        }
    }

    //----------------------------------------------------------------------------------------------------

    public void setReplayMode(String currentName, int currentPosition, List<String> nameLst, List<String> pathLst, boolean isRandom) {
        try {
            List<Mp3Bean> lst = new ArrayList<Mp3Bean>();
            for (int ii = 0; ii < nameLst.size(); ii++) {
                String name = (String) nameLst.get(ii);
                String url = (String) pathLst.get(ii);
                Mp3Bean b = new Mp3Bean();
                b.setName(name);
                b.setUrl(url);
                Log.v(TAG, "Add TotalLst : " + ReflectionToStringBuilder.toString(b));
                lst.add(b);
            }
            totalLst = lst;
            isRandomSong = isRandom;
        } catch (Exception ex) {
            Log.e(TAG, "ERR : " + ex.getMessage(), ex);
            throw new RuntimeException("setReplayMode ERR : " + ex.getMessage(), ex);
        }
    }

    public void onMyCompletion() {
        previousOrNext(1);
    }

    public String getCurrentName() {
        if (totalLst != null && !totalLst.isEmpty()) {
            for (Mp3Bean bean : totalLst) {
                if (StringUtils.equals(bean.getUrl(), currentPath)) {
                    return bean.getName();
                }
            }
        }
        return "NA";
    }

    public void previousOrNext(int song) {
        if (totalLst == null || totalLst.isEmpty()) {
            return;
        }
        if (song > 0) {
            song = 1;
        } else {
            song = -1;
        }
        String nextPath = "";
        for (int ii = 0; ii < totalLst.size(); ii++) {
            Mp3Bean bean = totalLst.get(ii);
            if (StringUtils.equals(bean.getUrl(), currentPath)) {
                if (song == 1) {
                    if (ii + 1 >= totalLst.size()) {
                        nextPath = totalLst.get(0).getUrl();
                    } else {
                        nextPath = totalLst.get(ii + 1).getUrl();
                    }
                    break;
                } else {
                    if (ii - 1 >= 0) {
                        nextPath = totalLst.get(ii - 1).getUrl();
                    } else {
                        int latest = totalLst.size() - 1;
                        nextPath = totalLst.get(latest).getUrl();
                    }
                    break;
                }
            }
        }
        if (StringUtils.isNotBlank(nextPath)) {
            openFile(nextPath);
            play();
        }
    }

    public void stopAll() {
        totalLst = null;
        stop();
    }

    public Map getCurrentBean() {
        Map<String, String> map = new HashMap<>();
        if (totalLst == null || totalLst.isEmpty()) {
            return map;
        }
        for (int ii = 0; ii < totalLst.size(); ii++) {
            Mp3Bean bean = totalLst.get(ii);
            if (StringUtils.equals(bean.getUrl(), currentPath)) {
                map.put("name", bean.getName());
                map.put("path", bean.getUrl());
                break;
            }
        }
        return map;
    }

    private static class ServiceStub extends com.example.gtu001.qrcodemaker.IPlaybackService.Stub {

        private final WeakReference<PlaybackService> _service;

        private ServiceStub(final PlaybackService service) {
            _service = new WeakReference<>(service);
        }

        @Override
        public void stop() throws RemoteException {
            _service.get().stop();
        }

        @Override
        public void play() throws RemoteException {
            _service.get().play();
        }

        @Override
        public void pause() throws RemoteException {
            _service.get().pause();
        }

        @Override
        public boolean openFile(String path) throws RemoteException {
            return _service.get().openFile(path);
        }

        @Override
        public long getDuration() throws RemoteException {
            return _service.get().getDuration();
        }


        @Override
        public long getPosition() throws RemoteException {
            return _service.get().getPosition();
        }

        @Override
        public void seek(long pos) throws RemoteException {
            _service.get().seek(pos);
        }

        @Override
        public boolean isPlaying() {
            return _service.get().mIsSupposedToBePlaying;
        }

        @Override
        public void setReplayMode(String currentName, int currentPosition, List<String> nameLst, List<String> pathLst, boolean isRandom) throws RemoteException {
            _service.get().setReplayMode(currentName, currentPosition, nameLst, pathLst, isRandom);
        }

        @Override
        public String getCurrentName() {
            return _service.get().getCurrentName();
        }

        @Override
        public void previousOrNext(int song) {
            _service.get().previousOrNext(song);
        }

        @Override
        public Map getCurrentBean() {
            return _service.get().getCurrentBean();
        }

        @Override
        public void stopAll() throws RemoteException {
            _service.get().stopAll();
        }
    }
}