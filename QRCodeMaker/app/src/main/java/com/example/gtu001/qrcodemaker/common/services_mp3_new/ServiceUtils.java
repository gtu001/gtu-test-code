package com.example.gtu001.qrcodemaker.common.services_mp3_new;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.widget.Toast;

import com.example.gtu001.qrcodemaker.IPlaybackService;
import com.example.gtu001.qrcodemaker.common.Log;
import com.example.gtu001.qrcodemaker.common.ServiceUtil;

import java.util.WeakHashMap;

public class ServiceUtils {

    private static final String TAG = "ServiceUtils";

    public static IPlaybackService sService = null;

    private static final WeakHashMap<Context, ServiceBinder> sConnectionMap;

    static {
        sConnectionMap = new WeakHashMap<>();
    }


    public static void bindToService2(Context context, ServiceConnection callback) {
//        boolean isRunning = ServiceUtil.isServiceRunning(context, PlaybackService.class);
//        if (!isRunning) {
        Intent intent = new Intent(context, PlaybackService.class);
        context.startService(intent);
//        }
        context.bindService(intent, callback, Context.BIND_AUTO_CREATE);
        Toast.makeText(context, "---init ok 1", Toast.LENGTH_SHORT).show();
    }

    /**
     * @param context  The {@link Context} to use
     * @param callback The {@link ServiceConnection} to use
     * @return The new instance of {@link ServiceToken}
     */
    public static ServiceToken bindToService(final Context context, final ServiceConnection callback) {
        Log.v(TAG, "-------------------------1");
        Activity realActivity = ((Activity) context).getParent();
        if (realActivity == null) {
            realActivity = (Activity) context;
            Log.v(TAG, "------------------------2-" + realActivity);
        }
        Log.v(TAG, "------------------------2-" + realActivity);

        final ContextWrapper contextWrapper = new ContextWrapper(realActivity);

        ComponentName componentName = contextWrapper.startService(new Intent(contextWrapper,
                PlaybackService.class));

        Log.v(TAG, "------------------------3-" + componentName);

        final ServiceBinder binder = new ServiceBinder(callback);
        Log.v(TAG, "-------------------------4");

        boolean bindResult = contextWrapper.bindService(
                new Intent().setClass(contextWrapper, PlaybackService.class),
                binder, Context.BIND_AUTO_CREATE);//0

        Log.v(TAG, "------------------------25-" + bindResult);

        Log.v(TAG, "=================================================");
        Log.v(TAG, "=                                               =");
        Log.v(TAG, "= sService : " + sService + " =");
        Log.v(TAG, "=                                               =");
        Log.v(TAG, "=================================================");

        if (bindResult) {
            Log.v(TAG, "-------------------------6");
            sConnectionMap.put(contextWrapper, binder);
            return new ServiceToken(contextWrapper);
        }
        return null;
    }

    /**
     * @param token The {@link ServiceToken} to unbind from
     */
    public static void unbindFromService(final ServiceToken token) {
        if (token == null) {
            return;
        }
        final ContextWrapper mContextWrapper = token.mWrappedContext;
        final ServiceBinder mBinder = sConnectionMap.remove(mContextWrapper);
        if (mBinder == null) {
            return;
        }
        mContextWrapper.unbindService(mBinder);
        if (sConnectionMap.isEmpty()) {
            sService = null;
        }
    }

    public static final class ServiceBinder implements ServiceConnection {
        private final ServiceConnection mCallback;

        public ServiceBinder(final ServiceConnection callback) {
            mCallback = callback;
        }

        @Override
        public void onServiceConnected(final ComponentName className, final IBinder service) {
            Log.v(TAG, "[onServiceConnected]-start");
            sService = IPlaybackService.Stub.asInterface(service);
            Log.v(TAG, "[onServiceConnected]-sService = " + sService);
            if (mCallback != null) {
                mCallback.onServiceConnected(className, service);
            }
            Log.v(TAG, "[onServiceConnected]-end");
        }

        @Override
        public void onServiceDisconnected(final ComponentName className) {
            Log.v(TAG, "[onServiceDisconnected]-start");
            if (mCallback != null) {
                mCallback.onServiceDisconnected(className);
            }
            Log.v(TAG, "[onServiceDisconnected]-sService = " + sService);
            sService = null;
            Log.v(TAG, "[onServiceDisconnected]-sService = " + sService);
            Log.v(TAG, "[onServiceDisconnected]-end");
        }
    }

    public static final class ServiceToken {
        public ContextWrapper mWrappedContext;

        /**
         * Constructor of <code>ServiceToken</code>
         *
         * @param context The {@link ContextWrapper} to use
         */
        public ServiceToken(final ContextWrapper context) {
            mWrappedContext = context;
        }
    }

}