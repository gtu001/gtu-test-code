
package com.example.gtu001.qrcodemaker;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gtu001.qrcodemaker.common.FloatViewChecker;
import com.example.gtu001.qrcodemaker.common.LayoutViewHelper;
import com.example.gtu001.qrcodemaker.common.Log;
import com.example.gtu001.qrcodemaker.common.PermissionUtil;
import com.example.gtu001.qrcodemaker.common.ProcessHandler;
import com.example.gtu001.qrcodemaker.common.mp3.Mp3InitListViewHandler;
import com.example.gtu001.qrcodemaker.common.services_mp3_new.PlaybackUtils;
import com.example.gtu001.qrcodemaker.common.services_mp3_new.ServiceUtils;
import com.example.gtu001.qrcodemaker.custom_dialog.UrlPlayerDialog_bg;
import com.example.gtu001.qrcodemaker.custom_dialog.UrlPlayerDialog_bgV2;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Mp3PlayerActivity2 extends Activity implements ServiceConnection {

    private static final String TAG = Mp3PlayerActivity2.class.getSimpleName();

    private ServiceUtils.ServiceToken mToken;

    private Mp3InitListViewHandler initListViewHandler;
    private ListView listView;

    public static final int FLOATVIEW_REQUESTCODE = 1234;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = LayoutViewHelper.createContentView_simple(this);

        //Let the device know that we're playing music, and volume controls should affect the music stream
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        //Bind to our Music Service. This is where the magic happens!
//        mToken = ServiceUtils.bindToService(this, this);

        ServiceUtils.bindToService2(this, new ServiceUtils.ServiceBinder(this));


        //初始Btn狀態紐
        Button btn1 = new Button(this);
        btn1.setText("選擇檔案");
        layout.addView(btn1);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                Mp3PlayerActivity2.TaskInfo.LOAD_CONTENT_FROM_FILE_RANDOM.onOptionsItemSelected(Mp3PlayerActivity2.this, intent, bundle);
            }
        });

        //初始Btn狀態紐
        Button btn4 = new Button(this);
        btn4.setText("選擇目錄");
        layout.addView(btn4);
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                Mp3PlayerActivity2.TaskInfo.LOAD_CONTENT_FROM_DIR.onOptionsItemSelected(Mp3PlayerActivity2.this, intent, bundle);
            }
        });

        final Button btn2 = new Button(this);
        btn2.setText("開啟撥放器");
        layout.addView(btn2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UrlPlayerDialog_bgV2 dialog = new UrlPlayerDialog_bgV2(Mp3PlayerActivity2.this);
                Dialog dialogV2 = dialog.build();
                dialogV2.show();
            }
        });

        //初始Btn狀態紐
        Button btn21 = new Button(this);
        btn21.setText("開啟現在播放");
        layout.addView(btn21);
        btn21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Mp3Bean mp3Bean = initListViewHandler.getCurrentPlayBean(Mp3PlayerActivity2.this);
                initListViewHandler.restoreTotalUrlList(Mp3PlayerActivity2.this);
                if (mp3Bean == null || Mp3InitListViewHandler.FAIL_FILE_NAME.equals(mp3Bean.getName())) {
                    Toast.makeText(Mp3PlayerActivity2.this, "無法回復原來撥放內容!", Toast.LENGTH_SHORT).show();
                    return;
                }

                List<String> nameLst = new ArrayList<>();
                List<String> pathLst = new ArrayList<>();
                List<Mp3Bean> totalLst = initListViewHandler.getTotalUrlList();
                for (Mp3Bean m : totalLst) {
                    nameLst.add(m.getName());
                    pathLst.add(m.getUrl());
                }

                PlaybackUtils.setReplayMode(mp3Bean.getName(), mp3Bean.getLastPositionInt(), nameLst, pathLst, false);

                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "====================================================================");
                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "name = " + mp3Bean.getName());
                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "videoUrl = " + mp3Bean.getUrl());
                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "lastPosition = " + (mp3Bean != null ? mp3Bean.getLastPosition() : "null"));
                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "====================================================================");

                if (PlaybackUtils.isPlaying()) {
                    PlaybackUtils.stop();
                }
                PlaybackUtils.openFile(mp3Bean.getUrl());
                PlaybackUtils.play();
                PlaybackUtils.seek(mp3Bean.getLastPositionInt());

                btn2.performClick();
            }
        });

        //初始Btn狀態紐
        Button btn5 = new Button(this);
        btn5.setText("停掉APP");
        layout.addView(btn5);
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcessHandler.killProcessByPackage2(Mp3PlayerActivity2.this, "com.example.gtu001.qrcodemaker", false);
                ServiceUtils.bindToService2(Mp3PlayerActivity2.this, new ServiceUtils.ServiceBinder(Mp3PlayerActivity2.this));
            }
        });

        //初始listView
        listView = new ListView(this);
        layout.addView(listView, //
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
//        LayoutViewHelper.setViewHeight(listView, 1000);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map<String, Object> item = (Map<String, Object>) listView.getAdapter().getItem(position);
                final Mp3InitListViewHandler.FileItem item2 = (Mp3InitListViewHandler.FileItem) item.get("item");

                List<String> nameLst = new ArrayList<>();
                List<String> pathLst = new ArrayList<>();
                List<Mp3Bean> totalLst = initListViewHandler.getTotalUrlList();
                for (Mp3Bean m : totalLst) {
                    nameLst.add(m.getName());
                    pathLst.add(m.getUrl());
                }

                PlaybackUtils.setReplayMode(item2.name, 0, nameLst, pathLst, false);
                initListViewHandler.saveTotalUrlList(Mp3PlayerActivity2.this);

                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "====================================================================");
                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "name = " + item2.name);
                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "videoUrl = " + item2.videoUrl);
                com.example.gtu001.qrcodemaker.common.Log.v(TAG, "====================================================================");

                if (PlaybackUtils.isPlaying()) {
                    PlaybackUtils.stop();
                }
                PlaybackUtils.openFile(item2.videoUrl);
                PlaybackUtils.play();

                btn2.performClick();
            }
        });

        initServices();

        PermissionUtil.verifyPermissions(this, new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.PROCESS_OUTGOING_CALLS,
        }, 9999);

        //申請開啟懸浮視窗權限
        FloatViewChecker.applyPermission(this, FLOATVIEW_REQUESTCODE);

    }

    private void initServices() {
        initListViewHandler = new Mp3InitListViewHandler(listView, this);
        initListViewHandler.initListView();
//        initListViewHandler.add(new File("/storage/1D0E-2671/Music/軒轅劍/軒轅劍參 - 背景音樂第48首 - 神州大地(墨者奔馳).mp3"));
    }


    @Override
    public void onDestroy() {
        //The activity is being destroyed. We should unbind from our service
        if (mToken != null) {
            ServiceUtils.unbindFromService(mToken);
            mToken = null;
        }
        super.onDestroy();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Toast.makeText(this, "PlaybackService START + " + ServiceUtils.sService, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        Toast.makeText(this, "PlaybackService STOP + " + ServiceUtils.sService, Toast.LENGTH_SHORT).show();
    }

    //    ==========================================================================================
    static int REQUEST_CODE = 5566;
    static int MENU_FIRST = Menu.FIRST;
    private static final String FILE_TYPE_PTN = "(avi|rmvb|rm|mp4|mp3|m4a|flv|3gp|flac|webm|wmv|mkv|m4s)";
    private static final String[] EXTENSION_DIR = new String[]{"/storage/1D0E-2671/Android/data/com.ghisler.android.TotalCommander/My Documents/"};

    enum TaskInfo {
        LOAD_CONTENT_FROM_FILE_RANDOM("讀取檔案", MENU_FIRST++, REQUEST_CODE++, FileFindActivity.class) {
            protected void onActivityResult(Mp3PlayerActivity2 activity, Intent intent, Bundle bundle) {
                File file = FileFindActivity.FileFindActivityStarter.getFile(intent);
                activity.initListViewHandler.add(file);
            }

            protected void onOptionsItemSelected(Mp3PlayerActivity2 activity, Intent intent, Bundle bundle) {
                bundle.putString(FileFindActivity.FILE_PATTERN_KEY, FILE_TYPE_PTN);
                if (BuildConfig.DEBUG) {
                    bundle.putStringArray(FileFindActivity.FILE_START_DIRS, EXTENSION_DIR);
                }
                super.onOptionsItemSelected(activity, intent, bundle);
            }
        }, //
        LOAD_CONTENT_FROM_DIR("讀取目錄", MENU_FIRST++, REQUEST_CODE++, FileFindMultiActivity.class) {
            protected void onActivityResult(Mp3PlayerActivity2 activity, Intent intent, Bundle bundle) {
                try {
                    List<File> fileLst = null;
                    Object val = FileFindMultiActivity.FileFindActivityStarter.getFile(intent);
                    if (val == null) {
                        val = FileFindMultiActivity.FileFindActivityStarter.getFiles(intent);
                    }
                    if (val instanceof ArrayList) {
                        fileLst = (List<File>) val;
                    } else if (val instanceof File) {
                        fileLst = new ArrayList<>();
                        fileLst.add((File) val);
                    }
                    for (File f : fileLst) {
                        if (f.isDirectory()) {
                            if (f.listFiles() != null) {
                                for (File subFile : f.listFiles()) {
                                    activity.initListViewHandler.add(subFile);
                                }
                            }
                        } else {
                            activity.initListViewHandler.add(f);
                        }
                    }
                } catch (Exception ex) {
                    Log.v(TAG, "取得檔案失敗 : " + ex.getMessage(), ex);
                }
            }

            protected void onOptionsItemSelected(Mp3PlayerActivity2 activity, Intent intent, Bundle bundle) {
                bundle.putString(FileFindMultiActivity.FILE_PATTERN_KEY, FILE_TYPE_PTN);
                if (BuildConfig.DEBUG) {
                    bundle.putStringArray(FileFindMultiActivity.FILE_START_DIRS, EXTENSION_DIR);
                }
                super.onOptionsItemSelected(activity, intent, bundle);
            }
        }, //
        ;

        final String title;
        final int option;
        final int requestCode;
        final Class<?> clz;
        final boolean debugOnly;

        TaskInfo(String title, int option, int requestCode, Class<?> clz) {
            this(title, option, requestCode, clz, false);
        }

        TaskInfo(String title, int option, int requestCode, Class<?> clz, boolean debugOnly) {
            this.title = title;
            this.option = option;
            this.requestCode = requestCode;
            this.clz = clz;
            this.debugOnly = debugOnly;
        }

        protected void onOptionsItemSelected(Mp3PlayerActivity2 activity, Intent intent, Bundle bundle) {
            intent.setClass(activity, clz);
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, requestCode);
        }

        protected void onActivityResult(Mp3PlayerActivity2 activity, Intent intent, Bundle bundle) {
            Log.v(TAG, "onActivityResult TODO!! = " + this.name());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v(TAG, "# onOptionsItemSelected");
        super.onOptionsItemSelected(item);
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        for (Mp3PlayerActivity2.TaskInfo task : Mp3PlayerActivity2.TaskInfo.values()) {
            if (item.getItemId() == task.option) {
                task.onOptionsItemSelected(this, intent, bundle);
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v(TAG, "# onActivityResult");
        Bundle bundle_ = new Bundle();
        if (data != null) {
            bundle_ = data.getExtras();
        }
        final Bundle bundle = bundle_;
        Log.v(TAG, "requestCode = " + requestCode);
        Log.v(TAG, "resultCode = " + resultCode);
        for (Mp3PlayerActivity2.TaskInfo t : Mp3PlayerActivity2.TaskInfo.values()) {
            if (requestCode == t.requestCode) {
                switch (resultCode) {
                    case RESULT_OK:
                        t.onActivityResult(this, data, bundle);
                        break;
                }
                break;
            }
        }

        if (requestCode == FLOATVIEW_REQUESTCODE) {
            if (!FloatViewChecker.isPermissionOk(this)) {
                //申請開啟懸浮視窗權限
                FloatViewChecker.applyPermission(this, FLOATVIEW_REQUESTCODE);
            } else {
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.v(TAG, "# onCreateOptionsMenu");
        for (Mp3PlayerActivity2.TaskInfo e : Mp3PlayerActivity2.TaskInfo.values()) {

            //純測試
            if (!BuildConfig.DEBUG && e.debugOnly == true) {
                continue;
            }

            menu.add(0, e.option, 0, e.title);
        }
        return true;
    }
}
