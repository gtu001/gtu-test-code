package com.example.gtu001.qrcodemaker.common.mp3;

import android.content.Context;
import android.os.Handler;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gtu001.qrcodemaker.common.Log;
import com.example.gtu001.qrcodemaker.common.services_mp3_new.PlaybackUtils;
import com.example.gtu001.qrcodemaker.custom_dialog.UrlPlayerDialog_bg;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

public class PercentProgressBarTimerV2 {
    private static final String TAG = PercentProgressBarTimerV2.class.getSimpleName();

    public AtomicBoolean isPercentProgressTrigger = new AtomicBoolean(false);
    private Context context;
    private Timer timer;
    private SeekBar progressBar;
    private TextView textTimer;
    private TextView textContent;
    private TextView textTitle;
    private final Handler handler = new Handler();

    private boolean isClose = false;

    public void close() {
        isClose = true;
    }

    public SeekBar getProgressBar() {
        return progressBar;
    }

    public TextView getTextTimer() {
        return textTimer;
    }

    public TextView getTextContent() {
        return textContent;
    }

    public TextView getTextTitle() {
        return textTitle;
    }

    public PercentProgressBarTimerV2() {
    }

    public PercentProgressBarTimerV2(final Context context, SeekBar progressBar, TextView textTimer, TextView textTitle, TextView textContent) {
        this.progressBar = progressBar;
        this.textTimer = textTimer;
        this.textContent = textContent;
        this.textTitle = textTitle;
        this.context = context;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    if (isClose && timer != null) {
                        timer.cancel();
                        return;
                    }

                    final int percent = PlaybackUtils.getProgressPercent();
                    final String timeTxt = PlaybackUtils.getProgressTime();
                    final Map<String, String> map = PlaybackUtils.getCurrentBean();
                    Log.v(TAG, "[PercentProgressBarTimerV2] == percent\t" + percent);
                    Log.v(TAG, "[PercentProgressBarTimerV2] == timeTxt\t" + timeTxt);
                    Log.v(TAG, "[PercentProgressBarTimerV2] == map\t" + map);

                    if(map.isEmpty()) {
                        timer.cancel();
                        return;
                    }

                    boolean isNameChange = Mp3InitListViewHandler.saveCurrentMp3(//
                            map.get("name"),//
                            map.get("path"), //
                            (int) PlaybackUtils.getPosition(), //
                            context);//

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            isPercentProgressTrigger.set(true);
                            getProgressBar().setProgress(percent);
                            getTextTimer().setText(timeTxt);
                            getTextTitle().setText(map.get("name"));
                            getTextContent().setText(map.get("path"));
                        }
                    });
                } catch (final Exception e) {
                    Log.e(TAG, "[PercentProgressBarTimer] ERR : " + e.getMessage(), e);
                    Log.line(TAG, "[PercentProgressBarTimer] ERR : " + e.getMessage(), e);
                    try {
                        timer.cancel();
                    } catch (Exception ex2) {
                    }
                }
            }
        }, 0, 1000L);
    }
}