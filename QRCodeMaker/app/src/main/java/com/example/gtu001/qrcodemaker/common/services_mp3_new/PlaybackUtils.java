package com.example.gtu001.qrcodemaker.common.services_mp3_new;

import android.os.RemoteException;

import com.example.gtu001.qrcodemaker.common.DateUtil;
import com.example.gtu001.qrcodemaker.common.Log;

import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PlaybackUtils {

    private static final String TAG = PlaybackUtils.class.getSimpleName();

    public static void openFile(String path) {
        if (ServiceUtils.sService == null) {
            Log.v(TAG, "server is null");
            return;
        }
        try {
            ServiceUtils.sService.openFile(path);
        } catch (RemoteException e) {
            Log.e(TAG, "ERR : " + e.getMessage(), e);
        }
    }

    public static void play() {
        if (ServiceUtils.sService == null) {
            Log.v(TAG, "server is null");
            return;
        }
        try {
            ServiceUtils.sService.play();
        } catch (RemoteException e) {
            Log.e(TAG, "ERR : " + e.getMessage(), e);
        }
    }

    public static void pause() {
        if (ServiceUtils.sService == null) {
            Log.v(TAG, "server is null");
            return;
        }
        try {
            ServiceUtils.sService.pause();
        } catch (RemoteException e) {
            Log.e(TAG, "ERR : " + e.getMessage(), e);
        }
    }

    public static void stop() {
        if (ServiceUtils.sService == null) {
            Log.v(TAG, "server is null");
            return;
        }
        try {
            ServiceUtils.sService.stop();
        } catch (RemoteException e) {
            Log.e(TAG, "ERR : " + e.getMessage(), e);
        }
    }

    public static long getDuration() {
        if (ServiceUtils.sService != null) {
            try {
                return ServiceUtils.sService.getDuration();
            } catch (RemoteException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
        return 0;
    }

    public static long getPosition() {
        if (ServiceUtils.sService != null) {
            try {
                return ServiceUtils.sService.getPosition();
            } catch (RemoteException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
        return 0;
    }

    public static boolean isPlaying() {
        if (ServiceUtils.sService != null) {
            try {
                return ServiceUtils.sService.isPlaying();
            } catch (final RemoteException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
        return false;
    }

    public static void seek(long position) {
        if (ServiceUtils.sService != null) {
            try {
                ServiceUtils.sService.seek(position);
            } catch (RemoteException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
    }

    //--------------------------------------------------------------------------- custom

    public static void setReplayMode(String currentName, int currentPosition, List<String> nameLst, List<String> pathLst, boolean isRandom) {
        if (ServiceUtils.sService != null) {
            try {
                ServiceUtils.sService.setReplayMode(currentName, currentPosition, nameLst, pathLst, isRandom);
            } catch (RemoteException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
    }

    public static String getCurrentName() {
        if (ServiceUtils.sService != null) {
            try {
                return ServiceUtils.sService.getCurrentName();
            } catch (RemoteException e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
        return "";
    }

    public static void backwardOrBackward(int second) {
        if (ServiceUtils.sService != null) {
            try {
                long length = getPosition();
                seek(length + (second * 1000));
                if (isPlaying()) {
                    play();
                } else {
                    pause();
                }
            } catch (Exception e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
    }

    public static void previousOrNext(int song) {
        if (ServiceUtils.sService != null) {
            try {
                ServiceUtils.sService.previousOrNext(song);
            } catch (Exception e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
    }

    public static void onProgressChange(int percent) {
        if (ServiceUtils.sService != null) {
            try {
                Log.v(TAG, "onProgressChange / percent : " + percent);
                if (percent == 0) {
                    return;
                }
                double currentPos = ((double) getDuration() / 100) * percent;
                seek((long) currentPos);
                if (isPlaying()) {
                    play();
                } else {
                    pause();
                }
            } catch (Exception e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
    }

    public static String getProgressTime() {
        if (ServiceUtils.sService != null) {
            try {
                String currentTimeStr = DateUtil.wasteTotalTime_HHmmss(getPosition());
                String allTimeStr = DateUtil.wasteTotalTime_HHmmss(getDuration());
                return currentTimeStr + " / " + allTimeStr;
            } catch (Exception e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
        return "NA";
    }

    public static int getProgressPercent() {
        if (ServiceUtils.sService != null) {
            try {
                double current = (double) getPosition();
                double duriation = (double) getDuration();
                int percent = (int) (current / duriation * 100);
                return percent;
            } catch (Exception e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
        return 0;
    }

    public static Map<String,String> getCurrentBean() {
        if (ServiceUtils.sService != null) {
            try {
                return ServiceUtils.sService.getCurrentBean();
            } catch (Exception e) {
                Log.e(TAG, "ERR : " + e.getMessage(), e);
            }
        } else {
            Log.v(TAG, "server is null");
        }
        return Collections.emptyMap();
    }

    public static void stopAll() {
        if (ServiceUtils.sService == null) {
            Log.v(TAG, "server is null");
            return;
        }
        try {
            ServiceUtils.sService.stopAll();
        } catch (RemoteException e) {
            Log.e(TAG, "ERR : " + e.getMessage(), e);
        }
    }
}
