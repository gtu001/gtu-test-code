import io
import re



def question_A(dataText) :
    checkAscii = re.compile("[^\x00-\x7F]", re.I| re.DOTALL | re.MULTILINE)
    matches = checkAscii.search(dataText)

    isAscii = False
    if matches == None :
        isAscii = True

    length = len(dataText) 
    lengthOk = False
    if length >= 1 and length <= 20 :
        lengthOk = True

    regexp = re.compile("[a-zA-Z]+", re.I | re.DOTALL | re.MULTILINE)
    matches = regexp.findall(dataText)

    print("matches", matches)

    if matches == False:
        return

    rtnStr = ''
    for m in matches:
        rtnStr += m.lower()

    return (not (isAscii and lengthOk), rtnStr)
    W


def main() :
    print(question_A("A man, a plan, a canal: Panama"))
    print(question_A("race a car"))


if __name__ == '__main__' :
    main()
    print("done...")
