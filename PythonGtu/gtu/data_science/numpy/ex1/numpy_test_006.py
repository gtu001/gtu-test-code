
import numpy as np
from gtu.reflect import checkSelf


def listIsInorder(lst) :
    temp = -1
    for c in lst :
        temp2 = ord(c[0])
        if temp2 > temp :
            temp = temp2
        else :
            return False
    return True


def question_B(strs, shape, columnCount) :

    n = len(strs)
    if not (n >= 1 and n <= 100) :
        print("Constraints violate")
        return 
    
    lst = list()
    for s in strs :
        lens = len(s)
        if not (lens >= 1 and lens <= 1000) :
            print("Constraints violate")
            return 
    
        for i in range(0, lens) :
            if s[i].isupper() :
                print("Constraints violate")
                return 
            lst.append(s[i])

    print("<<", lst)

    arry = np.array(lst)
    
    arry = np.reshape(arry, shape)

    deleteCount = 0

    for i in range(0, columnCount) :
        lst2 = arry[:, i].tolist()
        lstIsOk = listIsInorder(lst2)
        print("--->", lst2 , " is list ok ", lstIsOk)

        if lstIsOk == False:
            deleteCount +=1

    return deleteCount



if __name__ == '__main__':
    print("delete count = ", question_B(["cba","daf","ghi"], (3,3), 3))
    print("delete count = ", question_B(["a","b"], (1, 2), 2))
    print("delete count = ", question_B(["zyx","wvu","tsr"], (3,3), 3))
    print("done...")