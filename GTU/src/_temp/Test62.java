package _temp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.SerializationUtils;

public class Test62 {

    public static void main(String[] args) {
        Pattern ptn = Pattern.compile("(?=.*)");
        String strVal = "aaa|bbbb|cccc";
        Matcher mth = ptn.matcher(strVal);
        boolean findOk = false;
        while (mth.find()) {
            findOk = true;
            System.out.println(mth.group());
        }
        System.out.println(findOk);
        System.out.println("done...");
    }
}
