package _temp;

import java.io.IOException;
import java.text.ParseException;

import gtu.clipboard.ClipboardUtil;
import gtu.net.HttpUtil;

public class Test71 {

    public static void main(String[] args) throws IOException, ParseException {

        String id = "0";
        String phone = "84903" + "9999";
        String shortcode = "7039";
        String gateway = "vms-7x39";// vms-7x39,vtl-7x39,vnp-7x39,vnb-7x39
        String sms = "";
        String checksum = "";

        String testMessage = "C88";
        sms = bytesToHex(testMessage.getBytes("UTF8"));
        
        String checksumKey = "f65a0ea5725dfaa4a08c9cfc153389cde48fdf42";

        checksum = gtu.binary.MD5.md5(checksumKey + id + phone + shortcode + sms);
        
        String prefixUrl = "http://localhost:8889/vtdd_partner/send";
//        prefixUrl = "http://dev-comebet-gaming-api.hyu.tw/vtdd_partner/send"; // DEV

        String url = prefixUrl + "?id=" + id + "&phone=" + phone + "&shortcode=" + shortcode + "&gateway=" + gateway + "&sms=" + sms + "&checksum=" + checksum + "";

        System.out.println("url : " + url);
        
        String resultStr = HttpUtil.doGetRequest(url);
        System.out.println(">>>" + resultStr);
        
//        Your OTP is : 1234
        
        System.out.println("done...");
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

}
