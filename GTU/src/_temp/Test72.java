package _temp;

import java.io.IOException;
import java.text.ParseException;

import gtu.clipboard.ClipboardUtil;
import gtu.net.HttpUtil;
import gtu.runtime.ProcessWatcher;
import gtu.runtime.RuntimeBatPromptModeUtil;

public class Test72 {

    public static void main(String[] args) throws IOException, ParseException {

        RuntimeBatPromptModeUtil inst = RuntimeBatPromptModeUtil.newInstance();
        inst.command(" docker -version ");
        inst.runInBatFile(false);
        ProcessWatcher process = ProcessWatcher.newInstance(inst.apply());
        process.getStreamSync();
        System.out.println("input : " + process.getInputStreamToString());
        System.out.println("error : " + process.getErrorStreamToString());

        System.out.println("done...");
    }
}
