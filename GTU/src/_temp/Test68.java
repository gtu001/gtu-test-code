package _temp;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import gtu.date.DateUtil;

public class Test68 {

    public static void main(String[] args) throws IOException, ParseException {
        ZoneOffset ZONE = ZoneOffset.of("+8");
        Long start =  Instant.ofEpochSecond(System.currentTimeMillis()).atZone(ZONE).toLocalDate().atStartOfDay().toEpochSecond(ZONE);
        
        LocalDateTime ddddd = Instant.parse("2021-09-03T12:30:30.00Z").atZone(ZONE).toLocalDateTime();
        System.out.println(">>>" + ddddd);
        
        System.out.println("done...");
    }
}
