package _temp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class Test66 {

    public static void main(String[] args) {
        System.out.println("Question A ================");
        System.out.println(Arrays.toString(question_A("A man, a plan, a canal: Panama")));
        System.out.println(Arrays.toString(question_A("race a car")));
        System.out.println("Question B ================");
        System.out.println(question_B(new String[] { "cba", "daf", "ghi" }, 3, 3));
        System.out.println(question_B(new String[] { "a", "b" }, 1, 2));
        System.out.println(question_B(new String[] { "zyx", "wvu", "tsr" }, 3, 3));

        System.out.println("done...");
    }

    private static Object[] question_A(String strs) {
        Pattern isNotAscii = Pattern.compile("[^\\x00-\\x7F]");
        if (isNotAscii.matcher(strs).find()) {
            System.out.println("Constraints violate");
            return new Object[] { false, "" };
        }

        boolean valid = true;
        int len = StringUtils.length(strs);
        if (!(len >= 1 && len <= 20)) {
            System.out.println("Constraints violate");
            valid = false;
        }

        StringBuffer sb = new StringBuffer();
        Pattern ptn = Pattern.compile("[a-zA-Z]+", Pattern.CASE_INSENSITIVE);
        Matcher mth = ptn.matcher(strs);
        while (mth.find()) {
            sb.append(mth.group().toLowerCase());
        }
        return new Object[] { !valid, sb.toString() };
    }

    private static class Martix {
        Character value[][];
        int row;
        int col;

        Martix(List<Character> lst, int row, int col) {
            value = new Character[col][row];
            this.row = row;
            this.col = col;

            int row1 = 0;
            int col1 = 0;
            for (Character c : lst) {
                value[col1][row1] = c;
                row1++;
                if (row1 >= row) {
                    row1 = 0;
                    col1++;
                }
            }

            System.out.println("Matrix : ");
            for (Character[] arry : value) {
                System.out.println("\t" + Arrays.toString(arry));
            }
        }

        Character[] getColumn(int col) {
            List<Character> lst = new ArrayList<Character>();
            for (int ii = 0; ii < value.length; ii++) {
                lst.add(value[ii][col]);
            }
            return lst.toArray(new Character[0]);
        }
    }

    private static boolean isOrderOk(Character[] arry) {
        int temp = -1;
        for (int ii = 0; ii < arry.length; ii++) {
            int temp2 = (int) arry[ii];
            if (temp2 > temp) {
                temp = temp2;
            } else {
                return false;
            }
        }
        return true;
    }

    private static int question_B(String[] strs, int row, int col) {
        int n = strs.length;
        if (!(n >= 1 && n <= 100)) {
            System.out.println("Constraints violate");
        }
        List<Character> lst = new ArrayList<Character>();
        for (String str : strs) {
            int n2 = StringUtils.length(str);
            if (!(n2 >= 1 && n2 <= 1000)) {
                System.out.println("Constraints violate");
            }
            char[] arry = str.toCharArray();
            for (int ii = 0; ii < arry.length; ii++) {
                Character c = arry[ii];
                if (Character.isUpperCase(c)) {
                    System.out.println("Constraints violate");
                }
                lst.add(c);
            }
        }

        int deleteCount = 0;
        Martix mMartix = new Martix(lst, row, col);
        for (int ii = 0; ii < row; ii++) {
            Character[] column = mMartix.getColumn(ii);
            boolean deleteIfFalse = isOrderOk(column);
            System.out.println("Column : " + Arrays.toString(column) + " = " + deleteIfFalse);
            if (deleteIfFalse == false) {
                deleteCount++;
            }
        }
        return deleteCount;
    }
}
