package _temp;

import java.io.File;

import gtu.file.FileUtil;
import gtu.runtime.RuntimeBatPromptModeUtil;

public class Test64 {

    public static void main(String[] args) {
        try {
            File tempFile = File.createTempFile("PowerShell_", ".ps1");
            String content = " Get-Content \"C:/Users/gtu00/AppData/Local/Unity/Editor/Editor.log\" -Wait -Tail 30";
            FileUtil.saveToFile(tempFile, content, "UTF8");
//            String command = String.format("start powershell -noexit -file \"%s\"", tempFile);
            String command = String.format("start powershell -noexit \"%s\"", content);
            System.out.println(command);
            
            RuntimeBatPromptModeUtil inst = RuntimeBatPromptModeUtil.newInstance();
            inst.command(command);
            inst.apply();

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("done...");
    }
}
