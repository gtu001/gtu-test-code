package _temp;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

public class Test63 {

    public static void main(String[] args) {
        try {
            Context ctx = new InitialContext();
            ctx.bind("aaa", "AAAA");

            for (NamingEnumeration<NameClassPair> it = ctx.list("aaa"); it.hasMoreElements();) {
                NameClassPair obj = it.nextElement();
                System.out.println(obj.getClassName() + "\t" + obj.getName() + "\t" + obj.getNameInNamespace());
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }
        System.out.println("done...");
    }
}
