package _temp;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JFrame;
import javax.swing.JTextField;

import gtu.swing.JFrameTest;
import gtu.swing.util.JCommonUtil;

public class Test69 {

    public static void main(String[] args) throws IOException, ParseException {

        final JTextField jText = new JTextField();
        jText.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                File file = JCommonUtil._jFileChooser_selectFileAndDirectory();
                System.out.println("file = " + file);
                if (file == null) {
                    return;
                }
                jText.setText(file.getAbsolutePath());
            }
        });
        JFrame frame = JFrameTest.simpleTestComponent(jText);
        System.out.println("done...");
    }

}
