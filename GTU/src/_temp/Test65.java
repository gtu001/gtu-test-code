package _temp;

import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;

import gtu.swing.JFrameTest;
import gtu.swing.util.JCommonUtil;

public class Test65 {

    public static void main(String[] args) {
        try {
            JTextArea area = new JTextArea();
            JFrameTest.simpleTestComponent(area);

            area.setDropTarget(new DropTarget() {
                public synchronized void drop(DropTargetDropEvent evt) {
                    try {
                        evt.acceptDrop(DnDConstants.ACTION_COPY);
                        Object value = evt.getTransferable().getTransferData(null);
                        System.out.println(" value = " + value);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("done...");
    }
}
