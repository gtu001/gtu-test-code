package gtu.memory;

public class MemoryUsage {

    public static void main(String[] args) {
        MemoryUsage mMemoryUsage = MemoryUsage.newInstance();
        for(long ii = 0 ; true; ii ++ ) {
            mMemoryUsage.printMessage(ii);    
        }
    }

    private final static String[] units = { "Bytes", "KBytes", "MBytes", "GBytes" };

    long prevTotal = 0;
    long prevFree = 0;
    Runtime rt = Runtime.getRuntime();

    public static MemoryUsage newInstance() {
        return new MemoryUsage();
    }

    private MemoryUsage() {
        prevFree = rt.freeMemory();
    }

    private String toMemoryString(double bytes) {
        int pos = 0;
        while (bytes > 1024) {
            pos++;
            bytes = bytes / 1024;
        }
        return String.format("%2.2f", bytes) + " " + units[pos];
    }

    public void printMessage(Long i) {
        String istr = "";
        if (i != null) {
            istr = String.valueOf(i);
        }
        long total = rt.totalMemory();
        long free = rt.freeMemory();
        if (total != prevTotal || free != prevFree) {
            System.out.println(String.format("#%s, Total: %s, Free: %s, Diff: %s", istr, toMemoryString(total), toMemoryString(free), toMemoryString(prevFree - free)));
            prevTotal = total;
            prevFree = free;
        }
    }
}
