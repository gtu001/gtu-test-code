package gtu.jni;

import java.io.File;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import gtu.file.FileUtil;

public class Clipbd {

    static {
        try {
            System.loadLibrary("ConsoleApp1");
        } catch (Throwable ex) {

            URL jarFile = Clipbd.class.getProtectionDomain().getCodeSource().getLocation();
            System.out.println(jarFile);
            try {
                JarInputStream jarInputStream = new JarInputStream(jarFile.openStream());
                Manifest manifest = jarInputStream.getManifest();
                if (manifest != null) {
                    Attributes attributes = manifest.getAttributes("/");
                    for (Object key : attributes.keySet()) {
                        String name = (String) key;
                        String value = attributes.getValue(name);
                        System.out.println("-->" + name + " = " + value);
                    }
                }
                jarInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                URL url = Clipbd.class.getClassLoader().getResource("resource/jniLibs/ConsoleApp1.dll");
                File file = FileUtil.createFileFromURL(url, "ConsoleApp1.dll");
                System.load(file.getAbsolutePath());
            } catch (Exception ex1) {
                ex1.printStackTrace();
            }
        }
    }

    public static native String GetText();

    public static void main(String[] args) {
        String content = GetText();
        System.out.println(content);
        System.out.println("done...");
    }
}
