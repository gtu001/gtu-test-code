package gtu.music;

import java.io.File;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class MusicPlayerUtil {
    
    public static void main(String[] args) {
        play("C:\\Users\\gtu00\\OneDrive\\Desktop\\Rhapsody -of Fire- - Dawn of Victory.mp3");
    }

    public static void play(String path) {
        Media hit = new Media(new File(path).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        mediaPlayer.play();
    }
}
