package gtu.file;

public class OsInfoUtil {

    private static OS currentOS = null;
    
    public static enum OS {
    	WINDOW,//
    	LINUX,//
    	MAC//
    }
    
    static {
        if (System.getProperty("os.name").startsWith("Windows")) {
        	currentOS = OS.WINDOW;
        } else if ("Linux".equals(System.getProperty("os.name"))) {
        	currentOS = OS.LINUX;
        } else if (System.getProperty("os.name").startsWith("Mac")) {
        	currentOS = OS.MAC;
        }
    }

    public static boolean isWindows() {
        return currentOS == OS.WINDOW;
    }
    
    public static boolean isMac() {
    	return currentOS == OS.MAC;
    }
    
    public static boolean isLinux() {
    	return currentOS == OS.LINUX;
    }
    
    public static void main(String[] args) {
    	System.out.println(System.getProperty("os.name"));
    }
}
