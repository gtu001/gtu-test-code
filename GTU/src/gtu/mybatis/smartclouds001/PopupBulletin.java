package gtu.mybatis.smartclouds001;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author TroyChang
 * @since 2021-9-1
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("lottery_popup_bulletin")
public class PopupBulletin implements Serializable {

    private static final long serialVersionUID=1L;
    @TableId(value = "id",type = IdType.AUTO)
    private java.math.BigDecimal id;
    @TableField("adminId")
    private java.math.BigDecimal adminId;
    @TableField("title")
    private String title;
    @TableField("content")
    private String content;
    @TableField("platform")
    private String platform;
    @TableField("insertTime")
    private java.sql.Timestamp insertTime;
    @TableField("startTime")
    private java.sql.Timestamp startTime;
    @TableField("endTime")
    private java.sql.Timestamp endTime;
    @TableField("enable")
    private java.math.BigDecimal enable;
    @TableField("isTop")
    private java.math.BigDecimal isTop;
    @TableField("isDelete")
    private java.math.BigDecimal isDelete;
}
