package gtu.jfreechart;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

@WebServlet("/JFreeChartServlet")
public class JFreeChartServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public JFreeChartServlet() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }

    private ChartService chartService;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            response.setContentType("image/jpeg");
            JFreeChart chart = chartService.getJFreeChart(chartService.getDataSet(), chartService.getChartTitle());
            chart.setTitle(chartService.getChartTitle());

            // option1
            // ChartUtils.writeChartAsJPEG(response.getOutputStream(),
            // chartService.getQuality(), chart, chartService.getWidth(),
            // chartService.getHeight());// ChartUtils
            // ChartUtilities
            ChartUtilities.writeChartAsJPEG(response.getOutputStream(), chartService.getQuality(), chart, chartService.getWidth(), chartService.getHeight());

            // option2
            {
                BufferedImage objBufferedImage = chart.createBufferedImage(600, 800);
                ByteArrayOutputStream bas = new ByteArrayOutputStream();
                try {
                    ImageIO.write(objBufferedImage, "png", bas);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                byte[] byteArray = bas.toByteArray();
                response.setHeader("Content-Disposition", "filename=\"jfreechart.png\"");
                response.setContentLength(byteArray.length);
                OutputStream os = response.getOutputStream();
                try {
                    os.write(byteArray, 0, byteArray.length);
                } catch (Exception excp) {
                    // handle error
                } finally {
                    os.close();
                }
            }

            response.getOutputStream().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
