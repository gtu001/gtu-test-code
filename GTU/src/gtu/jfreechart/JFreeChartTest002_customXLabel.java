package gtu.jfreechart;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.AxisState;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnit;
import org.jfree.chart.axis.TickUnitSource;
import org.jfree.chart.axis.ValueTick;
import org.jfree.chart.renderer.AbstractRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.text.TextUtilities;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;
import org.jfree.util.ObjectUtilities;

import gtu.swing.util.JCommonUtil;

public class JFreeChartTest002_customXLabel extends ApplicationFrame {

    private static final long serialVersionUID = 1L;

    public JFreeChartTest002_customXLabel() {
        super("title");
        test1();
    }

    public static class MyNumberTickUnit extends NumberTickUnit {
        public MyNumberTickUnit(double size, NumberFormat formatter, int minorTickCount) {
            super(size, formatter, minorTickCount);
        }

        // TODO 最重要的方法
        public String valueToString(double value) {
            System.out.println(value);
            NumberFormat numberFormat = new DecimalFormat("#,###,###,###,###");

            BigDecimal val = new BigDecimal(value);
            val = val.divide(new BigDecimal(1000));

            return numberFormat.format(val) + "千元";
        }
    }

    public static class MyTickUnit extends TickUnit {
        public MyTickUnit(double value) {
            super(value);
        }
    }

    private void applyUsingChinese() {
        // 建立主題樣式
        StandardChartTheme standardChartTheme = new StandardChartTheme("CN");
        // 設定標題字型
        standardChartTheme.setExtraLargeFont(new Font("隸書", Font.BOLD, 20));
        // 設定圖例的字型
        standardChartTheme.setRegularFont(new Font("宋書", Font.PLAIN, 15));
        // 設定軸向的字型
        standardChartTheme.setLargeFont(new Font("宋書", Font.PLAIN, 15));
        // 應用主題樣式
        ChartFactory.setChartTheme(standardChartTheme);
    }

    private void test1() {
        applyUsingChinese();

        DefaultXYDataset dataset = new DefaultXYDataset();
        XYSeries s = new XYSeries("S");
        s.add(1.0, 11000000.0);
        s.add(2.0, 22000000.0);
        s.add(3.5, 35000000.0);
        s.add(5.0, null);
        dataset.addSeries("S", s.toArray());

        JFreeChart chart = ChartFactory.createXYLineChart("title", //
                "X", //
                "Y", //
                dataset);
        chart.getXYPlot().setDomainAxis(new CustomXAxis());
        TickUnitSource source = NumberAxis.createIntegerTickUnits();

        chart.getXYPlot().getRangeAxis().setStandardTickUnits(new MyNumberTickUnitSource());

        // 改背景色
        chart.getXYPlot().setBackgroundPaint(Color.white);

//        改x軸標籤顏色
//        chart.getXYPlot().getDomainAxis().setTickLabelPaint(Color.blue);
//        chart.getXYPlot().getDomainAxis().setLabelPaint(Color.red);

        // 紅色原點 ↓↓↓↓↓↓
        XYItemRenderer r = chart.getXYPlot().getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setBaseShapesVisible(true);
            renderer.setBaseShapesFilled(true);
        }
        // 紅色原點 ↑↑↑↑↑↑

        // 線粗細 ↓↓↓↓↓↓
        r.setBaseStroke(new BasicStroke(3.0f));
        ((AbstractRenderer) r).setAutoPopulateSeriesStroke(false);
        // 線粗細 ↑↑↑↑↑↑

        ChartPanel chartPanel = new ChartPanel(chart);

        chartPanel.setPreferredSize(new Dimension(500, 270));
        setContentPane((Container) chartPanel);
        JCommonUtil.setJFrameCenter(this);
    }

    public static class CustomXAxis extends NumberAxis {

        protected AxisState drawTickMarksAndLabels(Graphics2D g2, double cursor, Rectangle2D plotArea, Rectangle2D dataArea, RectangleEdge edge) {
            AxisState state = new AxisState(cursor);

            g2.setFont(getTickLabelFont());

            double ol = getTickMarkOutsideLength();
            double il = getTickMarkInsideLength();
            int y = (int) (Math.round(cursor - ol));
            LineMetrics lineMetrics = g2.getFont().getLineMetrics("Ápr", g2.getFontRenderContext());
            int h = (int) (lineMetrics.getHeight() + 6);

            // 重新定義X軸
            List<ValueTick> ticks = refreshTicks(g2, state, dataArea, edge);
            state.setTicks(ticks);

            for (int ii = 0; ii < ticks.size(); ii++) {
                ValueTick tick = ticks.get(ii);

                if (StringUtils.isNotBlank(tick.getText())) {
                    float[] prevAnchorPoint = calculateAnchorPoint(tick, cursor, dataArea, edge);
                    float[] prevAnchorPointAfter = null;

                    int width = 0;
                    if (ii + 1 <= ticks.size() - 1) {
                        prevAnchorPointAfter = calculateAnchorPoint(ticks.get(ii + 1), cursor, dataArea, edge);
                        width = Math.round(prevAnchorPointAfter[0] - prevAnchorPoint[0]);
                    }

                    int x1 = Math.round(prevAnchorPoint[0]);
                    g2.setColor(Color.BLACK);
                    g2.setFont(getTickLabelFont());
                    g2.setColor(Color.LIGHT_GRAY);
                    g2.fill3DRect(x1, y, width, h, true);
                    g2.setColor(Color.BLACK);
                    TextUtilities.drawAlignedString("測" + tick.getValue(), g2, x1, y, TextAnchor.TOP_CENTER);// <---g2決定字的顏色 TODO
                    System.out.println("x , y = " + x1 + "/ " + y);
                }
            }
            return state;
        }
    }

    public class MyNumberTickUnitSource implements TickUnitSource, Serializable {

        private boolean integers;

        private int power = 0;

        private int factor = 1;

        /** The number formatter to use (an override, it can be null). */
        private NumberFormat formatter;

        /**
         * Creates a new instance.
         */
        public MyNumberTickUnitSource() {
            this(false);
        }

        /**
         * Creates a new instance.
         * 
         * @param integers show integers only.
         */
        public MyNumberTickUnitSource(boolean integers) {
            this(integers, null);
        }

        /**
         * Creates a new instance.
         * 
         * @param integers  show integers only?
         * @param formatter a formatter for the axis tick labels ({@code null}
         *                  permitted).
         */
        public MyNumberTickUnitSource(boolean integers, NumberFormat formatter) {
            this.integers = integers;
            this.formatter = formatter;
            this.power = 0;
            this.factor = 1;
        }

        @Override
        public TickUnit getLargerTickUnit(TickUnit unit) {
            TickUnit t = getCeilingTickUnit(unit);
            if (t.equals(unit)) {
                next();
                t = new MyNumberTickUnit(getTickSize(), getTickLabelFormat(), getMinorTickCount());
            }
            return t;
        }

        @Override
        public TickUnit getCeilingTickUnit(TickUnit unit) {
            return getCeilingTickUnit(unit.getSize());
        }

        @Override
        public TickUnit getCeilingTickUnit(double size) {
            if (Double.isInfinite(size)) {
                throw new IllegalArgumentException("Must be finite.");
            }
            this.power = (int) Math.ceil(Math.log10(size));
            if (this.integers) {
                power = Math.max(this.power, 0);
            }
            this.factor = 1;
            boolean done = false;
            // step down in size until the current size is too small or there are
            // no more units
            while (!done) {
                done = !previous();
                if (getTickSize() < size) {
                    next();
                    done = true;
                }
            }
            return new MyNumberTickUnit(getTickSize(), getTickLabelFormat(), getMinorTickCount());
        }

        private boolean next() {
            if (factor == 1) {
                factor = 2;
                return true;
            }
            if (factor == 2) {
                factor = 5;
                return true;
            }
            if (factor == 5) {
                if (power == 300) {
                    return false;
                }
                power++;
                factor = 1;
                return true;
            }
            throw new IllegalStateException("We should never get here.");
        }

        private boolean previous() {
            if (factor == 1) {
                if (this.integers && power == 0 || power == -300) {
                    return false;
                }
                factor = 5;
                power--;
                return true;
            }
            if (factor == 2) {
                factor = 1;
                return true;
            }
            if (factor == 5) {
                factor = 2;
                return true;
            }
            throw new IllegalStateException("We should never get here.");
        }

        private double getTickSize() {
            return this.factor * Math.pow(10.0, this.power);
        }

        private DecimalFormat dfNeg4 = new DecimalFormat("0.0000");
        private DecimalFormat dfNeg3 = new DecimalFormat("0.000");
        private DecimalFormat dfNeg2 = new DecimalFormat("0.00");
        private DecimalFormat dfNeg1 = new DecimalFormat("0.0");
        private DecimalFormat df0 = new DecimalFormat("#,##0");
        private DecimalFormat df = new DecimalFormat("#.######E0");

        private NumberFormat getTickLabelFormat() {
            if (this.formatter != null) {
                return this.formatter;
            }
            if (power == -4) {
                return dfNeg4;
            }
            if (power == -3) {
                return dfNeg3;
            }
            if (power == -2) {
                return dfNeg2;
            }
            if (power == -1) {
                return dfNeg1;
            }
            if (power >= 0 && power <= 6) {
                return df0;
            }
            return df;
        }

        private int getMinorTickCount() {
            if (factor == 1) {
                return 10;
            } else if (factor == 5) {
                return 5;
            }
            return 0;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof MyNumberTickUnitSource)) {
                return false;
            }
            MyNumberTickUnitSource that = (MyNumberTickUnitSource) obj;
            if (this.integers != that.integers) {
                return false;
            }
            if (!ObjectUtilities.equal(this.formatter, that.formatter)) {
                return false;
            }
            if (this.power != that.power) {
                return false;
            }
            if (this.factor != that.factor) {
                return false;
            }
            return true;
        }
    }

    public static void main(String[] args) {
        JFreeChartTest002_customXLabel demo = new JFreeChartTest002_customXLabel();
        demo.pack();
        // UIUtils.centerFrameOnScreen((Window) demo);
        demo.setVisible(true);
    }
}
