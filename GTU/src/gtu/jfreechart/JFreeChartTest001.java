package gtu.jfreechart;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDifferenceRenderer;
import org.jfree.data.time.Month;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.ui.ApplicationFrame;

public class JFreeChartTest001 extends ApplicationFrame {

    public JFreeChartTest001(String title, int demo) {
        super(title);
        switch (demo) {
        case 3:
            test3();
            break;
        }
    }

    private void test3() {
        TimeSeries s1 = new TimeSeries("Sunshine Hours");
        s1.add((RegularTimePeriod) new Month(1, 2005), 56.4D);
        s1.add((RegularTimePeriod) new Month(2, 2005), 72.0D);
        s1.add((RegularTimePeriod) new Month(3, 2005), 79.5D);
        s1.add((RegularTimePeriod) new Month(4, 2005), 146.9D);
        s1.add((RegularTimePeriod) new Month(5, 2005), 216.6D);
        s1.add((RegularTimePeriod) new Month(6, 2005), 190.7D);
        s1.add((RegularTimePeriod) new Month(7, 2005), 178.7D);
        s1.add((RegularTimePeriod) new Month(8, 2005), 210.6D);
        s1.add((RegularTimePeriod) new Month(9, 2005), 150.6D);
        s1.add((RegularTimePeriod) new Month(10, 2005), 81.1D);
        s1.add((RegularTimePeriod) new Month(11, 2005), 90.9D);
        s1.add((RegularTimePeriod) new Month(12, 2005), 57.0D);
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(s1);
        JFreeChart chart = ChartFactory.createXYBarChart("Sunshine Hours - England & Wales", "Date", true, "Hours", (IntervalXYDataset) dataset);

        XYPlot plot3 = chart.getXYPlot();
        plot3.setBackgroundPaint(Color.white);// 網格背景色
//        plot3.setBackgroundPaint(Color.LIGHT_GRAY);
        plot3.setRenderer(new XYDifferenceRenderer(new Color(255, 164, 0), Color.WHITE, false));
        plot3.setRangeGridlinePaint(Color.BLACK);
        plot3.setRangeGridlinesVisible(true);
        plot3.setDomainGridlinePaint(Color.WHITE);
        plot3.setRangeGridlinePaint(Color.WHITE);

        chart.setBackgroundPaint(Color.WHITE);

        DateAxis dateAxis = (DateAxis) plot3.getDomainAxis();
        dateAxis.setTickMarkPosition(DateTickMarkPosition.MIDDLE);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(500, 270));
        setContentPane((Container) chartPanel);
    }

    public static void main(String[] args) {
        {
            JFreeChartTest001 demo = new JFreeChartTest001("Time Series Bar Demo 3", 3);
            demo.pack();
            // UIUtils.centerFrameOnScreen((Window) demo);
            demo.setVisible(true);
        }
    }

}
