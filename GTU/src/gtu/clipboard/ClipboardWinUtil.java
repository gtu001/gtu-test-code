package gtu.clipboard;

import java.io.File;
import java.net.URL;

import gtu.file.FileUtil;
import gtu.jni.Clipbd;
import gtu.runtime.ProcessWatcher;
import gtu.runtime.RuntimeBatPromptModeUtil;
import gtu.zip.ZipUtils;

public class ClipboardWinUtil {

    private static File getPaste() {
        try {
            URL url = Clipbd.class.getClassLoader().getResource("resource/exe/paste.zip");
            File zipfile = FileUtil.createFileFromURL(url, "paste.zip");
            ZipUtils.getInstance().unzipFile(zipfile, zipfile.getParentFile());
            System.out.println(zipfile);
            return new File(zipfile.getParentFile(), "paste.exe");
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
        return null;
    }

    private static File pasteFile;

    public static String getContent() {
        if (pasteFile == null || !pasteFile.exists()) {
            pasteFile = getPaste();
        }
        if (pasteFile != null) {
            RuntimeBatPromptModeUtil inst = RuntimeBatPromptModeUtil.newInstance();
            inst.command(String.format("\"%s\"", pasteFile));
            inst.runInBatFile(false);
            ProcessWatcher watcher = ProcessWatcher.newInstance(inst.apply());
            watcher.getStreamSync();
            String result = watcher.getInputStreamToString();
            return RuntimeBatPromptModeUtil.getFixBatInputString(result, 2, -1);
        }
        return "";
    }

    public static void main(String[] args) {
        String content = ClipboardWinUtil.getContent();
        System.out.println("content = " + content);
        System.out.println("done...");
    }
}
