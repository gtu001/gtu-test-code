package gtu.clipboard;

import java.io.File;
import java.io.IOException;

import gtu.file.FileUtil;
import gtu.runtime.ProcessWatcher;

public class ClipboardMacUtil {

    public static void main(String[] args) {
        String result = ClipboardMacUtil.getContentsMac();
        System.out.println(result);
        System.out.println("done...");
    }

    public static void setContentsMac(String context) {
        ProcessWatcher watcher = null;
        try {
            String copyCommand = String.format("echo %s|pbcopy", context);
            StringBuilder cmd = new StringBuilder();
            cmd.append(copyCommand);
            String prefix = "mac_pdcopy_";
            File tmpSh = File.createTempFile(prefix, ".command");
            cmd.insert(0, "#!/bin/bash\r\n");
            FileUtil.saveToFile(tmpSh, cmd.toString(), "UTF8");
            Runtime.getRuntime().exec(String.format("chmod u+x %s", tmpSh));
            watcher = ProcessWatcher.newInstance(Runtime.getRuntime().exec(String.format("sh %s ", tmpSh)));
            watcher.getStreamSync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            watcher.destroy(true);
        }
    }

    public static String getContentsMac() {
        ProcessWatcher watcher = null;
        String result = null;
        try {
            Process process = Runtime.getRuntime().exec("pbpaste");
            watcher = ProcessWatcher.newInstance(process);
            watcher.getStreamSync();
            result = watcher.getInputStreamToString();
            if (result.length() > 0 && result.charAt(result.length() - 1) == '\n') {
                StringBuilder sb = new StringBuilder(result);
                sb.deleteCharAt(sb.length() - 1);
                result = sb.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            watcher.destroy(true);
        }
        return result;
    }

}
