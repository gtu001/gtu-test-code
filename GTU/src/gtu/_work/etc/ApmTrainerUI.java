package gtu._work.etc;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import gtu._work.ui.SwingTemplateUI;
import gtu.date.DateUtil;
import gtu.number.RandomUtil;
import gtu.properties.PropertiesUtilBean;
import gtu.swing.util.JColorUtil;
import gtu.swing.util.JColorUtil.LinearGradientColor;
import gtu.swing.util.JCommonUtil;
import gtu.swing.util.JFrameUtil;
import gtu.swing.util.JMouseEventUtil;
import gtu.swing.util.JPopupMenuUtil;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class ApmTrainerUI extends javax.swing.JFrame {
    private static final long serialVersionUID = 1L;

    private JPanel jPanel1;
    Graphics2D g2dBlack;
    Graphics2D g2dWhite;
    Graphics2D g2dGreen;
    MouseEvent mousePressEvent;
    boolean initDraw;
    // boolean clickMode;
    boolean started = false;
    Timer timer = null;
    RectagleHolder mRectagleHolder = new RectagleHolder();
    ApmCounter backgroundApmCounter;

    int circleSize = 10;
    long rectagleInterval = 500;
    int maxSizeOfRectagle = 5;
    int widthOfRectagle_BLACK = 100;
    int heightOfRectagle_BLACK = 100;
    boolean useCustomWidthHeightOfRectagle_BLACK = false;
    int widthOfRectagle_RED = 75;
    int heightOfRectagle_RED = 75;
    boolean useCustomWidthHeightOfRectagle_RED = true;
    String colorOfGreen = "#00ee00";
    String colorOfCube = "#0000c4";
    String colorOfRed = "#FF0000";
    String ratioOfCube = "9-1";
    double rateOfMoveMode = 100D;
    double rateOfFrameStart = -0.5D;
    double rateOfFrameEnd = 0.5D;
    boolean useRandomColor = false;
    boolean useRandomColor2 = false;

    long todayFailClick = 0;
    long todaySuccessClick = 0;
    long todayTotalClick = 0;
    long todayDuring = 0;
    long thisTimeFailClick = 0;
    long thisTimeSuccessClick = 0;
    long thisTimeTotalClick = 0;
    long thisTimeDuring = 0;
    long thisTimeDuringPadding = 0;

    List<Long> thisTimeDuringLst = new ArrayList<Long>();

    boolean isContinueTimer = false;

    ModeHandler mModeHandler;

    static int APM_SECOND = 60;
    static int XXX = -2;
    static int YYY = -11;

    PropertiesUtilBean config = new PropertiesUtilBean(ApmTrainerUI.class);
    PropertiesUtilBean operateConfig = new PropertiesUtilBean(ApmTrainerUI.class, ApmTrainerUI.class.getSimpleName() + "_Operate");

    static class ApmCounter {
        int correctTime;
        int incorrectTime;
        Thread thread;
        boolean executeThread = true;
        Timer timer;
    }

    public static void main(String[] args) {
        if (!JFrameUtil.lockInstance_delable(SwingTemplateUI.class)) {
            return;
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ApmTrainerUI inst = new ApmTrainerUI();
                    gtu.swing.util.JFrameUtil.setVisible(true, inst);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void drawReleaseRectagle(MouseEvent m1, MouseEvent m2) {
        final int x = Math.min(m1.getX() + 5, m2.getX() + 5);
        final int y = Math.min(m1.getY() + 25, m2.getY() + 25);
        final int w = Math.max(m1.getX() + 5, m2.getX() + 5) - x;
        final int h = Math.max(m1.getY() + 25, m2.getY() + 25) - y;
        final RoundRectangle2D rect = new RoundRectangle2D.Double(x, y, w, h, 0, 0);
        g2dGreenDraw(rect, null);
        new Thread(Thread.currentThread().getThreadGroup(), new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Rectangle rect2 = new Rectangle();
                rect2.x = (int) (x);
                rect2.y = (int) (y);
                rect2.width = (int) (w);
                rect2.height = (int) (h);

                // g2dWhiteDraw(rect, rect2);
                g2dGreenHidden();
            }
        }).start();
    }

    private String getToday() {
        return DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd");
    }

    private void reflectConfigToUI() {
        ratioOfCube = (String) config.getProperty("ratioOfCube", ratioOfCube, null);
        circleSize = (Integer) config.getProperty("circleSize", circleSize, null);
        rectagleInterval = (Long) config.getProperty("rectagleInterval", rectagleInterval, null);
        maxSizeOfRectagle = (Integer) config.getProperty("maxSizeOfRectagle", maxSizeOfRectagle, null);
        widthOfRectagle_BLACK = (Integer) config.getProperty("widthOfRectagle_BLACK", widthOfRectagle_BLACK, null);
        heightOfRectagle_BLACK = (Integer) config.getProperty("heightOfRectagle_BLACK", heightOfRectagle_BLACK, null);
        useCustomWidthHeightOfRectagle_BLACK = (Boolean) config.getProperty("useCustomWidthHeightOfRectagle_BLACK", useCustomWidthHeightOfRectagle_BLACK, null);
        widthOfRectagle_RED = (Integer) config.getProperty("widthOfRectagle_RED", widthOfRectagle_RED, null);
        heightOfRectagle_RED = (Integer) config.getProperty("heightOfRectagle_RED", heightOfRectagle_RED, null);
        useCustomWidthHeightOfRectagle_RED = (Boolean) config.getProperty("useCustomWidthHeightOfRectagle_RED", useCustomWidthHeightOfRectagle_RED, null);
        colorOfGreen = (String) config.getProperty("colorOfGreen", colorOfGreen, null);
        colorOfCube = (String) config.getProperty("colorOfCube", colorOfCube, null);
        rateOfMoveMode = (Double) config.getProperty("rateOfMoveMode", rateOfMoveMode, null);
        rateOfFrameStart = (Double) config.getProperty("rateOfFrameStart", rateOfFrameStart, null);
        rateOfFrameEnd = (Double) config.getProperty("rateOfFrameEnd", rateOfFrameEnd, null);
        useRandomColor = (Boolean) config.getProperty("useRandomColor", useRandomColor, null);
        useRandomColor2 = (Boolean) config.getProperty("useRandomColor2", useRandomColor2, null);
        todayFailClick = (Long) operateConfig.getProperty(getToday() + "_FAIL", todayFailClick, null);
        todaySuccessClick = (Long) operateConfig.getProperty(getToday() + "_SUCCESS", todaySuccessClick, null);
        todayTotalClick = (Long) operateConfig.getProperty(getToday() + "_TOTAL_CLICK", todayTotalClick, null);
        todayDuring = (Long) operateConfig.getProperty(getToday() + "_DURING", todayTotalClick, null);
    }

    private void reflectUIToConfigAndStore() {
        config.setPropertyNullIsEmpty("ratioOfCube", ratioOfCube);
        config.setPropertyNullIsEmpty("circleSize", circleSize);
        config.setPropertyNullIsEmpty("rectagleInterval", rectagleInterval);
        config.setPropertyNullIsEmpty("maxSizeOfRectagle", maxSizeOfRectagle);
        config.setPropertyNullIsEmpty("widthOfRectagle_BLACK", widthOfRectagle_BLACK);
        config.setPropertyNullIsEmpty("heightOfRectagle_BLACK", heightOfRectagle_BLACK);
        config.setPropertyNullIsEmpty("useCustomWidthHeightOfRectagle_BLACK", useCustomWidthHeightOfRectagle_BLACK);
        config.setPropertyNullIsEmpty("widthOfRectagle_RED", widthOfRectagle_RED);
        config.setPropertyNullIsEmpty("heightOfRectagle_RED", heightOfRectagle_RED);
        config.setPropertyNullIsEmpty("useCustomWidthHeightOfRectagle_RED", useCustomWidthHeightOfRectagle_RED);
        config.setPropertyNullIsEmpty("colorOfGreen", colorOfGreen);
        config.setPropertyNullIsEmpty("colorOfCube", colorOfCube);
        config.setPropertyNullIsEmpty("rateOfMoveMode", rateOfMoveMode);
        config.setPropertyNullIsEmpty("rateOfFrameStart", rateOfFrameStart);
        config.setPropertyNullIsEmpty("rateOfFrameEnd", rateOfFrameEnd);
        config.setPropertyNullIsEmpty("useRandomColor", useRandomColor);
        config.setPropertyNullIsEmpty("useRandomColor2", useRandomColor2);
        config.store();
    }

    public ApmTrainerUI() {
        try {
            reflectConfigToUI();

            BorderLayout thisLayout = new BorderLayout();

            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            // setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            getContentPane().setLayout(thisLayout);
            {
                jPanel1 = new JPanel();

                getContentPane().add(jPanel1, BorderLayout.CENTER);
                jPanel1.setBackground(new java.awt.Color(255, 255, 255));

                jPanel1.setLayout(null);

                this.addKeyListener(new KeyAdapter() {

                    @Override
                    public void keyReleased(KeyEvent e) {
                        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                            if (isContinueTimer) {
                                JCommonUtil._jOptionPane_showMessageDialog_info("暫停");
                                isContinueTimer = false;
                                thisTimeDuringLst.add(thisTimeDuring);
                                thisTimeDuring = 0;
                                thisTimeDuringPadding = 0;
                            } else {
                                JCommonUtil._jOptionPane_showMessageDialog_info("繼續");
                                isContinueTimer = true;
                            }
                        }
                    }
                });

                jPanel1.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (JMouseEventUtil.buttonRightClick(2, e)) {
                            JPopupMenuUtil.newInstance(jPanel1).applyEvent(e)//
                                    .addJMenuItem("重新計算APM", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            backgroundApmCounterInit();
                                            initDraw = false;
                                        }
                                    })//
                                    .addJMenuItem("調整黑框Size", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String value = JCommonUtil._jOptionPane_showInputDialog("請輸入黑框的佔螢幕的比例,數字越小黑框越大 : (若1/10則輸入10)", String.valueOf(circleSize));
                                                circleSize = Integer.parseInt(StringUtils.trimToEmpty(value));
                                                reflectUIToConfigAndStore();
                                            } catch (Exception ex) {
                                                JCommonUtil._jOptionPane_showMessageDialog_error("請輸入2~40之間的建議值");
                                            }
                                        }
                                    })//
                                    .addJMenuItem("畫面同時最多方塊數量", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String value = JCommonUtil._jOptionPane_showInputDialog("畫面同時最多方塊數量", String.valueOf(maxSizeOfRectagle));
                                                maxSizeOfRectagle = Integer.parseInt(StringUtils.trimToEmpty(value));
                                                reflectUIToConfigAndStore();
                                            } catch (Exception ex) {
                                                JCommonUtil._jOptionPane_showMessageDialog_error("設定未成功!");
                                            }
                                        }
                                    })//
                                    .addJMenuItem("設定模式比例[點-圈]", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String text = JCommonUtil._jOptionPane_showInputDialog("設定模式比例[點-圈]", ratioOfCube);
                                                if (StringUtils.isNotBlank(text)) {
                                                    mModeHandler = new ModeHandler(text);
                                                    ratioOfCube = text;
                                                }
                                                reflectUIToConfigAndStore();
                                            } catch (Exception ex) {
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("設定移動方百分比", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String text = JCommonUtil._jOptionPane_showInputDialog("設定模式比例[點-圈]", String.valueOf(rateOfMoveMode));
                                                rateOfMoveMode = Double.parseDouble(text);
                                                reflectUIToConfigAndStore();
                                            } catch (Exception ex) {
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("調整黑框Size[w,h]", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String value = JCommonUtil._jOptionPane_showInputDialog("調整黑框Size[w,h]", widthOfRectagle_BLACK + "," + heightOfRectagle_BLACK);
                                                String[] arry = value.split(",", -1);
                                                if (arry.length == 2) {
                                                    widthOfRectagle_BLACK = Integer.parseInt(StringUtils.trimToEmpty(arry[0]));
                                                    heightOfRectagle_BLACK = Integer.parseInt(StringUtils.trimToEmpty(arry[1]));
                                                    useCustomWidthHeightOfRectagle_BLACK = true;
                                                }
                                                reflectUIToConfigAndStore();
                                            } catch (Exception ex) {
                                                useCustomWidthHeightOfRectagle_BLACK = false;
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("調整紅框Size[w,h]", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String value = JCommonUtil._jOptionPane_showInputDialog("調整紅框Size[w,h]", widthOfRectagle_RED + "," + heightOfRectagle_RED);
                                                String[] arry = value.split(",", -1);
                                                if (arry.length == 2) {
                                                    widthOfRectagle_RED = Integer.parseInt(StringUtils.trimToEmpty(arry[0]));
                                                    heightOfRectagle_RED = Integer.parseInt(StringUtils.trimToEmpty(arry[1]));
                                                    useCustomWidthHeightOfRectagle_RED = true;
                                                }
                                                reflectUIToConfigAndStore();
                                            } catch (Exception ex) {
                                                useCustomWidthHeightOfRectagle_RED = false;
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("設定出現間隔(毫秒)", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String text = JCommonUtil._jOptionPane_showInputDialog("設定出現間隔(毫秒)", String.valueOf(rectagleInterval));
                                                rectagleInterval = Long.parseLong(StringUtils.trimToEmpty(text));
                                                JCommonUtil._jOptionPane_showMessageDialog_info("已設定為 : " + rectagleInterval);
                                                started = false;
                                                reflectUIToConfigAndStore();
                                            } catch (Exception ex) {
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("設定綠色方塊顏色", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String text = StringUtils.trimToEmpty(JCommonUtil._jOptionPane_showInputDialog("設定綠色方塊顏色", colorOfGreen));
                                                if (StringUtils.isNotBlank(text)) {
                                                    colorOfGreen = text;
                                                    reflectUIToConfigAndStore();
                                                }
                                            } catch (Exception ex) {
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("設定黑色方塊顏色", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String text = StringUtils.trimToEmpty(JCommonUtil._jOptionPane_showInputDialog("設定黑色方塊顏色", colorOfCube));
                                                if (StringUtils.isNotBlank(text)) {
                                                    colorOfCube = text;
                                                    useRandomColor = false;
                                                    useRandomColor2 = false;
                                                    reflectUIToConfigAndStore();
                                                }
                                            } catch (Exception ex) {
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("設定紅色方塊顏色", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String text = StringUtils.trimToEmpty(JCommonUtil._jOptionPane_showInputDialog("設定紅色方塊顏色", colorOfRed));
                                                if (StringUtils.isNotBlank(text)) {
                                                    colorOfRed = text;
                                                    useRandomColor = false;
                                                    useRandomColor2 = false;
                                                    reflectUIToConfigAndStore();
                                                }
                                            } catch (Exception ex) {
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("使用隨機顏色(true/false)", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String text = StringUtils.trimToEmpty(JCommonUtil._jOptionPane_showInputDialog("使用隨機顏色(true/false)", String.valueOf(useRandomColor)));
                                                if (StringUtils.isNotBlank(text)) {
                                                    useRandomColor = Boolean.parseBoolean(StringUtils.trimToEmpty(text));
                                                    reflectUIToConfigAndStore();
                                                }
                                            } catch (Exception ex) {
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .addJMenuItem("使用隨機顏色2(true/false)", new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            try {
                                                String text = StringUtils.trimToEmpty(JCommonUtil._jOptionPane_showInputDialog("使用隨機顏色2(true/false)", String.valueOf(useRandomColor2)));
                                                if (StringUtils.isNotBlank(text)) {
                                                    useRandomColor2 = Boolean.parseBoolean(StringUtils.trimToEmpty(text));
                                                    reflectUIToConfigAndStore();
                                                }
                                            } catch (Exception ex) {
                                                JCommonUtil.handleException(ex);
                                            }
                                        }
                                    })//
                                    .show();
                        }

                        if (JMouseEventUtil.buttonLeftClick(2, e)) {
                            if (!started) {
                                doStartOrStop(true);
                            }
                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {
                        System.out.println(e.getX() + "/" + e.getY());
                        mousePressEvent = e;

                        // if (clickMode) {
                        if (mRectagleHolder.checkMouseEvent_forClickMode(e)) {
                            backgroundApmCounter.correctTime++;
                        } else {
                            backgroundApmCounter.incorrectTime++;
                        }
                        // }
                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {
                        System.out.println(e.getX() + "/" + e.getY());

                        drawReleaseRectagle(e, mousePressEvent);

                        // if (clickMode) {
                        // return;
                        // }

                        if (mRectagleHolder.checkMouseEvent(e, mousePressEvent)) {
                            backgroundApmCounter.correctTime++;
                        } else {
                            backgroundApmCounter.incorrectTime++;
                        }
                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                    }
                });

                jPanel1.addMouseMotionListener(new MouseMotionListener() {
                    List<MouseEvent> allMoveList = new ArrayList<MouseEvent>();

                    void drawReleaseRectagle_forDrag(MouseEvent m1, MouseEvent m2) {
                        int x = Math.min(m1.getX() + 5, m2.getX() + 5);
                        int y = Math.min(m1.getY() + 25, m2.getY() + 25);
                        int w = Math.max(m1.getX() + 5, m2.getX() + 5) - x;
                        int h = Math.max(m1.getY() + 25, m2.getY() + 25) - y;
                        final RoundRectangle2D rect = new RoundRectangle2D.Double(x, y, w, h, 0, 0);
                        // g2dWhiteDraw(rect, null);
                    }

                    void drawReleaseRectagle_forNew(MouseEvent m1, MouseEvent m2) {
                        int x = Math.min(m1.getX() + 5, m2.getX() + 5);
                        int y = Math.min(m1.getY() + 25, m2.getY() + 25);
                        int w = Math.max(m1.getX() + 5, m2.getX() + 5) - x;
                        int h = Math.max(m1.getY() + 25, m2.getY() + 25) - y;
                        final RoundRectangle2D rect = new RoundRectangle2D.Double(x, y, w, h, 0, 0);

                        Rectangle rect2 = new Rectangle();
                        rect2.x = (int) (x);
                        rect2.y = (int) (y);
                        rect2.width = (int) (w);
                        rect2.height = (int) (h);

                        g2dGreenDraw(rect, rect2);
                    }

                    @Override
                    public void mouseDragged(MouseEvent e) {
                        // 是否要執行拖曳模式
                        if (false) {
                            return;
                        }

                        if (g2dGreen == null || g2dWhite == null || g2dBlack == null) {
                            return;
                        }

                        for (int ii = 0; ii < allMoveList.size(); ii++) {
                            MouseEvent e2 = allMoveList.get(ii);
                            drawReleaseRectagle_forDrag(e2, mousePressEvent);
                            allMoveList.remove(ii);
                            ii--;
                        }

                        allMoveList.add(e);
                        drawReleaseRectagle_forNew(e, mousePressEvent);
                    }

                    @Override
                    public void mouseMoved(MouseEvent e) {
                        if (g2dWhite == null) {
                            g2dWhite = (Graphics2D) getGraphics();
                            g2dWhite.setStroke(new BasicStroke(4.0f));
                        }
                        if (g2dBlack == null) {
                            g2dBlack = (Graphics2D) getGraphics();
                            g2dBlack.setStroke(new BasicStroke(4.0f));
                        }
                        if (g2dGreen == null) {
                            g2dGreen = (Graphics2D) getGraphics();
                            g2dGreen.setStroke(new BasicStroke(1.0f));
                            g2dGreen.setColor(Color.GREEN);
                        }
                    }
                });
            }

            this.addComponentListener(new ComponentListener() {

                @Override
                public void componentResized(ComponentEvent e) {
                    initDraw = false;
                }

                @Override
                public void componentMoved(ComponentEvent e) {
                }

                @Override
                public void componentShown(ComponentEvent e) {
                }

                @Override
                public void componentHidden(ComponentEvent e) {
                }
            });

            pack();
            setSize(567, 523);

            backgroundApmCounterInit();

            setLocationRelativeTo(null);

            JCommonUtil.setJFrameIcon(this, "resource/images/ico/starcraft.ico");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AlphaContainer extends JComponent {
        private JComponent component;

        public AlphaContainer(JComponent component) {
            this.component = component;
            this.component.setBackground(new Color(0, 0, 0, 0));// 0,0,0,0 全透明

            setLayout(new BorderLayout());
            setOpaque(false);
            component.setOpaque(false);
            add(component);
        }

        @Override
        public void paintComponent(Graphics g) {
            g.setColor(component.getBackground());
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    private class ModeHandler {
        int red = 1;
        int black = 2;
        int start = 0;
        int sum = 3;

        private void test() {
            ApmTrainerUI t = new ApmTrainerUI();
            ModeHandler t2 = t.new ModeHandler("8-2");
            int clickModeCount = 0;
            int notClickModeCount = 0;
            for (int ii = 0; ii < 10000; ii++) {
                boolean a = t2.isClickMode();
                if (a) {
                    clickModeCount++;
                } else {
                    notClickModeCount++;
                }
            }

            System.out.println("click = " + clickModeCount);
            System.out.println("no click = " + notClickModeCount);
        }

        private ModeHandler(String ratioOfCube) {
            String[] arry = ratioOfCube.split("-", -1);
            if (arry.length == 2) {
                red = Integer.parseInt(StringUtils.trimToEmpty(arry[0]));
                black = Integer.parseInt(StringUtils.trimToEmpty(arry[1]));
                start = 0;
                sum = red + black - 1;
            } else {
                throw new RuntimeException("定義失敗:" + ratioOfCube);
            }
        }

        private boolean isClickMode() {
            int mode = RandomUtil.rangeInteger(start, sum);
            if (mode < red) {
                System.out.println("紅");
                return true;
            }
            System.out.println("黑");
            return false;
        }
    }

    private class RectagleHolder {
        List<BlackCube> rectagleLst2 = new ArrayList<BlackCube>();
        List<RedCube> rectagleLst3 = new ArrayList<RedCube>();

        GreenCube green;

        private void createOne() {
            if ((rectagleLst2.size() + rectagleLst3.size()) > maxSizeOfRectagle) {
                return;
            }

            RoundRectangle2D rectagle = new RoundRectangle2D.Double(30, 50, 50, 50, 8, 8);

            this.randomRectagle(rectagle);
        }

        private void randomRectagle(RoundRectangle2D rectagle) {
            System.out.println("panel = " + jPanel1.getHeight() + "/" + jPanel1.getWidth());
            int w = jPanel1.getWidth() / circleSize;
            int h = jPanel1.getHeight() / circleSize;

            int GAP = 20;
            int x = RandomUtil.rangeInteger(5 + GAP, jPanel1.getWidth() - w - GAP);
            int y = RandomUtil.rangeInteger(25 + GAP, jPanel1.getHeight() - h - GAP);

            if (mModeHandler == null) {
                mModeHandler = new ModeHandler(ratioOfCube);
            }

            if (!mModeHandler.isClickMode()) {
                if (useCustomWidthHeightOfRectagle_BLACK) {
                    w = widthOfRectagle_BLACK;
                    h = heightOfRectagle_BLACK;
                }

                rectagle.setRoundRect(x, y, w, h, 8, 8);

                BlackCube cube = g2dBlackDraw(rectagle, null);

                rectagleLst2.add(cube);
            } else {
                if (useCustomWidthHeightOfRectagle_RED) {
                    w = widthOfRectagle_RED;
                    h = heightOfRectagle_RED;
                }

                rectagle.setRoundRect(x, y, w, h, 8, 8);

                RedCube cube = g2dRedDraw(rectagle, null);

                rectagleLst3.add(cube);
            }
        }

        private void removeRectagle(RoundRectangle2D rectagle) {
            g2dWhiteDraw(rectagle, null);
        }

        private boolean checkMouseEvent_forClickMode(MouseEvent m1) {
            if (!initDraw) {
                initDraw = true;
                return true;
            }

            int x = m1.getX();
            int y = m1.getY();

            boolean removeSuccess = false;

            int padding = 5;
            int yOffset = 0;

            for (int ii = 0; ii < rectagleLst3.size(); ii++) {
                RedCube cube = rectagleLst3.get(ii);
                RoundRectangle2D rectagle = cube.rectagle;
                int rectagleX = (int) rectagle.getX();
                int rectagleY = (int) rectagle.getY();
                int rectagleW = (int) rectagle.getWidth();
                int rectagleH = (int) rectagle.getHeight();

                int left = rectagleX - padding;
                int right = rectagleX + rectagleW + padding;
                int top = rectagleY + yOffset - padding;
                int buttom = rectagleY + rectagleH + yOffset + padding;

                System.out.println("左" + left + "右" + right + "上" + top + "下" + buttom);
                System.out.println("點" + x + "/" + y);

                if (left < x && right > x && top < y && buttom > y) {
                    rectagleLst3.remove(ii);
                    ii--;
                    this.removeRectagleCube(cube);
                    removeSuccess = true;
                }
            }

            return removeSuccess;
        }

        private void removeRectagleCube(MoveCube cube) {
            jPanel1.remove(cube);
            if (cube.timer != null) {
                cube.timer.cancel();
            }
        }

        private boolean checkMouseEvent(MouseEvent m1, MouseEvent m2) {
            if (!initDraw) {
                initDraw = true;
                return true;
            }

            int xOffset = 5;
            int yOffset = 0;

            int minX = Math.min(m1.getX() + xOffset, m2.getX() + xOffset);
            int minY = Math.min(m1.getY() + yOffset, m2.getY() + yOffset);

            int maxX = Math.max(m1.getX() + xOffset, m2.getX() + xOffset);
            int maxY = Math.max(m1.getY() + yOffset, m2.getY() + yOffset);

            boolean removeSuccess = false;

            for (int ii = 0; ii < rectagleLst2.size(); ii++) {
                BlackCube cube = rectagleLst2.get(ii);
                RoundRectangle2D rectagle = cube.rectagle;

                int rectagleX = (int) rectagle.getX();
                int rectagleY = (int) rectagle.getY();
                int rectagleW = (int) rectagle.getWidth();
                int rectagleH = (int) rectagle.getHeight();

                if (minX < rectagleX && minY < rectagleY//
                        && (rectagleX + rectagleW) < maxX && (rectagleY + rectagleH) < maxY) {
                    rectagleLst2.remove(ii);
                    ii--;
                    this.removeRectagleCube(cube);
                    removeSuccess = true;
                }
            }

            return removeSuccess;
        }
    }

    private void g2dWhiteDraw(RoundRectangle2D rectagle, Rectangle fillRect) {
        if (fillRect != null) {
            g2dWhite.fillRect((int) fillRect.getX(), (int) fillRect.getY(), (int) fillRect.getWidth(), (int) fillRect.getHeight());
        }
        g2dWhite.setColor(Color.WHITE);
        g2dWhite.draw(rectagle);
    }

    private Point getRandomMoveToPoint(int width1, int height1) {
        int startWidth = 20;
        int startHeight = 20;
        int width = jPanel1.getWidth() - 20;
        int height = jPanel1.getHeight() - 20;
        int targetWidth = RandomUtil.rangeInteger(startWidth, width - width1);
        int targetHeight = RandomUtil.rangeInteger(startHeight, height - height1);
        return new Point(targetWidth, targetHeight);
    }

    private int caculateFrame(Point start, Point end) {
        int startX = (int) start.getX();
        int startY = (int) start.getY();
        int endX = (int) end.getX();
        int endY = (int) end.getY();
        int a = Math.abs(startX - endX);
        int b = Math.abs(startY - endY);
        double c = Math.sqrt(a * a + b * b);
        double randOffset = RandomUtil.rangeDouble(-0.5, 0.5);
        return (int) (c * (1 + randOffset));
    }

    private RoundRectangle2D cloneRect(RoundRectangle2D rectagle) {
        return new RoundRectangle2D.Double(rectagle.getX(), rectagle.getY(), rectagle.getWidth(), rectagle.getHeight(), 0, 0);
    }

    private Point convert(RoundRectangle2D rectagle) {
        Point x1 = new Point((int) rectagle.getWidth(), (int) rectagle.getHeight());
        x1.x = (int) rectagle.getX();
        x1.y = (int) rectagle.getY();
        return x1;
    }

    private abstract class MoveCube extends JPanel {
        RoundRectangle2D rectagle;
        RoundRectangle2D orignRectagle;
        Timer timer;
        protected Color tempColor = null;
        LinearGradientColor linearGradientColor = new LinearGradientColor(100);

        private MoveCube(RoundRectangle2D rectagle) {
            this.rectagle = rectagle;
            this.orignRectagle = cloneRect(rectagle);
        }

        public void afterAdd() {
            int moveRate1 = RandomUtil.rangeInteger(0, 100);
            if (moveRate1 < rateOfMoveMode) {
                lineForwardAnime(this);
            }
        }

        @Override
        protected void paintComponent(Graphics g) {
            paintComponent((Graphics2D) g);
        }

        abstract void paintComponent(Graphics2D g);
    }

    private class RedCube extends MoveCube {
        private RedCube(RoundRectangle2D rectagle) {
            super(rectagle);
            this.setOpaque(false);
            tempColor = JColorUtil.rgb(colorOfRed);
            if (useRandomColor) {
                tempColor = JColorUtil.randomColor();
            }
        }

        protected void paintComponent(Graphics2D g) {
            g.setColor(tempColor);
            if (useRandomColor2) {
                g.setColor(linearGradientColor.get());
            }
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    private class BlackCube extends MoveCube {
        private BlackCube(RoundRectangle2D rectagle) {
            super(rectagle);
            this.setOpaque(false);
            tempColor = JColorUtil.rgb(colorOfCube);
            if (useRandomColor) {
                tempColor = JColorUtil.randomColor();
            }
        }

        protected void paintComponent(Graphics2D g) {
            g.setColor(tempColor);
            if (useRandomColor2) {
                g.setColor(linearGradientColor.get());
            }
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    private class GreenCube extends JPanel {
        private GreenCube() {
            this.setOpaque(false);
        }

        @Override
        protected void paintComponent(Graphics g) {
            paintComponent((Graphics2D) g);
        }

        protected void paintComponent(Graphics2D g) {
            g.setColor(JColorUtil.rgb(colorOfGreen, 64));
            g.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    private BlackCube g2dBlackDraw(RoundRectangle2D rectagle, Rectangle fillRect) {
        BlackCube cube = new BlackCube(rectagle);

        Rectangle rect = new Rectangle();
        rect.x = (int) rectagle.getX() + XXX;
        rect.y = (int) rectagle.getY() + YYY;
        rect.width = (int) rectagle.getWidth() + XXX;
        rect.height = (int) rectagle.getHeight() + YYY;
        cube.setBounds(rect);

        jPanel1.add(cube);
        cube.afterAdd();

        jPanel1.invalidate();
        jPanel1.repaint();

        return cube;
    }

    private RedCube g2dRedDraw(RoundRectangle2D rectagle, Rectangle fillRect) {
        RedCube cube = new RedCube(rectagle);

        Rectangle rect = new Rectangle();
        rect.x = (int) rectagle.getX() + XXX;
        rect.y = (int) rectagle.getY() + YYY;
        rect.width = (int) rectagle.getWidth() + XXX;
        rect.height = (int) rectagle.getHeight() + YYY;
        cube.setBounds(rect);

        jPanel1.add(cube);
        cube.afterAdd();

        jPanel1.invalidate();
        jPanel1.repaint();

        return cube;
    }

    private void g2dBlackDraw2(RoundRectangle2D rectagle, Rectangle fillRect) {
        if (fillRect != null) {
            g2dBlack.fillRect((int) fillRect.getX(), (int) fillRect.getY(), (int) fillRect.getWidth(), (int) fillRect.getHeight());
        }
        g2dBlack.setColor(Color.BLACK);
        g2dBlack.draw(rectagle);
    }

    private void g2dGreenDraw(RoundRectangle2D rectagle, Rectangle fillRect) {
        if (mRectagleHolder.green == null) {
            mRectagleHolder.green = new GreenCube();
            jPanel1.add(mRectagleHolder.green);
        }

        mRectagleHolder.green.setVisible(true);

        Rectangle rect = new Rectangle();
        rect.x = (int) rectagle.getX() + XXX;
        rect.y = (int) rectagle.getY() + YYY;
        rect.width = (int) rectagle.getWidth() + XXX;
        rect.height = (int) rectagle.getHeight() + YYY;
        mRectagleHolder.green.setBounds(rect);

        jPanel1.invalidate();
        jPanel1.repaint();
    }

    private void g2dGreenHidden() {
        if (mRectagleHolder.green == null) {
            return;
        }
        mRectagleHolder.green.setVisible(false);
    }

    private void g2dGreenDraw2(RoundRectangle2D rectagle, Rectangle fillRect) {
        if (fillRect != null) {
            g2dGreen.fillRect((int) fillRect.getX(), (int) fillRect.getY(), (int) fillRect.getWidth(), (int) fillRect.getHeight());
        }
        g2dGreen.draw(rectagle);
    }

    private void doStartOrStop(boolean start) {
        if (start == true) {
            started = true;
            this.isContinueTimer = true;
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    mRectagleHolder.createOne();
                    if (started == false) {
                        timer.cancel();
                    }
                }
            }, new Date(), rectagleInterval);
        } else {
            started = false;
            this.isContinueTimer = false;
        }
    }

    void backgroundApmCounterInit() {
        if (backgroundApmCounter != null) {
            // backgroundApmCounter.thread.interrupt();
            backgroundApmCounter.executeThread = false;
            try {
                Thread.sleep(1000);
            } catch (Exception ex) {
            }
        }
        backgroundApmCounter = new ApmCounter();
        backgroundApmCounter.thread = new Thread(new Runnable() {
            @Override
            public void run() {
                long currentTime = System.currentTimeMillis();

                long todayFailClickZ = todayFailClick;
                long todaySuccessClickZ = todaySuccessClick;
                long todayTotalClickZ = todayTotalClick;
                long todayDuringZ = todayDuring;

                while (backgroundApmCounter.executeThread) {
                    try {
                        Thread.sleep(500);

                        if (!isContinueTimer) {
                            continue;
                        }

                        if (!thisTimeDuringLst.isEmpty()) {
                            long totalDuringXX = 0;
                            for (long addVal : thisTimeDuringLst) {
                                totalDuringXX += addVal;
                            }
                            thisTimeDuringLst.clear();
                            thisTimeDuringPadding = totalDuringXX;
                            currentTime = System.currentTimeMillis();
                        }

                        thisTimeDuring = System.currentTimeMillis() - currentTime + thisTimeDuringPadding;

                        float betweenSec = (float) thisTimeDuring / 1000;

                        thisTimeSuccessClick = backgroundApmCounter.correctTime;
                        thisTimeFailClick = backgroundApmCounter.incorrectTime;
                        thisTimeTotalClick = thisTimeSuccessClick + thisTimeFailClick;

                        float total = ((float) (backgroundApmCounter.correctTime + backgroundApmCounter.incorrectTime)) / betweenSec * 60;
                        float correct = ((float) (backgroundApmCounter.correctTime)) / betweenSec * 60;
                        float incorrect = ((float) (backgroundApmCounter.incorrectTime)) / betweenSec * 60;

                        setTitle("APM:" + (int) total + //
                        ", EAPM:" + (int) correct + //
                        ", 無效APM:" + (int) incorrect + //

                        ", 本次時長:" + DateUtil.wasteTotalTime(thisTimeDuring) + //
                        ", 本日時長:" + DateUtil.wasteTotalTime(todayDuringZ + thisTimeDuring) + //

                        ", id:" + backgroundApmCounter.thread.getId() //
                        );
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        backgroundApmCounter.thread.start();

        if (backgroundApmCounter.timer != null) {
            backgroundApmCounter.timer.cancel();
        }
        backgroundApmCounter.timer = new Timer();
        backgroundApmCounter.timer.schedule(new TimerTask() {
            @Override
            public void run() {
                operateConfig.setPropertyNullIsEmpty(getToday() + "_FAIL", String.valueOf(todayFailClick + thisTimeFailClick));
                operateConfig.setPropertyNullIsEmpty(getToday() + "_SUCCESS", String.valueOf(todaySuccessClick + thisTimeSuccessClick));
                operateConfig.setPropertyNullIsEmpty(getToday() + "_TOTAL_CLICK", String.valueOf(todayTotalClick + thisTimeTotalClick));
                operateConfig.setPropertyNullIsEmpty(getToday() + "_DURING", String.valueOf(todayDuring + thisTimeDuring));
                operateConfig.store();
            }
        }, new Date(), 5 * 1000);
    }

    private void lineForwardAnime(final MoveCube cube) {
        final Point startPoint = convert(cube.rectagle);

        final Point newPoint = getRandomMoveToPoint((int) cube.rectagle.getWidth(), (int) cube.rectagle.getHeight());

        final int frames = caculateFrame(startPoint, newPoint);

        final int interval = 5;

        System.out.println("---------------------------");
        System.out.println("startPoint = " + startPoint);
        System.out.println("newPoint = " + newPoint);
        System.out.println("frames = " + frames);
        System.out.println("interval = " + interval);

        final Rectangle compBounds = cube.getBounds();

        final Point oldPoint = new Point(compBounds.x, compBounds.y); //

        final double divideX = ((double) newPoint.x - (double) oldPoint.x) / (double) frames;
        final double divideY = ((double) newPoint.y - (double) oldPoint.y) / (double) frames;

        System.out.println(" divide X = " + divideX);
        System.out.println(" divide Y = " + divideY);

        cube.timer = new Timer();
        cube.timer.schedule(new TimerTask() {
            int currentFrame = 0;

            @Override
            public void run() {
                try {
                    double newX = ((double) compBounds.x + ((double) divideX * (double) currentFrame));
                    double newY = ((double) compBounds.y + ((double) divideY * (double) currentFrame));

                    cube.setBounds((int) newX, //
                            (int) newY, //
                            compBounds.width, //
                            compBounds.height);//

                    cube.rectagle.setFrame(newX, newY, compBounds.width, compBounds.height);

                    if (currentFrame != frames) {
                        currentFrame += 1;
                    } else {
                        cube.timer.cancel();
                        lineForwardAnime(cube);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, 100, interval);
    }
}