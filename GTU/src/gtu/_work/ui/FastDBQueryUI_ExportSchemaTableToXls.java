package gtu._work.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import gtu.binary.StringUtil4FullChar;
import gtu.date.DateUtil;
import gtu.db.JdbcDBUtil;
import gtu.db.jdbc.util.DBDateUtil;
import gtu.db.sqlMaker.DbSqlCreater;
import gtu.db.sqlMaker.DbSqlCreater.TableInfo;
import gtu.file.FileUtil;
import gtu.poi.hssf.ExcelColorCreater;
import gtu.poi.hssf.ExcelUtil_Xls97;
import gtu.poi.hssf.ExcelWriter;
import gtu.poi.hssf.ExcelWriter.CellStyleHandler;
import gtu.string.StringUtil_;
import gtu.swing.util.JCommonUtil;
import gtu.swing.util.JProgressBarHelper;

public class FastDBQueryUI_ExportSchemaTableToXls {
    final ExcelUtil_Xls97 exlUtl = ExcelUtil_Xls97.getInstance();

    private class DataDetectProcess {
        int excelPos = -1;
        int maxLength = -1;
        String columnName;
        String dataType;
    }

    Pattern functionPtn = Pattern.compile("\\w+\\((.*)\\)");
    Pattern datePtn = Pattern.compile("\\d{4}\\-\\d{2}\\-\\d{2}");
    Pattern datetimePtn = Pattern.compile("^\\d{4}\\-\\d{2}\\-\\d{2}\\s(\\d{2}\\:\\d{2}\\:\\d{2})");

    private int caculateVarchar2Length(int length) {
        BigDecimal v1 = new BigDecimal(length);
        BigDecimal v2 = v1.divide(new BigDecimal(10), BigDecimal.ROUND_UP);
        return (v2.intValue() + 1) * 10;
    }

    public static enum DBType {
        Qracle("date", "timestamp", "number(20, 2)", "varchar2"), //
        Sqlite("DATETIME", "DATETIME", "NUMERIC(10,2)", "NVARCHAR"),// number=INTEGER
                                                                    // or
                                                                    // NUMERIC(10,2)
        ;

        final String date;
        final String datetime;
        final String number;
        final String varchar;

        DBType(String date, String datetime, String number, String varchar) {
            this.date = date;
            this.datetime = datetime;
            this.number = number;
            this.varchar = varchar;
        }

        public String toString() {
            return name();
        }
    }

    private String generateCreateTableSQL(String tableSchema, DBType mDBType, Sheet sheet, final JProgressBarHelper prog) {
        Row titleRow = sheet.getRow(0);
        Map<Integer, DataDetectProcess> map = new LinkedHashMap<Integer, DataDetectProcess>();

        for (int ii = 0; ii <= titleRow.getLastCellNum(); ii++) {
            DataDetectProcess mDataDetectProcess = new DataDetectProcess();
            mDataDetectProcess.excelPos = ii;
            String value = ExcelUtil_Xls97.getInstance().readCell(exlUtl.getCellChk(titleRow, ii));
            if (StringUtils.isBlank(value)) {
                break;
            }
            mDataDetectProcess.columnName = value;
            map.put(ii, mDataDetectProcess);

            if (prog.isExitFlag()) {
                return " [ERROR : 操作中斷!] ";
            }
        }

        int specsCount = 0;

        for (int ii = 1; ii <= sheet.getLastRowNum(); ii++) {
            Row row = sheet.getRow(ii);
            if (row == null) {
                continue;
            }
            for (int jj = 0; jj < row.getLastCellNum(); jj++) {
                String value = ExcelUtil_Xls97.getInstance().readCell(row.getCell(jj));
                if (map.containsKey(jj)) {
                    DataDetectProcess mDataDetectProcess = map.get(jj);
                    mDataDetectProcess.maxLength = Math.max(mDataDetectProcess.maxLength, StringUtil4FullChar.length(value));

                    if (StringUtils.isNotBlank(value) && mDataDetectProcess.dataType == null) {
                        boolean defineOk = false;

                        String trimValue = StringUtils.trimToEmpty(value);

                        if (DateUtil.isUsingDateFormat_USA(trimValue)) {
                            value = DateUtil.filterUSADateString(trimValue);
                            mDataDetectProcess.dataType = mDBType.datetime;
                        }

                        Matcher mth1 = null;

                        if (!defineOk) {
                            mth1 = datetimePtn.matcher(trimValue);
                            if (mth1.find()) {
                                if ("00:00:00".equals(mth1.group(1))) {
                                    value = trimValue;
                                    mDataDetectProcess.dataType = mDBType.date;
                                    defineOk = true;
                                } else {
                                    value = trimValue;
                                    mDataDetectProcess.dataType = mDBType.datetime;
                                    defineOk = true;
                                }
                            }
                        }

                        if (!defineOk) {
                            mth1 = datePtn.matcher(trimValue);
                            if (mth1.matches()) {
                                if ("00:00:00".equals(mth1.group(1))) {
                                    value = trimValue;
                                    mDataDetectProcess.dataType = mDBType.date;
                                    defineOk = true;
                                }
                            }
                        }

                        if (!defineOk) {
                            mDataDetectProcess.dataType = mDBType.varchar;
                            defineOk = true;
                        }
                    }

                    if (prog.isExitFlag()) {
                        return " [ERROR : 操作中斷!] ";
                    }
                }
            }
            specsCount++;
        }
        return this.processCreateTableSql(specsCount, tableSchema, map, mDBType);
    }

    private String processCreateTableSql(int specsCount, String tableSchema, Map<Integer, DataDetectProcess> map, DBType mDBType) {
        StringBuffer sb = new StringBuffer();
        sb.append("--樣本採集數 : " + specsCount + "\r\n");
        sb.append("    create table " + tableSchema + " ( \r\n");
        List<Integer> keys = new ArrayList<Integer>(map.keySet());
        for (int ii = 0; ii < keys.size(); ii++) {
            Integer key = keys.get(ii);
            DataDetectProcess mDataDetectProcess = map.get(key);
            if (specsCount == 0 || //
                    mDataDetectProcess.dataType == null) {
                mDataDetectProcess.dataType = mDBType.varchar;
                mDataDetectProcess.maxLength = 50;
            }
            sb.append("        ").append(mDataDetectProcess.columnName).append(" ");
            if (StringUtils.equals(mDataDetectProcess.dataType, mDBType.varchar)) {
                sb.append(mDataDetectProcess.dataType);
                int maxLength = caculateVarchar2Length(mDataDetectProcess.maxLength);
                sb.append("(").append(maxLength).append(")");
            } else {
                sb.append(mDataDetectProcess.dataType);
            }
            if (ii != keys.size() - 1) {
                sb.append(" ,\r\n");
            } else {
                sb.append(" \r\n");
            }
        }
        sb.append("    );");
        return sb.toString();
    }

    private class ExportInsertSql4Dump {
        String tableSchema;
        Sheet sheet;
        TableInfo tableInfo;
        final JProgressBarHelper prog;

        Map<Integer, String> map = new LinkedHashMap<Integer, String>();

        ExportInsertSql4Dump(String tableSchema, Sheet sheet, DBDateUtil.DBDateFormat mDBType, final JProgressBarHelper prog, FastDBQueryUI self) throws SQLException {
            this.tableSchema = tableSchema;
            this.sheet = sheet;
            this.prog = prog;

            tableInfo = new DbSqlCreater.TableInfo();
            tableInfo.setDbDateDateFormat(mDBType);
            tableInfo.execute("select * from " + tableSchema + " where 1!=1 ", self.getDataSource().getConnection());
            tableInfo.setTableAndSchema(tableSchema, false);

            {
                Row titleRow = sheet.getRow(0);
                for (int ii = 0; ii <= titleRow.getLastCellNum(); ii++) {
                    String value = ExcelUtil_Xls97.getInstance().readCell(exlUtl.getCellChk(titleRow, ii));
                    if (StringUtils.isBlank(value)) {
                        break;
                    }
                    map.put(ii, value);
                }
            }
        }

        String applyRow(Row row) {
            Map<String, String> map22 = new LinkedHashMap<String, String>();
            for (int jj = 0; jj < row.getLastCellNum(); jj++) {
                String value = ExcelUtil_Xls97.getInstance().readCell(row.getCell(jj));
                if (map.containsKey(jj)) {
                    String column = map.get(jj);
                    map22.put(column, value);
                }
            }

            String sql = tableInfo.createInsertSql(map22, Collections.EMPTY_SET, true);

            if (prog.isExitFlag()) {
                return " [ERROR : 操作中斷!] ";
            }
            return sql;
        }
    }

    public void exportCurrentSystemCreateSQL(final DBType mDBType, final FastDBQueryUI self) {
        final File xlsSqlDir = new File(FileUtil.DESKTOP_DIR, "FastDBQueryUI_ExportSchema_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd") + File.separator + "CREATE_TABLE_SQL");
        final File xlsDir = new File(FileUtil.DESKTOP_DIR, "FastDBQueryUI_ExportSchema_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd"));
        xlsDir.mkdirs();
        xlsSqlDir.mkdirs();
        try {
            final List<String> tabNameLst = tableNameSchemaLst();

            if (tabNameLst == null) {
                return;
            }

            // ====================== START ======================

            final JProgressBarHelper prog = JProgressBarHelper.newInstance(self, "正在匯出Schema");
            prog.closeListenerDefault();
            prog.indeterminate(true);
            prog.build();
            prog.show();

            final List<String> logLst = new ArrayList<String>();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (final String tabName : tabNameLst) {
                        prog.setStateText(tabName + "....start");
                        try {
                            File tableFile = new File(xlsDir, tabName + ".xls");
                            if (tableFile.exists()) {
                                Workbook wb = exlUtl.readExcel(tableFile);
                                Sheet sheet = wb.getSheetAt(0);
                                String createSQL = generateCreateTableSQL(tabName, mDBType, sheet, prog);
                                FileUtil.saveToFile(new File(xlsSqlDir, tabName + ".sql"), createSQL, "UTF8");
                                logLst.add(tabName + "\tsuccess");
                            } else {
                                logLst.add(tabName + "\terror: excel not found");
                            }

                            if (prog.isExitFlag()) {
                                logLst.add(tabName + "\terror:" + " [操作中斷!] ");
                                break;
                            }

                            prog.addOne();
                        } catch (Exception e) {
                            e.printStackTrace();
                            logLst.add(tabName + "\terror:" + e.getMessage());
                        }
                    }
                    FileUtil.saveToFile(new File(xlsSqlDir, "log.txt"), StringUtils.join(logLst, "\r\n"), "UTF8");
                    prog.dismiss();
                }
            }, "---thread1").start();
        } catch (Exception ex) {
            JCommonUtil.handleException(ex);
        }
    }

    public void exportCurrentSystemToInsertSQL(final DBDateUtil.DBDateFormat mDBType, final FastDBQueryUI self) {
        final File xlsSqlDir = new File(FileUtil.DESKTOP_DIR, "FastDBQueryUI_ExportSchema_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd") + File.separator + "INSERT_SQL");
        final File xlsDir = new File(FileUtil.DESKTOP_DIR, "FastDBQueryUI_ExportSchema_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd"));
        xlsDir.mkdirs();
        xlsSqlDir.mkdirs();
        try {
            final List<String> tabNameLst = tableNameSchemaLst();

            if (tabNameLst == null) {
                return;
            }

            // ====================== START ======================

            final JProgressBarHelper prog = JProgressBarHelper.newInstance(self, "正在匯出Schema");
            prog.indeterminate(true);
            prog.build();
            prog.show();

            final List<String> logLst = new ArrayList<String>();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    A: for (final String tabName : tabNameLst) {
                        prog.setStateText(tabName + "....start");
                        try {
                            File tableFile = new File(xlsDir, tabName + ".xls");
                            if (tableFile.exists()) {
                                Workbook wb = exlUtl.readExcel(tableFile);
                                Sheet sheet = wb.getSheetAt(0);

                                ExportInsertSql4Dump mExportInsertSql4Dump = new ExportInsertSql4Dump(tabName, sheet, mDBType, prog, self);

                                StringBuffer sb = new StringBuffer();

                                int totalSize = 0;

                                B: for (int ii = 1; ii <= sheet.getLastRowNum(); ii++) {
                                    Row row = sheet.getRow(ii);
                                    if (row == null) {
                                        continue;
                                    }
                                    String sql = mExportInsertSql4Dump.applyRow(row);
                                    sb.append(sql).append(" ; \r\n");
                                    totalSize++;

                                    if (prog.isExitFlag()) {
                                        break B;
                                    }
                                }

                                FileUtil.saveToFile(new File(xlsSqlDir, tabName + ".sql"), sb.toString(), "UTF8");

                                logLst.add(tabName + "\tsuccess size:" + totalSize);

                                if (prog.isExitFlag()) {
                                    break A;
                                }
                            } else {
                                logLst.add(tabName + "\terror: excel not found");
                            }

                            if (prog.isExitFlag()) {
                                logLst.add(tabName + "\terror:" + " [操作中斷!] ");
                                break;
                            }

                            prog.addOne();
                        } catch (Exception e) {
                            e.printStackTrace();
                            logLst.add(tabName + "\terror:" + e.getMessage());
                        }
                    }
                    FileUtil.saveToFile(new File(xlsSqlDir, "log.txt"), StringUtils.join(logLst, "\r\n"), "UTF8");
                    prog.dismiss();
                }
            }, "---thread1").start();
        } catch (Exception ex) {
            JCommonUtil.handleException(ex);
        }
    }

    public void exportCurrentSystemToExcel(final FastDBQueryUI self) {
        final File xlsDir = new File(FileUtil.DESKTOP_DIR, "FastDBQueryUI_ExportSchema_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd"));
        xlsDir.mkdirs();
        try {
            final List<String> tabNameLst = tableNameSchemaLst();

            if (tabNameLst == null) {
                return;
            }

            // ====================== START ======================

            final JProgressBarHelper prog = JProgressBarHelper.newInstance(self, "正在匯出Schema");
            prog.closeListenerDefault();
            prog.indeterminate(true);
            prog.build();
            prog.show();

            final List<String> logLst = new ArrayList<String>();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    A: for (final String tabName : tabNameLst) {
                        prog.setStateText(tabName + "....start");
                        File tableXlsFile = new File(xlsDir, tabName + ".xls");
                        if (tableXlsFile.exists()) {
                            logLst.add(tabName + "\tignore:" + "檔案已存在!");
                            continue;
                        }

                        final int maxRowsLimit = 0;
                        final String sql = "select * from " + tabName;
                        final ExcelWriterForDataRow mExcelWriterForDataRow = new ExcelWriterForDataRow(tableXlsFile, sql, tabName, false);
                        final AtomicInteger rowPos = new AtomicInteger(0);
                        final AtomicReference<String> prefixMessage = new AtomicReference<String>("");
                        try {
                            JdbcDBUtil.queryForList_customColumns_everyRows(sql, new Object[0], self.getDataSource().getConnection(), true, maxRowsLimit, new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    Triple<List<String>, List<Class<?>>, Object[]> source = (Triple<List<String>, List<Class<?>>, Object[]>) e.getSource();
                                    rowPos.set(Integer.parseInt(e.getActionCommand()));
                                    mExcelWriterForDataRow.applyRow(source, rowPos.get());
                                    prog.setStateText(tabName + "...." + (rowPos.get() + 1));
                                    if (prog.isExitFlag()) {
                                        e.setSource(false);
                                        prefixMessage.set(" [中斷未完成]: ");
                                    }
                                }
                            });

                            logLst.add(tabName + "\tsuccess:" + prefixMessage.get() + (rowPos.get() + 1));
                            if (prog.isExitFlag()) {
                                break A;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            logLst.add(tabName + "\terror:" + e.getMessage());
                        } finally {
                            try {
                                mExcelWriterForDataRow.finish();
                            } catch (Exception e2) {
                                logLst.add(tabName + "\terror:" + e2.getMessage());
                            }
                            prog.addOne();
                        }
                    }

                    FileUtil.saveToFile(new File(xlsDir, "log.txt"), StringUtils.join(logLst, "\r\n"), "UTF8");

                    prog.dismiss();
                }
            }, "---thread1").start();
        } catch (Exception ex) {
            JCommonUtil.handleException(ex);
        }
    }

    public void importInsertSQLToExcelNSchema(final FastDBQueryUI self, final String insertSqlFilePath, final String tableSchema, final DBType mDBType) {
        final File xlsDir = new File(FileUtil.DESKTOP_DIR, "FastDBQueryUI_ExportSchema_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd"));
        xlsDir.mkdirs();
        final File tableDir = new File(xlsDir, FileUtil.escapeFilename(tableSchema, true));
        tableDir.mkdirs();
        try {
            Validate.isTrue(StringUtils.isNotBlank(insertSqlFilePath), "檔案必須不為空");
            final File insertSqlFile = new File(StringUtils.trimToEmpty(insertSqlFilePath));
            Validate.isTrue(insertSqlFile.exists(), "檔案必須存在");
            Validate.isTrue(StringUtils.isNotBlank(tableSchema), "表必須不為空");
            Validate.isTrue(mDBType != null, "資料庫類型必須不為空");

            // ====================== START ======================

            final JProgressBarHelper prog = JProgressBarHelper.newInstance(self, "將InsertSQL產生schema");
            prog.closeListenerDefault();
            prog.indeterminate(true);
            prog.build();
            prog.show();

            final List<String> logLst = new ArrayList<String>();

            new Thread(new Runnable() {

                @Override
                public void run() {
                    try {

                        String sql = FileUtil.loadFromFile(insertSqlFile, "UTF8");
                        List<String> lines = StringUtil_.readContentToList(sql, true, true, false);

                        File excelFile = new File(tableDir, FileUtil.escapeFilename(tableSchema, true) + ".xls");

                        ExcelWriterForDataRow mExcelWriterForDataRow = new ExcelWriterForDataRow(excelFile, "", tableSchema, false);

                        Map<Integer, DataDetectProcess> mapx = new LinkedHashMap<Integer, DataDetectProcess>();

                        for (int jj = 0; jj < lines.size(); jj++) {
                            String line = lines.get(jj);
                            Map<String, String> map = FastDBQueryUI_InsertSqlParseToMap.parseSql(line);

                            List<String> columns = new ArrayList<String>(map.keySet());

                            for (int ii = 0; ii < columns.size(); ii++) {
                                String key = columns.get(ii);
                                String strVal = map.get(key);

                                DataDetectProcess tDataDetectProcess = new DataDetectProcess();
                                if (mapx.containsKey(ii)) {
                                    tDataDetectProcess = mapx.get(ii);
                                } else {
                                    mapx.put(ii, tDataDetectProcess);
                                }

                                tDataDetectProcess.columnName = key;

                                Pair<Boolean, String> filterResult = findDatetimeFormatString(strVal);
                                if (filterResult.getLeft()) {
                                    map.put(key, filterResult.getRight());
                                    tDataDetectProcess.dataType = mDBType.datetime;
                                } else {
                                    tDataDetectProcess.maxLength = Math.max(tDataDetectProcess.maxLength, StringUtil4FullChar.length(strVal));
                                }
                            }

                            mExcelWriterForDataRow.applyRow(columns, map.values().toArray(), jj);

                            if (prog.isExitFlag()) {
                                System.out.println("採取中斷操作 index : " + jj);
                                logLst.add("採取中斷操作 index : " + jj);
                                break;
                            }

                            prog.addOne();
                        }

                        String createSQL = processCreateTableSql(lines.size(), tableSchema, mapx, mDBType);
                        FileUtil.saveToFile(new File(tableDir, tableSchema + "_create.sql"), createSQL, "UTF8");

                        mExcelWriterForDataRow.finish();

                        FileUtil.saveToFile(new File(tableDir, "log.txt"), StringUtils.join(logLst, "\r\n"), "UTF8");
                    } catch (Exception ex) {
                        JCommonUtil.handleException(ex);
                    } finally {
                        prog.dismiss();
                    }
                }

            }, "---thread1").start();
        } catch (Exception ex) {
            JCommonUtil.handleException(ex);
        }
    }

    private Pair<Boolean, String> findDatetimeFormatString(String strVal) {
        Matcher mth = functionPtn.matcher(strVal);
        if (mth.find()) {
            String strVal1 = mth.group(1);
            Matcher mth1 = datetimePtn.matcher(strVal1);
            if (mth1.find()) {
                return Pair.of(true, mth1.group());
            }
            Matcher mth2 = datePtn.matcher(strVal1);
            if (mth2.find()) {
                return Pair.of(true, mth2.group());
            }
        }
        return Pair.of(false, strVal);
    }

    private class ExcelWriterForDataRow {
        final ExcelUtil_Xls97 exlUtl = ExcelUtil_Xls97.getInstance();
        File tableXls;
        String sql;
        String tableName;
        boolean nullIsEmpty;
        HSSFWorkbook wk = new HSSFWorkbook();
        boolean firstRowDone = false;
        HSSFSheet sheet1;
        HSSFSheet sheet0;
        int paddingCount = 1;
        boolean isFinishDone = false;

        ExcelWriterForDataRow(File tableXls, String sql, String tableName, boolean nullIsEmpty) {
            this.tableXls = tableXls;
            this.sql = sql;
            this.tableName = tableName;
            this.nullIsEmpty = nullIsEmpty;

            sheet1 = wk.createSheet("orign value sheet");
            sheet0 = wk.createSheet("string value sheet");

            // 寫sql
            {
                appendExcelSQLSheet(wk, sql);
            }
        }

        void applyRow(List<String> columns, Object[] rows, int rowIndex) {
            if (firstRowDone == false) {
                CellStyleHandler titleCs = ExcelWriter.CellStyleHandler.newInstance(wk.createCellStyle())//
                        .setForegroundColor(new HSSFColor.LIGHT_GREEN());
                HSSFRow titleRow0 = sheet0.createRow(0);
                HSSFRow titleRow1 = sheet1.createRow(0);

                for (int ii = 0; ii < columns.size(); ii++) {
                    exlUtl.setCellValue(exlUtl.getCellChk(titleRow0, ii), columns.get(ii));
                    titleCs.applyStyle(exlUtl.getCellChk(titleRow0, ii));
                }
                for (int ii = 0; ii < columns.size(); ii++) {
                    exlUtl.setCellValue(exlUtl.getCellChk(titleRow1, ii), columns.get(ii));
                    titleCs.applyStyle(exlUtl.getCellChk(titleRow1, ii));
                }
                firstRowDone = true;
            }

            // 寫資料
            Row row_string = sheet0.createRow(rowIndex + paddingCount);
            Row row_orign$ = sheet1.createRow(rowIndex + paddingCount);

            for (int jj = 0; jj < columns.size(); jj++) {
                String col = columns.get(jj);
                Object value = rows[jj];
                if (value == null && nullIsEmpty) {
                    value = "";
                }
                exlUtl.setCellValue(exlUtl.getCellChk(row_string, jj), String.valueOf(value));
                exlUtl.setCellValue(exlUtl.getCellChk(row_orign$, jj), value);
            }
        }

        void applyRow(Triple<List<String>, List<Class<?>>, Object[]> source, int rowIndex) {
            List<String> columns = new ArrayList<String>(source.getLeft());

            if (firstRowDone == false) {
                CellStyleHandler titleCs = ExcelWriter.CellStyleHandler.newInstance(wk.createCellStyle())//
                        .setForegroundColor(new HSSFColor.LIGHT_GREEN());
                HSSFRow titleRow0 = sheet0.createRow(0);
                HSSFRow titleRow1 = sheet1.createRow(0);

                for (int ii = 0; ii < columns.size(); ii++) {
                    exlUtl.setCellValue(exlUtl.getCellChk(titleRow0, ii), columns.get(ii));
                    titleCs.applyStyle(exlUtl.getCellChk(titleRow0, ii));
                }
                for (int ii = 0; ii < columns.size(); ii++) {
                    exlUtl.setCellValue(exlUtl.getCellChk(titleRow1, ii), columns.get(ii));
                    titleCs.applyStyle(exlUtl.getCellChk(titleRow1, ii));
                }
                firstRowDone = true;
            }

            // 寫資料
            Row row_string = sheet0.createRow(rowIndex + paddingCount);
            Row row_orign$ = sheet1.createRow(rowIndex + paddingCount);

            Object[] rows = source.getRight();
            for (int jj = 0; jj < columns.size(); jj++) {
                String col = columns.get(jj);
                Object value = rows[jj];
                if (value == null && nullIsEmpty) {
                    value = "";
                }
                exlUtl.setCellValue(exlUtl.getCellChk(row_string, jj), String.valueOf(value));
                exlUtl.setCellValue(exlUtl.getCellChk(row_orign$, jj), value);
            }
        }

        private void finish() {
            if (isFinishDone == false) {
                exlUtl.autoCellSize(sheet0);
                exlUtl.autoCellSize(sheet1);
                exlUtl.writeExcel(tableXls, wk);
                isFinishDone = true;
            }
        }
    }

    private void appendExcelSQLSheet(HSSFWorkbook wk, String sql) {
        final ExcelUtil_Xls97 exlUtl = ExcelUtil_Xls97.getInstance();
        HSSFSheet sheet2 = wk.createSheet("sql");
        ExcelColorCreater mExcelColorCreater = ExcelColorCreater.newInstance(wk);
        Row sqlRow = exlUtl.getRowChk(sheet2, 0);
        Cell sqlCell = exlUtl.getCellChk(sqlRow, 0);
        sqlCell.setCellValue(sql);
        Cell sqlCell2 = exlUtl.getCellChk(sqlRow, 1);
        sqlCell2.setCellValue(sql);
        sqlRow.setHeight((short) -1);
        CellStyle rowHeightStyle = wk.createCellStyle();
        rowHeightStyle.setWrapText(true);
        sqlRow.setRowStyle(rowHeightStyle);
        exlUtl.setSheetWidth(sheet2, new short[] { 8000, 8000 });
        exlUtl.applyAutoHeight(sheet2, wk);
    }

    private List<String> tableNameSchemaLst() {
        File xlsFile = JCommonUtil._jFileChooser_selectFileOnly();
        final List<String> tabNameLst = new ArrayList<String>();
        if (xlsFile.getName().toLowerCase().endsWith("xls")) {
            boolean SKIP_FIRST_ROW = false;
            int startRow = 0;
            if (SKIP_FIRST_ROW) {
                startRow = 1;
            }
            Workbook wb = exlUtl.readExcel(xlsFile);
            Sheet sheet = wb.getSheetAt(0);

            for (int ii = startRow; ii <= sheet.getLastRowNum(); ii++) {
                Row row = sheet.getRow(ii);
                if (row == null) {
                    continue;
                }
                String schema = StringUtils.trimToEmpty(exlUtl.readCell(exlUtl.getCellChk(row, 0)));
                String tableName = StringUtils.trimToEmpty(exlUtl.readCell(exlUtl.getCellChk(row, 1)));
                if (StringUtils.isNotBlank(schema)) {
                    schema = schema + ".";
                }
                tabNameLst.add(schema + tableName);
            }
        } else if (xlsFile.getName().toLowerCase().endsWith("txt")) {
            List<String> lst = FileUtil.loadFromFile_asList(xlsFile, "UTF8");
            for (String tab : lst) {
                tab = StringUtils.trimToEmpty(tab);
                if (!tabNameLst.contains(tab)) {
                    tabNameLst.add(tab);
                }
            }
        } else {
            JCommonUtil._jOptionPane_showMessageDialog_error("檔案格式錯誤!");
            return null;
        }
        return tabNameLst;
    }
}
