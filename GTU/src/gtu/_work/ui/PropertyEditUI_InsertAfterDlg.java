package gtu._work.ui;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import gtu.file.FileUtil;
import gtu.properties.PropertiesUtil;
import gtu.swing.util.JCommonUtil;

public class PropertyEditUI_InsertAfterDlg extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private JTextField insertAfterKeyText;
    private JTextField keyText;
    private JTextArea valueArea;
    private DontMoveFileAddRow mDontMoveFileAddRow;

    /**
     * Launch the application.
     */
    public static PropertyEditUI_InsertAfterDlg newInstacne(File file) {
        PropertyEditUI_InsertAfterDlg dialog = new PropertyEditUI_InsertAfterDlg();
        try {
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
            dialog.mDontMoveFileAddRow = new DontMoveFileAddRow();
            dialog.mDontMoveFileAddRow.init(file);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return dialog;
    }

    /**
     * Create the dialog.
     */
    public PropertyEditUI_InsertAfterDlg() {
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));
        {
            JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
            contentPanel.add(tabbedPane, BorderLayout.CENTER);
            {
                JPanel panel = new JPanel();
                tabbedPane.addTab("New tab", null, panel, null);
                panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
                        new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                                RowSpec.decode("default:grow"), }));
                {
                    JLabel lblNewLabel = new JLabel("insert after key");
                    panel.add(lblNewLabel, "2, 2, right, default");
                }
                {
                    insertAfterKeyText = new JTextField();
                    panel.add(insertAfterKeyText, "4, 2, fill, default");
                    insertAfterKeyText.setColumns(10);
                    insertAfterKeyText.addFocusListener(new FocusAdapter() {
                        @Override
                        public void focusLost(FocusEvent e) {
                            String text = insertAfterKeyText.getText();
                            mDontMoveFileAddRow.init(null);
                            String value = mDontMoveFileAddRow.loadValue(text);
                            if (StringUtils.isNotBlank(value)) {
                                valueArea.setText(value);
                            }
                        }
                    });
                }
                {
                    JLabel lblNewLabel_1 = new JLabel("key");
                    panel.add(lblNewLabel_1, "2, 4, right, default");
                }
                {
                    keyText = new JTextField();
                    panel.add(keyText, "4, 4, fill, default");
                    keyText.setColumns(10);
                }
                {
                    JLabel lblNewLabel_2 = new JLabel("value");
                    panel.add(lblNewLabel_2, "2, 6");
                }
                {
                    valueArea = new JTextArea();
                    JCommonUtil.applyDropFiles(valueArea, new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            List<File> lst = (List<File>) e.getSource();
                            if (!lst.isEmpty()) {
                                String content = FileUtil.loadFromFile(lst.get(0), "UTF8");
                                valueArea.setText(content);
                            }
                        }
                    });

                    panel.add(JCommonUtil.createScrollComponent(valueArea), "4, 6, fill, fill");
                }
            }
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton insertButton = new JButton("插入");
                insertButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        try {
                            String insertAfterKey = insertAfterKeyText.getText();
                            String key = keyText.getText();
                            String value = valueArea.getText();
                            if (StringUtils.isBlank(insertAfterKey) || StringUtils.isBlank(key)) {
                                JCommonUtil._jOptionPane_showMessageDialog_error("必輸輸入鍵直!");
                                return;
                            }
                            mDontMoveFileAddRow.init(null);
                            mDontMoveFileAddRow.insertAfter(insertAfterKey, key, value);
                        } catch (Exception ex) {
                            JCommonUtil.handleException(ex);
                        }
                    }
                });
                {
                    JButton transBackBtn = new JButton("轉回中文");
                    transBackBtn.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            try {
                                mDontMoveFileAddRow.init(null);
                                mDontMoveFileAddRow.transferBackToTxtFile();
                            } catch (Exception ex) {
                                JCommonUtil.handleException(ex);
                            }
                        }
                    });
                    transBackBtn.setActionCommand("OK");
                    buttonPane.add(transBackBtn);
                }
                // insertButton.setActionCommand("OK");
                buttonPane.add(insertButton);
                // getRootPane().setDefaultButton(insertButton);
            }
            {
                JButton updateBtn = new JButton("修改");
                updateBtn.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        try {
                            String insertAfterKey = insertAfterKeyText.getText();
                            String key = keyText.getText();
                            String value = valueArea.getText();
                            if (StringUtils.isBlank(insertAfterKey) || StringUtils.isBlank(value)) {
                                JCommonUtil._jOptionPane_showMessageDialog_error("必輸輸入鍵直!");
                                return;
                            }
                            mDontMoveFileAddRow.init(null);
                            mDontMoveFileAddRow.updateKey(insertAfterKey, value);
                        } catch (Exception ex) {
                            JCommonUtil.handleException(ex);
                        }
                    }
                });
                updateBtn.setActionCommand("OK");
                buttonPane.add(updateBtn);
            }
            {
                JButton deleteBtn = new JButton("刪除");
                deleteBtn.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        try {
                            String insertAfterKey = insertAfterKeyText.getText();
                            String key = keyText.getText();
                            String value = valueArea.getText();
                            if (StringUtils.isBlank(insertAfterKey)) {
                                JCommonUtil._jOptionPane_showMessageDialog_error("必輸輸入鍵直!");
                                return;
                            }
                            mDontMoveFileAddRow.init(null);
                            mDontMoveFileAddRow.removeKey(insertAfterKey);
                        } catch (Exception ex) {
                            JCommonUtil.handleException(ex);
                        }
                    }
                });
                deleteBtn.setActionCommand("OK");
                buttonPane.add(deleteBtn);
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }
        {
            /////////////////////////////////////////////////
            JCommonUtil.setJFrameCenter(this);
        }
    }

    private static class DontMoveFileAddRow {

        LinkedList<String> contentLst;
        File file;

        public void init(File file) {
            if (file != null) {
                this.file = file;
            }
            contentLst = new LinkedList(FileUtil.loadFromFile_asList(this.file, "UTF8"));
        }

        private String lineToUnicode(String key, String value) {
            Pattern ptnKey = Pattern.compile("[a-zA-Z0-9\\@\\$\\%\\^\\&\\*\\(\\)\\_\\+\\-\\[\\]\\/\\'\\;\\\"\\,\\.\\?\\>\\<\\`\\~]");
            Pattern ptnVal = Pattern.compile("[a-zA-Z0-9\\@\\$\\%\\^\\&\\*\\(\\)\\_\\+\\-\\[\\]\\/\\'\\;\\\"\\,\\.\\?\\>\\<\\`\\~\\s]");
            Matcher mthKey = null;
            Matcher mthVal = null;
            String result = "";
            for (int i = 0; i < key.length(); i++) {
                char charCode = key.charAt(i);
                mthKey = ptnKey.matcher("" + charCode);
                if (charCode == '\n') {
                    result += "\\n";
                } else if (charCode <= 127) {
                    if (!mthKey.find()) {
                        result += "\\" + key.charAt(i);
                    } else {
                        result += key.charAt(i);
                    }
                } else {
                    String hex = String.format("%04x", (int) charCode);
                    result += "\\u" + (hex.toUpperCase());
                }
            }
            result += "=";
            for (int i = 0; i < value.length(); i++) {
                char charCode = value.charAt(i);
                mthVal = ptnVal.matcher("" + charCode);
                if (charCode == '\n') {
                    result += "\\n";
                } else if (charCode <= 127) {
                    if (!mthVal.find()) {
                        result += "\\" + value.charAt(i);
                    } else {
                        result += value.charAt(i);
                    }
                } else {
                    String hex = String.format("%04x", (int) charCode);
                    result += "\\u" + (hex.toUpperCase());
                }
            }
            return result;
        }

        private String unicodeToUtf8(String source) {
            String content = StringEscapeUtils.unescapeJava(source);
            char[] arry = content.toCharArray();
            StringBuffer sb = new StringBuffer();
            for (char c : arry) {
                if (c == '\n') {
                    sb.append("\\n");
                } else {
                    sb.append(c);
                }
            }
            return sb.toString();
        }

        public String loadValue(String afterKey) {
            for (int ii = 0; ii < contentLst.size(); ii++) {
                String line1 = contentLst.get(ii);
                line1 = unicodeToUtf8(line1);
                if (line1.startsWith(afterKey + "=")) {
                    String key = line1.substring(0, afterKey.length());
                    String value = line1.substring(afterKey.length() + 1);
                    System.out.println("key = " + key);
                    System.out.println("value = " + key);
                    return value;
                }
            }
            return "";
        }

        public void removeKey(String afterKey) {
            for (int ii = 0; ii < contentLst.size(); ii++) {
                String line1 = contentLst.get(ii);
                line1 = unicodeToUtf8(line1);
                System.out.println(line1);
                int pos = StringUtils.lastIndexOf(line1, "=");
                if (pos == -1) {
                    continue;
                }
                String key = StringUtils.substring(line1, 0, pos);
                if (StringUtils.trimToEmpty(key).equals(afterKey)) {
                    System.out.println("find = " + line1);
                    List<String> prefix = contentLst.subList(0, ii + 1);
                    List<String> rearfix = contentLst.subList(ii + 1, contentLst.size());
                    List<String> newArry = new ArrayList<String>();
                    newArry.addAll(prefix);
                    newArry.add("");
                    newArry.addAll(rearfix);
                    String content = StringUtils.join(newArry, "\r\n");
                    FileUtil.saveToFile(file, content, "UTF8");
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException e) {
                    }
                }
            }
        }

        public void updateKey(String afterKey, String value) {
            for (int ii = 0; ii < contentLst.size(); ii++) {
                String line1 = contentLst.get(ii);
                line1 = unicodeToUtf8(line1);
                System.out.println(line1);
                int pos = StringUtils.lastIndexOf(line1, "=");
                if (pos == -1) {
                    continue;
                }
                String key = StringUtils.substring(line1, 0, pos);
                if (StringUtils.trimToEmpty(key).equals(afterKey)) {
                    System.out.println("find = " + line1);
                    List<String> prefix = contentLst.subList(0, ii + 1);
                    List<String> rearfix = contentLst.subList(ii + 1, contentLst.size());
                    List<String> newArry = new ArrayList<String>();
                    String newLine = lineToUnicode(afterKey, value);
                    System.out.println(newLine);
                    newArry.addAll(prefix);
                    newArry.add(newLine);
                    newArry.addAll(rearfix);
                    String content = StringUtils.join(newArry, "\r\n");
                    FileUtil.saveToFile(file, content, "UTF8");
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException e) {
                    }
                }
            }
        }

        public void insertAfter(String afterKey, String key11, String value) {
            for (int ii = 0; ii < contentLst.size(); ii++) {
                String line1 = contentLst.get(ii);
                line1 = unicodeToUtf8(line1);
                System.out.println(line1);
                int pos = StringUtils.lastIndexOf(line1, "=");
                if (pos == -1) {
                    continue;
                }
                String key = StringUtils.substring(line1, 0, pos);
                if (StringUtils.trimToEmpty(key).equals(afterKey)) {
                    System.out.println("find = " + line1);
                    List<String> prefix = contentLst.subList(0, ii + 1);
                    List<String> rearfix = contentLst.subList(ii + 1, contentLst.size());
                    List<String> newArry = new ArrayList<String>();
                    String newLine = lineToUnicode(key11, value);
                    System.out.println(newLine);
                    newArry.addAll(prefix);
                    newArry.add(newLine);
                    newArry.addAll(rearfix);
                    String content = StringUtils.join(newArry, "\r\n");
                    FileUtil.saveToFile(file, content, "UTF8");
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException e) {
                    }
                }
            }
        }

        public void transferBackToTxtFile() {
            List<String> contentLst2 = new ArrayList<String>();
            for (int ii = 0; ii < contentLst.size(); ii++) {
                String line1 = contentLst.get(ii);
                line1 = unicodeToUtf8(line1);
                contentLst2.add(line1);
            }
            String content = StringUtils.join(contentLst2, "\r\n");
            File newFile = new File(file.getParentFile(), file.getName() + ".txt");
            FileUtil.saveToFile(newFile, content, "UTF8");
            try {
                Desktop.getDesktop().open(newFile);
            } catch (IOException e) {
            }
        }
    }
}
