package gtu._work.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.EventObject;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;

import gtu._work.ui.JMenuBarUtil.JMenuAppender;
import gtu.file.FileUtil;
import gtu.swing.util.HideInSystemTrayHelper;
import gtu.swing.util.JCommonUtil;
import gtu.swing.util.JFrameRGBColorPanel;
import gtu.swing.util.JFrameUtil;
import gtu.swing.util.SwingActionUtil;
import gtu.swing.util.SwingActionUtil.Action;
import gtu.swing.util.SwingActionUtil.ActionAdapter;

public class SrtMovieTimeFixUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private HideInSystemTrayHelper hideInSystemTrayHelper;
    private JFrameRGBColorPanel jFrameRGBColorPanel;
    private SwingActionUtil swingUtil;
    private JTabbedPane tabbedPane;
    private JPanel panel_2;
    private JPanel panel_3;
    private JPanel panel_4;
    private JPanel panel_5;
    private JPanel panel_6;
    private JPanel panel_7;
    private JPanel panel_8;
    private JPanel panel_9;
    private JPanel panel_10;
    private JPanel panel_11;
    private JTextField timerText;
    private JButton startTimeBtn;
    private JLabel lblNewLabel;
    private JPanel panel_12;
    private JLabel lblNewLabel_1;
    private JTextField srtFileText;
    private JButton fixTimeExecuteBtn;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        if (!JFrameUtil.lockInstance_delable(SrtMovieTimeFixUI.class)) {
            return;
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    SrtMovieTimeFixUI frame = new SrtMovieTimeFixUI();
                    gtu.swing.util.JFrameUtil.setVisible(true, frame);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public SrtMovieTimeFixUI() {
        swingUtil = SwingActionUtil.newInstance(this);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.addChangeListener((ChangeListener) ActionAdapter.ChangeListener.create(ActionDefine.JTabbedPane_ChangeIndex.name(), swingUtil));
        contentPane.add(tabbedPane, BorderLayout.CENTER);

        JPanel panel = new JPanel();
        tabbedPane.addTab("字幕檔", null, panel, null);
        panel.setLayout(new BorderLayout(0, 0));

        panel_3 = new JPanel();
        panel.add(panel_3, BorderLayout.NORTH);

        panel_4 = new JPanel();
        panel.add(panel_4, BorderLayout.WEST);

        panel_5 = new JPanel();
        panel.add(panel_5, BorderLayout.EAST);

        panel_6 = new JPanel();
        panel.add(panel_6, BorderLayout.SOUTH);

        panel_12 = new JPanel();
        panel.add(panel_12, BorderLayout.CENTER);

        lblNewLabel_1 = new JLabel("檔名");
        panel_12.add(lblNewLabel_1);

        srtFileText = new JTextField();
        JCommonUtil.jTextFieldSetFilePathMouseEvent(srtFileText, false);
        panel_12.add(srtFileText);
        srtFileText.setColumns(20);

        fixTimeExecuteBtn = new JButton("校正");
        fixTimeExecuteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                swingUtil.invokeAction("fixTimeExecuteBtn.click", e);
            }
        });
        panel_12.add(fixTimeExecuteBtn);

        JPanel panel_1 = new JPanel();
        tabbedPane.addTab("校正時間", null, panel_1, null);
        panel_1.setLayout(new BorderLayout(0, 0));

        panel_7 = new JPanel();
        panel_1.add(panel_7, BorderLayout.NORTH);

        panel_8 = new JPanel();
        panel_1.add(panel_8, BorderLayout.WEST);

        panel_9 = new JPanel();
        panel_1.add(panel_9, BorderLayout.EAST);

        panel_10 = new JPanel();
        panel_1.add(panel_10, BorderLayout.SOUTH);

        panel_11 = new JPanel();
        panel_1.add(panel_11, BorderLayout.CENTER);

        lblNewLabel = new JLabel("校正時間");
        panel_11.add(lblNewLabel);

        timerText = new JTextField();
        panel_11.add(timerText);
        timerText.setColumns(10);

        startTimeBtn = new JButton("計時");
        startTimeBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                swingUtil.invokeAction("startTimeBtn.click", e);
            }
        });
        panel_11.add(startTimeBtn);

        panel_2 = new JPanel();
        tabbedPane.addTab("其他設定", null, panel_2, null);
        panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        {
            // 掛載所有event
            applyAllEvents();

            JCommonUtil.setJFrameCenter(this);
            JCommonUtil.setJFrameIcon(this, "resource/images/ico/tk_aiengine.ico");
            hideInSystemTrayHelper = HideInSystemTrayHelper.newInstance();
            hideInSystemTrayHelper.apply(this);
            jFrameRGBColorPanel = new JFrameRGBColorPanel(this);
            panel_2.add(jFrameRGBColorPanel.getToggleButton(false));
            panel_2.add(hideInSystemTrayHelper.getToggleButton(false));
            this.applyAppMenu();
            JCommonUtil.defaultToolTipDelay();
            this.setTitle("You Set My World On Fire");
        }
    }

    private enum ActionDefine {
        TEST_DEFAULT_EVENT, //
        JTabbedPane_ChangeIndex, //
        ;
    }

    private void applyAllEvents() {
        swingUtil.addActionHex(ActionDefine.TEST_DEFAULT_EVENT.name(), new Action() {
            @Override
            public void action(EventObject evt) throws Exception {
                System.out.println("====Test Default Event!!====");
            }
        });
        swingUtil.addActionHex(ActionDefine.JTabbedPane_ChangeIndex.name(), new Action() {
            @Override
            public void action(EventObject evt) throws Exception {
                System.out.println("tabbedPane : " + tabbedPane.getSelectedIndex());
            }
        });

        swingUtil.addActionHex("startTimeBtn.click", new Action() {
            Timer timer;
            boolean doStop = false;

            @Override
            public void action(EventObject evt) throws Exception {
                if (timer == null) {
                    doStop = false;
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            long result = getStartTime() + 1;
                            timerText.setText(String.valueOf(result));
                            if (doStop) {
                                timer.cancel();
                                timer = null;
                            }
                        }
                    }, 0, 1);
                } else {
                    doStop = !doStop;
                }
            }
        });

        swingUtil.addActionHex("fixTimeExecuteBtn.click", new Action() {
            Pattern ptn1 = Pattern.compile("(\\d{2}\\:\\d{2}\\:\\d{2}\\,\\d{3})(\\s*\\-\\-\\>\\s*)(\\d{2}\\:\\d{2}\\:\\d{2}\\,\\d{3})", Pattern.DOTALL | Pattern.MULTILINE);// 02:08:11,587
            Pattern ptn2 = Pattern.compile("(\\d{2})\\:(\\d{2})\\:(\\d{2})\\,(\\d{3})");
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss,SSS");

            private String addTime(String leftTime, int addTime) {
                Matcher mth = ptn2.matcher(leftTime);
                if (mth.find()) {
                    int hours = Integer.parseInt(mth.group(1));
                    int mins = Integer.parseInt(mth.group(2));
                    int secs = Integer.parseInt(mth.group(3));
                    int millisec = Integer.parseInt(mth.group(4));

                    Calendar cal = Calendar.getInstance();
                    cal.set(Calendar.HOUR, hours);
                    cal.set(Calendar.MINUTE, mins);
                    cal.set(Calendar.SECOND, secs);
                    cal.set(Calendar.MILLISECOND, millisec);

                    cal.add(Calendar.MILLISECOND, addTime);

                    return sdf.format(cal.getTime());
                }
                throw new RuntimeException("修正時間錯誤!:" + leftTime);
            }

            @Override
            public void action(EventObject evt) throws Exception {
                int addTime = (int) getStartTime();
                Validate.isTrue(addTime != 0, "修正時間不可為0");

                File srtFile = new File(StringUtils.trimToEmpty(srtFileText.getText()));
                Validate.isTrue(srtFile.getName().toLowerCase().endsWith(".srt") || //
                srtFile.getName().toLowerCase().endsWith(".srt.bak"), "必須為srt檔");
                String content = FileUtil.loadFromFile(srtFile, "UTF8");
                Matcher mth = ptn1.matcher(content);

                StringBuffer sb = new StringBuffer();
                while (mth.find()) {
                    String leftTime = mth.group(1);
                    String middle = mth.group(2);
                    String rightTime = mth.group(3);

                    leftTime = addTime(leftTime, addTime);
                    rightTime = addTime(rightTime, addTime);
                    mth.appendReplacement(sb, leftTime + middle + rightTime);
                }
                mth.appendTail(sb);

                File fixFile = new File(srtFile.getParentFile(), srtFile.getName() + ".fix");
                FileUtil.saveToFile(fixFile, sb.toString(), "UTF8");
                JCommonUtil._jOptionPane_showMessageDialog_info("寫檔完成!");
            }
        });
    }

    private long getStartTime() {
        try {
            return Long.parseLong(timerText.getText());
        } catch (Exception ex) {
            return 0;
        }
    }

    private void applyAppMenu() {
        JMenu menu1 = JMenuAppender.newInstance("child_item")//
                .addMenuItem("detail1", (ActionListener) ActionAdapter.ActionListener.create(ActionDefine.TEST_DEFAULT_EVENT.name(), getSwingUtil()))//
                .getMenu();
        JMenu mainMenu = JMenuAppender.newInstance("file")//
                .addMenuItem("item1", null)//
                .addMenuItem("item2", (ActionListener) ActionAdapter.ActionListener.create(ActionDefine.TEST_DEFAULT_EVENT.name(), getSwingUtil()))//
                .addChildrenMenu(menu1)//
                .getMenu();
        JMenuBarUtil.newInstance().addMenu(mainMenu).apply(this);
    }

    public SwingActionUtil getSwingUtil() {
        return swingUtil;
    }
}
