package gtu._work.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import gtu.file.FileUtil;
import gtu.swing.util.JCommonUtil;
import gtu.swing.util.JCommonUtil.HandleDocumentEvent;
import gtu.swing.util.JFrameRGBColorPanel;
import gtu.swing.util.JTextAreaUtil;
import gtu.swing.util.KeyEventExecuteHandler;

public class FastDBQueryUI_UpdateSqlArea extends JDialog {

    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTextArea updateSqlArea;
    private JLabel lblForMessage;
    private static final String DELIMIT = "^;^";
    private static final String DELIMIT_SPLIT_PTN = Pattern.quote(DELIMIT);
    private SqlAreaHandler sqlAreaHandler = new SqlAreaHandler();
    private ActionListener confirmDo;
    private JFrameRGBColorPanel jFrameRGBColorPanel;
    private boolean jFrameRGBColorPanel_isStop;
    private KeyEventExecuteHandler keyEventExecuteHandler;
    private JButton okButton;
    private JTextField filterText;
    private List<Integer> findLst = new ArrayList<Integer>();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        FastDBQueryUI_UpdateSqlArea.newInstance("XXXX", Arrays.asList("1111", "eeeee"), false, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // ((FastDBQueryUI_UpdateSqlArea)e.getSource());
            }
        });
    }

    public static FastDBQueryUI_UpdateSqlArea newInstance(String title, List<String> sqlText, boolean jFrameRGBColorPanel_isStop, final ActionListener onCloseListener) {
        final FastDBQueryUI_UpdateSqlArea dialog = new FastDBQueryUI_UpdateSqlArea();
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setVisible(true);

        dialog.lblForMessage.setText(title);
        dialog.updateSqlArea.setText(dialog.sqlAreaHandler.convert(sqlText));
        dialog.jFrameRGBColorPanel_isStop = jFrameRGBColorPanel_isStop;

        dialog.addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent e) {
                if (onCloseListener != null) {
                    onCloseListener.actionPerformed(new ActionEvent(dialog, -1, "close"));
                }
            }

            public void windowClosing(WindowEvent e) {
            }
        });
        return dialog;
    }

    public void setConfirmDo(ActionListener confirmDo) {
        this.confirmDo = confirmDo;
    }

    private class SqlAreaHandler {
        private String convert(List<String> sqlLst) {
            return StringUtils.join(sqlLst, DELIMIT + "\n");
        }

        private List<String> convert(String sqlText) {
            String text = StringUtils.trimToEmpty(sqlText);
            text = text.replace("[\r\n]+", "");
            return Arrays.asList(text.split(DELIMIT_SPLIT_PTN, -1));
        }
    }

    public List<String> getSqlText() {
        return sqlAreaHandler.convert(this.updateSqlArea.getText());
    }

    /**
     * Create the dialog.
     */
    public FastDBQueryUI_UpdateSqlArea() {
        setBounds(100, 100, 554, 378);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));
        {
            JPanel panel = new JPanel();
            contentPanel.add(panel, BorderLayout.NORTH);
            {
                lblForMessage = new JLabel("                         ");
                panel.add(lblForMessage);
                filterText = new JTextField();
                filterText.setPreferredSize(new Dimension(150, 25));
                filterText.setToolTipText("查詢SQL内容");
                panel.add(filterText);

                filterText.getDocument().addDocumentListener(JCommonUtil.getDocumentListener(new HandleDocumentEvent() {
                    DefaultHighlighter.DefaultHighlightPainter yellowPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.yellow);

                    @Override
                    public void process(DocumentEvent event) {
                        findLst.clear();
                        String text = this.getDocText(event);
                        Pattern ptn = Pattern.compile(Pattern.quote(text), Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
                        updateSqlArea.getHighlighter().removeAllHighlights();
                        String sqlText = updateSqlArea.getText();
                        if (StringUtils.isBlank(sqlText)) {
                            return;
                        }
                        Matcher mth = ptn.matcher(sqlText);
                        while (mth.find()) {
                            int start = mth.start();
                            int end = mth.end();
                            try {
                                findLst.add(start);
                                updateSqlArea.getHighlighter().addHighlight(start, end, yellowPainter);
                            } catch (BadLocationException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }));
            }
        }
        {
            JPanel panel = new JPanel();
            contentPanel.add(panel, BorderLayout.WEST);
        }
        {
            JPanel panel = new JPanel();
            contentPanel.add(panel, BorderLayout.EAST);
        }
        {
            JPanel panel = new JPanel();
            contentPanel.add(panel, BorderLayout.SOUTH);
        }
        {
            updateSqlArea = new JTextArea();
            JTextAreaUtil.applyCommonSetting(updateSqlArea);
            contentPanel.add(JCommonUtil.createScrollComponent(updateSqlArea), BorderLayout.CENTER);

            updateSqlArea.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    try {
                        if (findLst.isEmpty()) {
                            return;
                        }
                        if (e.getKeyCode() == KeyEvent.VK_F3) {
                            int currentPos = updateSqlArea.getCaretPosition();
                            int idx = findLst.indexOf(currentPos);
                            int newPos = 0;
                            if (idx == -1) {
                                newPos = findLst.get(0);
                            } else {
                                if (idx + 1 >= findLst.size()) {
                                    idx = 0;
                                } else {
                                    idx++;
                                }
                                newPos = findLst.get(idx);
                            }
                            updateSqlArea.setCaretPosition(newPos);
                        }
                    } catch (Exception ex) {
                        JCommonUtil.handleException(ex);
                    }
                }
            });
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton exportSqlBtn = new JButton("匯出檔案");
                exportSqlBtn.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        exportSqlBtnAction();
                    }
                });
                buttonPane.add(exportSqlBtn);
            }
            {
                okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);// 預設按鈕

                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (confirmDo != null) {
                            confirmDo.actionPerformed(e);
                        }
                        dispose();
                    }
                });
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                    }
                });
            }
        }

        this.keyEventExecuteHandler = KeyEventExecuteHandler.newInstance(this, null, null, new Runnable() {
            @Override
            public void run() {
                JCommonUtil.triggerButtonActionPerformed(okButton);
            }
        }, new Component[] {});

        JCommonUtil.setJFrameCenter(this);
        JCommonUtil.defaultToolTipDelay();
        jFrameRGBColorPanel = new JFrameRGBColorPanel(this);
        jFrameRGBColorPanel.setStop(jFrameRGBColorPanel_isStop);
    }

    private void exportSqlBtnAction() {
        String sqlText = StringUtils.defaultString(updateSqlArea.getText());
        if (sqlText.contains("^;^")) {
            sqlText = sqlText.replaceAll("\\^\\;\\^", ";");
            sqlText += ";";
        }
        String filename = JCommonUtil._jOptionPane_showInputDialog("請輸入匯出檔名",
                FastDBQueryUI.class.getSimpleName() + "_" + DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMdd_HHmmss") + ".sql");
        if (StringUtils.isBlank(filename)) {
            JCommonUtil._jOptionPane_showMessageDialog_error("檔名有誤!");
            return;
        }
        File outputFile = new File(FileUtil.DESKTOP_DIR, filename);
        if (outputFile.exists()) {
            JCommonUtil._jOptionPane_showMessageDialog_error("檔案已存在!");
            return;
        }
        FileUtil.saveToFile(outputFile, sqlText, "UTF8");
        JCommonUtil._jOptionPane_showMessageDialog_info("匯出完成 : " + outputFile);
    }
}
