package gtu._work.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.Clip;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.StringUtils;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import gtu.music.BeepUtil;
import gtu.properties.PropertiesUtilBean;
import gtu.swing.util.HideInSystemTrayHelper;
import gtu.swing.util.JColorUtil;
import gtu.swing.util.JCommonUtil;
import gtu.swing.util.JFrameUtil;

public class AlwaysOpenUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField textField;
    private JToggleButton tglbtnStartend;
    private Timer timer = new Timer();
    private Timer timer2 = new Timer();
    private Clip clip = null;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        if (!JFrameUtil.lockInstance_delable(AlwaysOpenUI.class)) {
            return;
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AlwaysOpenUI frame = new AlwaysOpenUI();
                    gtu.swing.util.JFrameUtil.setVisible(true, frame);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private PropertiesUtilBean config = new PropertiesUtilBean(AlwaysOpenUI.class);
    private JToggleButton tglbtnStartend_1;
    private JLabel lblNewLabel;
    private JTextField colorText;

    /**
     * Create the frame.
     */
    public AlwaysOpenUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 326);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        contentPane.add(tabbedPane, BorderLayout.CENTER);

        JPanel panel = new JPanel();
        tabbedPane.addTab("New tab", null, panel, null);
        panel.setLayout(new FormLayout(new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
                new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

        JLabel lbln = new JLabel("每N秒觸發一次");
        panel.add(lbln, "2, 2, right, default");

        textField = new JTextField();
        panel.add(textField, "4, 2, fill, default");
        textField.setColumns(10);

        tglbtnStartend = new JToggleButton("啟動/結束");
        tglbtnStartend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String sec = textField.getText();
                    if (tglbtnStartend.isSelected()) {
                        timer = new Timer();
                        timer.schedule(createTask(), 0, Integer.parseInt(sec) * 1000);

                        if (StringUtils.isNumeric(sec)) {
                            config.getConfigProp().setProperty("sec", sec);
                            config.store();
                        }
                    } else {
                        timer.cancel();
                    }
                } catch (Exception ex) {
                    JCommonUtil.handleException(ex);
                }
            }
        });

        this.addWindowListener(new WindowListener() {

            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
                String sec = textField.getText();
                String color = colorText.getText();
                if (StringUtils.isNumeric(sec)) {
                    config.getConfigProp().setProperty("sec", sec);
                }
                if (StringUtils.isNotBlank(color)) {
                    config.getConfigProp().setProperty("color", color);
                }
                config.store();
            }

            @Override
            public void windowClosed(WindowEvent e) {
            }

            @Override
            public void windowActivated(WindowEvent e) {
            }
        });

        lblNewLabel = new JLabel("警報色票");
        panel.add(lblNewLabel, "2, 4, right, default");

        colorText = new JTextField();
        panel.add(colorText, "4, 4, fill, default");
        colorText.setColumns(10);
        panel.add(tglbtnStartend, "4, 16");

        tglbtnStartend_1 = new JToggleButton("警報啟動/結束");
        tglbtnStartend_1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    if (tglbtnStartend_1.isSelected()) {
                        timer2 = new Timer();
                        timer2.schedule(createColoTask(), 0, 1000);

                        String color = colorText.getText();
                        if (StringUtils.isNotBlank(color)) {
                            config.getConfigProp().setProperty("color", color);
                            config.store();
                        }
                    } else {
                        if (clip != null) {
                            clip.close();
                        }
                        timer2.cancel();
                    }
                } catch (Exception ex) {
                    JCommonUtil.handleException(ex);
                }
            }
        });
        panel.add(tglbtnStartend_1, "4, 18");

        if (config.getConfigProp().containsKey("sec")) {
            String sec = config.getConfigProp().getProperty("sec");
            textField.setText(sec);
        }
        if (config.getConfigProp().containsKey("color")) {
            String color = config.getConfigProp().getProperty("color");
            colorText.setText(color);
        }

        JCommonUtil.setJFrameCenter(this);
        HideInSystemTrayHelper.newInstance().apply(this, "保持登入工具", "resource/images/ico/whtsuvifzdlhiqcxubpu.ico");

        checkAndStart();
    }

    private void checkAndStart() {
        try {
            String text = StringUtils.trimToEmpty(textField.getText());
            if (Integer.parseInt(text) > 0) {
                if (!tglbtnStartend.isSelected()) {
                    tglbtnStartend.doClick();
                }
            }
        } catch (Exception ex) {
        }
    }

    private TimerTask createTask() {
        return new TimerTask() {
            private int getAdd() {
                int[] arry = new int[] { -1, 1 };
                int pos = new Random().nextInt(arry.length);
                return arry[pos];
            }

            @Override
            public void run() {
                try {
                    Point p = MouseInfo.getPointerInfo().getLocation();
                    Robot robot = new Robot();
                    robot.mouseMove(p.x + getAdd(), p.y + getAdd());
                    System.out.println("move pos!!");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    private TimerTask createColoTask() {
        return new TimerTask() {
            Robot robot;

            @Override
            public void run() {
                try {
                    if (robot == null) {
                        robot = new Robot();
                    }
                    Point p = MouseInfo.getPointerInfo().getLocation();
                    Color color = robot.getPixelColor((int) p.x, (int) p.y);
                    if (StringUtils.isNotBlank(colorText.getText())) {
                        if (!color.equals(JColorUtil.rgb(colorText.getText()))) {
                            if (clip == null || !clip.isRunning()) {
                                clip = BeepUtil.beep(100);
                            }
                        } else {
                            if (clip != null && clip.isRunning()) {
                                clip.close();
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }
}
