package gtu._work.ui;

import java.awt.AWTException;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jnativehook.NativeInputEvent;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.mouse.NativeMouseEvent;

import gtu.keyboard_mouse.JnativehookKeyboardMouseHelper;
import gtu.keyboard_mouse.JnativehookKeyboardMouseHelper.MyNativeKeyAdapter;
import gtu.swing.util.JCommonUtil;

public class Magnifier4VRUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private int MAGNIFIER_WIDTH = 500;
    private int MAGNIFIER_HEIGHT = 400;
    private BackgroundHolder mBackgroundHolder;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        try {
            Magnifier4VRUI dialog = new Magnifier4VRUI();
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the dialog.
     */
    public Magnifier4VRUI() {
        setBounds(100, 100, MAGNIFIER_WIDTH, MAGNIFIER_HEIGHT);
        getContentPane().setLayout(new BorderLayout());

        mBackgroundHolder = new BackgroundHolder();

        getContentPane().add(mBackgroundHolder.mainPanel, BorderLayout.CENTER);

        {
            // JPanel buttonPane = new JPanel();
            // buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            // getContentPane().add(buttonPane, BorderLayout.SOUTH);
            // {
            // JButton okButton = new JButton("OK");
            // okButton.setActionCommand("OK");
            // buttonPane.add(okButton);
            // getRootPane().setDefaultButton(okButton);
            // }
            // {
            // JButton cancelButton = new JButton("Cancel");
            // cancelButton.setActionCommand("Cancel");
            // buttonPane.add(cancelButton);
            // }
        }
        //////////////////////////////////////////////////////////////
        {
            init_ok();
        }
    }

    private BufferedImage createTransparentImage(int width, int height) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = img.createGraphics();

        // clear
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR));
        g2.fillRect(0, 0, 256, 256);

        // reset composite
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));
        // draw
        g2.setPaint(Color.RED);
        g2.fillOval(50, 50, 100, 100);
        return img;
    }

    private class BackgroundHolder {
        BufferedImage image;

        void setBImg(BufferedImage image) {
            this.image = image;
        }

        BufferedImage getBImg() {
            if (image == null) {
                image = createTransparentImage(MAGNIFIER_WIDTH, MAGNIFIER_HEIGHT);
            }
            return image;
        }

        JPanel mainPanel = new JPanel(new BorderLayout()) {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(getBImg(), 0, 0, getWidth(), getHeight(), this);
            }

            @Override
            public Dimension getPreferredSize() {
                Dimension size = super.getPreferredSize();
                size.width = getBImg().getWidth();
                size.height = getBImg().getHeight();
                return size;
            }
        };
    }

    private void init_ok() {
        JnativehookKeyboardMouseHelper.startNativeKeyboardAndMouse(new MyNativeKeyAdapter() {
            Robot robot;

            private BufferedImage zoom(BufferedImage before, double zoom) {
                int w = before.getWidth();
                int h = before.getHeight();
                BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
                AffineTransform at = new AffineTransform();
                at.scale(zoom, zoom);
                AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
                after = scaleOp.filter(before, after);
                return after;
            }

            @Override
            public void nativeMouseReleased(NativeMouseEvent e) {
                if (robot == null) {
                    try {
                        robot = new Robot();
                    } catch (AWTException e1) {
                        e1.printStackTrace();
                    }
                }

                Point point = MouseInfo.getPointerInfo().getLocation();
                double x = point.getX();
                double y = point.getY();

                int divX = MAGNIFIER_WIDTH / 2;
                int divY = MAGNIFIER_HEIGHT / 2;

                BufferedImage img1 = robot.createScreenCapture(new Rectangle((int) x - divX, (int) y - divY, divX * 2, divY * 2));
                BufferedImage img2 = zoom(img1, 2D);

                mBackgroundHolder.setBImg(img2);
                mBackgroundHolder.mainPanel.repaint();
            }

            @Override
            public void nativeKeyReleased(NativeKeyEvent e) {
                if ((e.getModifiers() & NativeInputEvent.ALT_L_MASK) != 0 && //
                e.getKeyCode() == NativeKeyEvent.VC_C) {
                    startNewUI();
                }
            }

            private void startNewUI() {
                if (JCommonUtil.isOnTop(Magnifier4VRUI.this)) {
                    gtu.swing.util.JFrameUtil.setVisible(false, Magnifier4VRUI.this);
                } else {
                    JCommonUtil.setFrameAtop(Magnifier4VRUI.this, false);
                }
            }
        });
    }
}
