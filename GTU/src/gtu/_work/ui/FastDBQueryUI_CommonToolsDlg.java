package gtu._work.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import gtu._work.ui.FastDBQueryUI_ExportSchemaTableToXls.DBType;
import gtu.date.DateUtil;
import gtu.db.jdbc.util.DBDateUtil;
import gtu.file.FileUtil;
import gtu.string.StringUtil_;
import gtu.swing.util.JCommonUtil;
import gtu.swing.util.JTabbedPaneUtil;
import gtu.swing.util.JTextAreaUtil;

public class FastDBQueryUI_CommonToolsDlg extends JDialog {

    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTextArea sqlTextArea;
    private JTextArea paramsTextArea;
    private JTextArea resultTextArea;
    private FastDBQueryUI_ExportSchemaTableToXls mFastDBQueryUI_ExportXlsTableProcess = new FastDBQueryUI_ExportSchemaTableToXls();
    private FastDBQueryUI self;
    private JTabbedPane tabbedPane;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        FastDBQueryUI_CommonToolsDlg inst = FastDBQueryUI_CommonToolsDlg.newInstance(null);
    }

    public static FastDBQueryUI_CommonToolsDlg newInstance(FastDBQueryUI self) {
        FastDBQueryUI_CommonToolsDlg dialog = new FastDBQueryUI_CommonToolsDlg();
        try {
            dialog.self = self;
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setVisible(true);
        } catch (Exception e) {
            JCommonUtil.handleException(e);
        }
        return dialog;
    }

    /**
     * Create the dialog.
     */
    public FastDBQueryUI_CommonToolsDlg() {
        setBounds(100, 100, 663, 488);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BorderLayout(0, 0));
        {
            tabbedPane = new JTabbedPane(JTabbedPane.TOP);
            JTabbedPaneUtil.applyCommonSetting(tabbedPane);
            contentPanel.add(tabbedPane, BorderLayout.CENTER);
            {
                final JPanel panel = new JPanel();
                tabbedPane.addTab("QuestionSQL", null, panel, null);
                panel.setLayout(new BorderLayout(0, 0));
                {
                    final JPanel panel_1 = new JPanel();
                    panel.add(panel_1, BorderLayout.NORTH);
                }
                {
                    final JPanel panel_1 = new JPanel();
                    panel.add(panel_1, BorderLayout.WEST);
                }
                {
                    final JPanel panel_1 = new JPanel();
                    panel.add(panel_1, BorderLayout.SOUTH);
                }
                {
                    final JPanel panel_1 = new JPanel();
                    panel.add(panel_1, BorderLayout.EAST);
                }
                {
                    final JPanel panel_1 = new JPanel();
                    panel.add(panel_1, BorderLayout.CENTER);
                    panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
                    {
                        final JLabel lblNewLabel = new JLabel("原始SQL");
                        panel_1.add(lblNewLabel);
                    }
                    {
                        sqlTextArea = new JTextArea();
                        sqlTextArea.setColumns(75);
                        sqlTextArea.setRows(3);
                        panel_1.add(JCommonUtil.createScrollComponent(sqlTextArea));
                    }
                    {
                        final JLabel lblNewLabel_1 = new JLabel("參數文字");
                        panel_1.add(lblNewLabel_1);
                    }
                    {
                        paramsTextArea = new JTextArea();
                        paramsTextArea.setRows(3);
                        paramsTextArea.setColumns(75);
                        panel_1.add(JCommonUtil.createScrollComponent(paramsTextArea));
                    }
                    {
                        final JLabel lblNewLabel_2 = new JLabel("      結果");
                        panel_1.add(lblNewLabel_2);
                    }
                    {
                        resultTextArea = new JTextArea();
                        resultTextArea.setRows(3);
                        resultTextArea.setColumns(75);
                        panel_1.add(JCommonUtil.createScrollComponent(resultTextArea));
                    }
                    {
                        final JButton executeBtn1 = new JButton("執行");
                        panel_1.add(executeBtn1);
                        executeBtn1.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                try {
                                    String originSql = sqlTextArea.getText();
                                    String paramsString = paramsTextArea.getText();
                                    String resultSql = processSqlCombineProcess(originSql, paramsString);
                                    resultTextArea.setText(resultSql);
                                    JCommonUtil._jOptionPane_showMessageDialog_info("執行完成!");
                                } catch (Exception ex) {
                                    JCommonUtil.handleException(ex);
                                }
                            }
                        });
                    }
                    {
                        final JButton clearBtn1 = new JButton("清除");
                        panel_1.add(clearBtn1);
                        {
                            JPanel panel_2 = new JPanel();
                            tabbedPane.addTab("Insert SQL Transfer", null, panel_2, null);
                            final JComboBox insertSqlDbTypeCombobox = createSchemaTablesToCreateSQLCombobox();
                            panel_2.setLayout(new BorderLayout(0, 0));
                            {
                                JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.NORTH);
                            }
                            {
                                JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.WEST);
                            }
                            {
                                JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.EAST);
                            }
                            {
                                JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.SOUTH);
                            }
                            {
                                JPanel panel_4 = new JPanel();
                                panel_2.add(panel_4, BorderLayout.CENTER);
                                {
                                    JLabel lblNewLabel_6 = new JLabel("Insert SQL 檔");
                                    panel_4.add(lblNewLabel_6);
                                }
                                {
                                    insertSqlFileText = new JTextField();
                                    JCommonUtil.jTextFieldSetFilePathMouseEvent(insertSqlFileText, false);
                                    panel_4.add(insertSqlFileText);
                                    insertSqlFileText.setColumns(20);
                                }
                                {
                                    JButton insertSqlFileBtn = new JButton("轉換");
                                    insertSqlFileBtn.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            FastDBQueryUI_ExportSchemaTableToXls.DBType mDBType = (FastDBQueryUI_ExportSchemaTableToXls.DBType) insertSqlDbTypeCombobox.getSelectedItem();
                                            insertSqlFileBtnAction(insertSqlFileText.getText(), "XXXXXXXXXXXX", mDBType);
                                        }
                                    });
                                    {
                                        panel_4.add(insertSqlDbTypeCombobox);
                                    }
                                    panel_4.add(insertSqlFileBtn);
                                }
                            }
                        }
                        {
                            final JPanel panel_2 = new JPanel();
                            tabbedPane.addTab("Export Schema", null, panel_2, null);
                            panel_2.setLayout(new BorderLayout(0, 0));
                            {
                                final JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.NORTH);
                            }
                            {
                                final JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.WEST);
                            }
                            {
                                final JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.EAST);
                            }
                            {
                                final JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.SOUTH);
                            }
                            {
                                JPanel panel_3 = new JPanel();
                                final JComboBox schemaTablesToCreateSQLCombobox = createSchemaTablesToCreateSQLCombobox();
                                final JComboBox schemaTablesToInsertTableSQLCombobox = createSchemaTablesToInsertTableSQLCombobox();
                                panel_2.add(panel_3, BorderLayout.CENTER);
                                panel_3.setLayout(new FormLayout(
                                        new ColumnSpec[] { FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                                                FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), },
                                        new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                                                FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));
                                {
                                    JLabel lblNewLabel_3 = new JLabel("匯出schema");
                                    panel_3.add(lblNewLabel_3, "2, 2");
                                }
                                {
                                    final JButton schemaTablesToExcelBtn = new JButton("Schema Tables To Excel");
                                    schemaTablesToExcelBtn.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent arg0) {
                                            exportCurrentSystemToExcel();
                                        }
                                    });
                                    panel_3.add(schemaTablesToExcelBtn, "4, 2");
                                }
                                {
                                    final JLabel lblNewLabel_4 = new JLabel("匯出CreateTableSQL");
                                    panel_3.add(lblNewLabel_4, "2, 4");
                                }
                                {
                                    final JButton schemaTablesToCreateSQLBtn = new JButton("Schema Tables To Create Table SQL");
                                    schemaTablesToCreateSQLBtn.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent arg0) {
                                            FastDBQueryUI_ExportSchemaTableToXls.DBType mDBType = (FastDBQueryUI_ExportSchemaTableToXls.DBType) schemaTablesToCreateSQLCombobox.getSelectedItem();
                                            exportCurrentSystemToCreateSQL(mDBType);
                                        }
                                    });
                                    panel_3.add(schemaTablesToCreateSQLBtn, "4, 4");
                                }
                                {
                                    panel_3.add(schemaTablesToCreateSQLCombobox, "6, 4, fill, default");
                                }
                                {
                                    final JLabel lblNewLabel_5 = new JLabel("匯出Insert SQL");
                                    panel_3.add(lblNewLabel_5, "2, 6");
                                }
                                {
                                    final JButton schemaTablesToInsertTableSQLBtn = new JButton("Schema Tables To Insert Table SQL");
                                    schemaTablesToInsertTableSQLBtn.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent arg0) {
                                            DBDateUtil.DBDateFormat mDBType = (DBDateUtil.DBDateFormat) schemaTablesToInsertTableSQLCombobox.getSelectedItem();
                                            exportCurrentSystemToInsertSQL(mDBType);
                                        }
                                    });
                                    panel_3.add(schemaTablesToInsertTableSQLBtn, "4, 6");
                                }
                                {

                                    panel_3.add(schemaTablesToInsertTableSQLCombobox, "6, 6, fill, default");
                                }
                            }
                        }
                        {
                            JPanel panel_2 = new JPanel();
                            final JCheckBox stringBuilderRemoveSimpleTextChk = new JCheckBox("[簡單字串相接]");
                            tabbedPane.addTab("StringBuilder Remove", null, panel_2, null);
                            panel_2.setLayout(new BorderLayout(0, 0));
                            final JTextArea stringBuilderRemoveArea = new JTextArea();
                            JButton stringBuilderRemoveClearBtn = new JButton("清除");
                            JTextAreaUtil.applyCommonSetting(stringBuilderRemoveArea);
                            {
                                JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.NORTH);
                                {
                                    panel_3.add(stringBuilderRemoveSimpleTextChk);
                                    stringBuilderRemoveSimpleTextChk.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e) {
                                            String label = stringBuilderRemoveSimpleTextChk.isSelected() ? "[簡單字串相接]" : "[StringBuilder]";
                                            stringBuilderRemoveSimpleTextChk.setText(label);
                                            if (stringBuilderRemoveSimpleTextChk.isSelected()) {
                                                stringBuilderRemoveText.setText("");
                                                stringBuilderRemoveText.setVisible(false);
                                            } else {
                                                stringBuilderRemoveText.setText("");
                                                stringBuilderRemoveText.setVisible(true);
                                            }
                                        }
                                    });
                                }
                                {
                                }
                                {
                                    stringBuilderRemoveText = new JTextField();
                                    panel_3.add(stringBuilderRemoveText);
                                    stringBuilderRemoveText.setColumns(15);
                                }
                                {
                                    stringBuilderRemoveClearBtn.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            stringBuilderRemoveText.setText("");
                                            stringBuilderRemoveArea.setText("");
                                        }
                                    });
                                    panel_3.add(stringBuilderRemoveClearBtn);
                                }
                                {
                                    JButton stringBuilderRemoveExecuteBtn = new JButton("執行");
                                    stringBuilderRemoveExecuteBtn.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            String sql = stringBuilderRemoveArea.getText();
                                            String paramName = stringBuilderRemoveText.getText();
                                            if (!stringBuilderRemoveSimpleTextChk.isSelected()) {
                                                stringBuilderRemoveExecuteBtnAction(sql, paramName, stringBuilderRemoveArea);
                                            } else {
                                                stringBuilderRemoveExecuteBtnAction_Simple(sql, paramName, stringBuilderRemoveArea);
                                            }
                                        }
                                    });
                                    panel_3.add(stringBuilderRemoveExecuteBtn);
                                }
                            }
                            {
                                JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.WEST);
                            }
                            {
                                JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.EAST);
                            }
                            {
                                JPanel panel_3 = new JPanel();
                                panel_2.add(panel_3, BorderLayout.SOUTH);
                            }
                            {
                                panel_2.add(JCommonUtil.createScrollComponent(stringBuilderRemoveArea), BorderLayout.CENTER);
                            }

                            //////////////////////////////////////
                            JCommonUtil.triggerButtonActionPerformed(stringBuilderRemoveSimpleTextChk);
                            //////////////////////////////////////
                        }
                        clearBtn1.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                sqlTextArea.setText("");
                                paramsTextArea.setText("");
                                resultTextArea.setText("");
                            }
                        });
                    }
                }
            }
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                    }
                });
            }
        }
        // =========================================================================================
        // =========================================================================================
        {
            JCommonUtil.setJFrameCenter(this);
            JTabbedPaneUtil mJTabbedPaneUtil = JTabbedPaneUtil.newInst(tabbedPane);
            mJTabbedPaneUtil.setSelectTabIndex(3);
        }
    }

    private JComboBox createSchemaTablesToInsertTableSQLCombobox() {
        final JComboBox schemaTablesToInsertTableSQLCombobox = new JComboBox();
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (DBDateUtil.DBDateFormat mDBType : DBDateUtil.DBDateFormat.values()) {
            model.addElement(mDBType);
        }
        schemaTablesToInsertTableSQLCombobox.setModel(model);
        return schemaTablesToInsertTableSQLCombobox;
    }

    private JComboBox createSchemaTablesToCreateSQLCombobox() {
        final JComboBox schemaTablesToCreateSQLCombobox = new JComboBox();
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (FastDBQueryUI_ExportSchemaTableToXls.DBType mDBType : FastDBQueryUI_ExportSchemaTableToXls.DBType.values()) {
            model.addElement(mDBType);
        }
        schemaTablesToCreateSQLCombobox.setModel(model);
        return schemaTablesToCreateSQLCombobox;
    }

    private static final Pattern QuestionPtn = Pattern.compile("\\?", Pattern.MULTILINE | Pattern.DOTALL);
    private static final Pattern NumberPtn = Pattern.compile("[\\-\\d\\.eE]+");
    private JTextField stringBuilderRemoveText;
    private JTextField insertSqlFileText;

    private String processSqlCombineProcess(String originSql, String paramsString) {
        originSql = StringUtils.defaultString(originSql);
        paramsString = StringUtils.defaultString(paramsString);

        List<Pair<Integer, Integer>> varchar2Lst = StringUtil_.caculateQuoteMap(originSql, '\'', '\'');

        StringBuffer sb = new StringBuffer();

        String paramsString1 = StringUtils.trimToEmpty(paramsString);
        if (paramsString1.startsWith("[") && paramsString1.endsWith("]")) {
            paramsString1 = paramsString1.substring(1, paramsString1.length() - 1);
            paramsString = paramsString1;
        }

        String[] array = paramsString.split(",", -1);

        int index = 0;

        Matcher mth = QuestionPtn.matcher(originSql);
        A: while (mth.find()) {
            for (Pair<Integer, Integer> pair : varchar2Lst) {
                if (pair.getLeft() <= mth.start() && mth.end() <= pair.getRight()) {
                    System.out.println("[Real]ignore = " + StringUtils.substring(originSql, pair.getLeft(), pair.getRight()));
                    continue A;
                }
            }

            String value = getArry(array, index);
            value = getRealValue(value);

            mth.appendReplacement(sb, value);
            index++;
        }

        mth.appendTail(sb);
        return sb.toString();
    }

    private String getRealValue(String value) {
        value = StringUtils.trimToEmpty(value);
        if ("null".equals(value)) {
            return "null";
        }
        value = DateUtil.filterUSADateString(value);
        Matcher mth = NumberPtn.matcher(value);
        if (mth.matches()) {
            return "'" + value + "'";
        }
        return "'" + value + "'";
    }

    private String getArry(String[] arry, int index) {
        if (arry == null || arry.length == 0) {
        } else if (arry.length > index) {
            return arry[index];
        }
        return "XXXXXXXXXXX";
    }

    private void exportCurrentSystemToExcel() {
        mFastDBQueryUI_ExportXlsTableProcess.exportCurrentSystemToExcel(self);
    }

    private void exportCurrentSystemToCreateSQL(FastDBQueryUI_ExportSchemaTableToXls.DBType mDBType) {
        mFastDBQueryUI_ExportXlsTableProcess.exportCurrentSystemCreateSQL(mDBType, //
                self);
    }

    private void exportCurrentSystemToInsertSQL(DBDateUtil.DBDateFormat mDBType) {
        mFastDBQueryUI_ExportXlsTableProcess.exportCurrentSystemToInsertSQL(mDBType, self);
    }

    private String fixAppendLine(String text) {
        text = text.replaceAll("\\\\n", "");
        return text;
    }

    private void stringBuilderRemoveExecuteBtnAction(String sql, String paramName, JTextArea stringBuilderRemoveArea) {
        try {
            sql = StringUtils.defaultString(sql);
            paramName = StringUtils.trimToEmpty(paramName);

            Pattern stringBuilderPtn = Pattern.compile(paramName + "\\.append(\\(.*\\))");

            List<String> lines = StringUtil_.readContentToList(sql, true, true, false);
            Matcher mth = null;

            StringBuilder sb = new StringBuilder();

            for (String line : lines) {
                boolean appendOk = false;
                if (line.startsWith("//")) {
                    continue;
                }
                mth = stringBuilderPtn.matcher(line);
                while (mth.find()) {
                    String groupString = mth.group(1);
                    List<Pair<Integer, Integer>> lst = StringUtil_.caculateQuoteMap(groupString, '\"', '\"');
                    for (Pair<Integer, Integer> p : lst) {
                        String fixString = StringUtils.substring(groupString, p.getLeft() + 1, p.getRight());
                        sb.append(fixAppendLine(fixString));
                        appendOk = true;
                    }
                    if (lst.isEmpty()) {
                        sb.append(fixAppendLine(line));
                    }
                }
                if (appendOk) {
                    sb.append("\r\n");
                }
            }

            stringBuilderRemoveArea.setText(sb.toString());
        } catch (Exception ex) {
            JCommonUtil.handleException(ex);
        }
    }

    private void stringBuilderRemoveExecuteBtnAction_Simple(String sql, String paramName, JTextArea stringBuilderRemoveArea) {
        try {
            sql = StringUtils.defaultString(sql);
            paramName = StringUtils.trimToEmpty(paramName);

            List<String> lines = StringUtil_.readContentToList(sql, true, true, false);

            StringBuilder sb = new StringBuilder();

            for (String line : lines) {
                boolean appendOk = false;
                if (line.startsWith("//")) {
                    continue;
                }
                String groupString = line;
                List<Pair<Integer, Integer>> lst = StringUtil_.caculateQuoteMap(groupString, '\"', '\"');
                for (Pair<Integer, Integer> p : lst) {
                    String fixString = StringUtils.substring(groupString, p.getLeft() + 1, p.getRight());
                    sb.append(fixAppendLine(fixString));
                    appendOk = true;
                }
                if (lst.isEmpty()) {
                    sb.append(fixAppendLine(line));
                }
                if (appendOk) {
                    sb.append("\r\n");
                }
            }

            stringBuilderRemoveArea.setText(sb.toString());
        } catch (Exception ex) {
            JCommonUtil.handleException(ex);
        }
    }

    private void insertSqlFileBtnAction(String insertSqlFilePath, String tableSchema, DBType mDBType) {
        try {
            mFastDBQueryUI_ExportXlsTableProcess.importInsertSQLToExcelNSchema(self, insertSqlFilePath, tableSchema, mDBType);
        } catch (Exception ex) {
            JCommonUtil.handleException(ex);
        }
    }
}
