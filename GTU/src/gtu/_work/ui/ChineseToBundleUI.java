package gtu._work.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import gtu._work.ui.JMenuBarUtil.JMenuAppender;
import gtu.file.FileUtil;
import gtu.string.StringEx;
import gtu.string.StringEx.CharEx;
import gtu.string.StringUtil_;
import gtu.swing.util.HideInSystemTrayHelper;
import gtu.swing.util.JCommonUtil;
import gtu.swing.util.JFrameRGBColorPanel;
import gtu.swing.util.JFrameUtil;
import gtu.swing.util.JTableUtil;
import gtu.swing.util.JTextAreaUtil;
import gtu.swing.util.SwingActionUtil;
import gtu.swing.util.SwingActionUtil.Action;
import gtu.swing.util.SwingActionUtil.ActionAdapter;

public class ChineseToBundleUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private HideInSystemTrayHelper hideInSystemTrayHelper;
    private JFrameRGBColorPanel jFrameRGBColorPanel;
    private SwingActionUtil swingUtil;
    private JTabbedPane tabbedPane;
    private JPanel panel_2;
    private JPanel panel_3;
    private JPanel panel_4;
    private JPanel panel_5;
    private JPanel panel_6;
    private JTextArea contentArea;
    private JPanel panel_7;
    private JPanel panel_8;
    private JPanel panel_9;
    private JPanel panel_10;
    private JTable resultTable;
    private JButton executeBtn;
    private JButton clearBtn;
    private JTextField prefixText;
    private JLabel lblNewLabel;
    private JLabel lblNewLabel_1;
    private JTextField numberStartText;
    private JButton fixBtn;
    private JButton applyBtn;

    private FindTextContent mFindTextContent;
    private JPanel panel_11;
    private JTextArea resultTextArea1;
    private JTextArea resultTextArea2;
    private JLabel lblNewLabel_2;
    private JTextField suffixText;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        if (!JFrameUtil.lockInstance_delable(ChineseToBundleUI.class)) {
            return;
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ChineseToBundleUI frame = new ChineseToBundleUI();
                    gtu.swing.util.JFrameUtil.setVisible(true, frame);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public ChineseToBundleUI() {
        swingUtil = SwingActionUtil.newInstance(this);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 649, 489);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.addChangeListener((ChangeListener) ActionAdapter.ChangeListener.create(ActionDefine.JTabbedPane_ChangeIndex.name(), swingUtil));
        contentPane.add(tabbedPane, BorderLayout.CENTER);

        JPanel panel = new JPanel();
        tabbedPane.addTab("本文", null, panel, null);
        panel.setLayout(new BorderLayout(0, 0));

        panel_3 = new JPanel();
        panel.add(panel_3, BorderLayout.NORTH);

        panel_4 = new JPanel();
        panel.add(panel_4, BorderLayout.WEST);

        panel_5 = new JPanel();
        panel.add(panel_5, BorderLayout.EAST);

        panel_6 = new JPanel();
        panel.add(panel_6, BorderLayout.SOUTH);

        executeBtn = new JButton("執行");
        executeBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                swingUtil.invokeAction("executeBtn.click", e);
            }
        });
        panel_6.add(executeBtn);

        clearBtn = new JButton("清除");
        clearBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                swingUtil.invokeAction("clearBtn.click", e);
            }
        });
        panel_6.add(clearBtn);

        contentArea = new JTextArea();
        JTextAreaUtil.applyCommonSetting(contentArea);
        JCommonUtil.applyDropFiles(contentArea, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<File> lst = (List<File>) e.getSource();
                if (!lst.isEmpty()) {
                    String content = FileUtil.loadFromFile(lst.get(0), "UTF8");
                    contentArea.setText(content);
                }
            }
        });
        panel.add(JCommonUtil.createScrollComponent(contentArea), BorderLayout.CENTER);

        JPanel panel_1 = new JPanel();
        tabbedPane.addTab("更新", null, panel_1, null);
        panel_1.setLayout(new BorderLayout(0, 0));

        panel_7 = new JPanel();
        panel_1.add(panel_7, BorderLayout.NORTH);

        lblNewLabel = new JLabel("prefix");
        panel_7.add(lblNewLabel);

        prefixText = new JTextField();
        panel_7.add(prefixText);
        prefixText.setColumns(10);

        lblNewLabel_1 = new JLabel("數字");
        panel_7.add(lblNewLabel_1);

        numberStartText = new JTextField();
        numberStartText.setColumns(10);
        panel_7.add(numberStartText);

        fixBtn = new JButton("設定");
        fixBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                swingUtil.invokeAction("fixBtn.click", e);
            }
        });

        lblNewLabel_2 = new JLabel("suffix");
        panel_7.add(lblNewLabel_2);

        suffixText = new JTextField();
        suffixText.setColumns(10);
        panel_7.add(suffixText);
        panel_7.add(fixBtn);

        panel_8 = new JPanel();
        panel_1.add(panel_8, BorderLayout.WEST);

        panel_9 = new JPanel();
        panel_1.add(panel_9, BorderLayout.EAST);

        panel_10 = new JPanel();
        panel_1.add(panel_10, BorderLayout.SOUTH);

        applyBtn = new JButton("應用");
        applyBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                swingUtil.invokeAction("applyBtn.click", e);
            }
        });
        panel_10.add(applyBtn);

        resultTable = new JTable();
        JTableUtil.defaultSetting(resultTable);
        panel_1.add(JCommonUtil.createScrollComponent(resultTable), BorderLayout.CENTER);

        panel_11 = new JPanel();
        tabbedPane.addTab("結果", null, panel_11, null);

        resultTextArea1 = new JTextArea();
        resultTextArea1.setColumns(30);
        resultTextArea1.setRows(10);
        panel_11.add(JCommonUtil.createScrollComponent(resultTextArea1));

        resultTextArea2 = new JTextArea();
        resultTextArea2.setRows(10);
        resultTextArea2.setColumns(30);
        panel_11.add(JCommonUtil.createScrollComponent(resultTextArea2));

        panel_2 = new JPanel();
        tabbedPane.addTab("其他設定", null, panel_2, null);
        panel_2.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        {
            // 掛載所有event
            applyAllEvents();

            JCommonUtil.setJFrameCenter(this);
            JCommonUtil.setJFrameIcon(this, "resource/images/ico/tk_aiengine.ico");
            hideInSystemTrayHelper = HideInSystemTrayHelper.newInstance();
            hideInSystemTrayHelper.apply(this);
            jFrameRGBColorPanel = new JFrameRGBColorPanel(this);
            panel_2.add(jFrameRGBColorPanel.getToggleButton(false));
            panel_2.add(hideInSystemTrayHelper.getToggleButton(false));
            this.applyAppMenu();
            JCommonUtil.defaultToolTipDelay();
            this.setTitle("You Set My World On Fire");
        }
    }

    private enum ActionDefine {
        TEST_DEFAULT_EVENT, //
        JTabbedPane_ChangeIndex, //
        ;
    }

    private void applyAllEvents() {
        swingUtil.addActionHex(ActionDefine.TEST_DEFAULT_EVENT.name(), new Action() {
            @Override
            public void action(EventObject evt) throws Exception {
                System.out.println("====Test Default Event!!====");
            }
        });
        swingUtil.addActionHex(ActionDefine.JTabbedPane_ChangeIndex.name(), new Action() {
            @Override
            public void action(EventObject evt) throws Exception {
                System.out.println("tabbedPane : " + tabbedPane.getSelectedIndex());
            }
        });
        swingUtil.addActionHex("executeBtn.click", new Action() {
            @Override
            public void action(EventObject evt) throws Exception {
                mFindTextContent = new FindTextContent();
                mFindTextContent.execute(contentArea.getText());
                mFindTextContent.applyToJTable();
            }
        });
        swingUtil.addActionHex("clearBtn.click", new Action() {
            @Override
            public void action(EventObject evt) throws Exception {
                resetResultTable();
                prefixText.setText("");
                numberStartText.setText("");
                contentArea.setText("");
                resultTextArea1.setText("");
                resultTextArea2.setText("");
                suffixText.setText("");
            }
        });
        swingUtil.addActionHex("fixBtn.click", new Action() {

            @Override
            public void action(EventObject evt) throws Exception {
                JTableUtil utl = JTableUtil.newInstance(resultTable);
                String prefix = prefixText.getText();
                String suffix2 = suffixText.getText();
                int start = 0;
                try {
                    start = Integer.parseInt(numberStartText.getText());
                } catch (Exception ex) {
                }
                DefaultTableModel model = (DefaultTableModel) resultTable.getModel();
                int index = 0;
                int padding = StringUtils.length(numberStartText.getText());
                for (int ii = 0; ii < model.getRowCount(); ii++) {
                    Boolean usage = (Boolean) utl.getValueAt(false, ii, ColumnDefZ.Usage.ordinal());
                    if (usage) {
                        String suffix = StringUtils.leftPad(String.valueOf((index + start)), padding, '0');
                        utl.setValueAt(false, prefix + suffix + suffix2, ii, ColumnDefZ.Replace.ordinal());
                        index++;
                    }
                }
            }
        });
        swingUtil.addActionHex("applyBtn.click", new Action() {
            private String getValue(String text) {
                char c1 = text.charAt(0);
                char c2 = text.charAt(text.length() - 1);
                if ((c1 == '\'' && c2 == '\'') || (c1 == '\"' && c2 == '\"')) {
                    return text.substring(1, text.length() - 1);
                }
                return text;
            }

            @Override
            public void action(EventObject evt) throws Exception {
                if (mFindTextContent == null) {
                    JCommonUtil._jOptionPane_showMessageDialog_error("請先執行!");
                    return;
                }

                JTableUtil utl = JTableUtil.newInstance(resultTable);
                DefaultTableModel model = (DefaultTableModel) resultTable.getModel();
                StringBuffer sb = new StringBuffer();
                for (int ii = 0; ii < model.getRowCount(); ii++) {
                    Boolean usage = (Boolean) utl.getValueAt(false, ii, ColumnDefZ.Usage.ordinal());
                    String text = (String) utl.getValueAt(false, ii, ColumnDefZ.Chinese.ordinal());
                    String tag = (String) utl.getValueAt(false, ii, ColumnDefZ.Replace.ordinal());
                    StringEx vo = (StringEx) utl.getValueAt(false, ii, ColumnDefZ.VO.ordinal());
                    if (usage) {
                        List<StringEx> lst = mFindTextContent.string.findAll(text);
                        for (StringEx e : lst) {
                            mFindTextContent.string.replace(e, tag);
                            sb.append(tag + "=" + getValue(text) + "\r\n");
                        }
                    }
                }

                String contentStr = StringUtil_.readContentAgain(sb.toString(), false, true, true);
                resultTextArea1.setText(mFindTextContent.string.getContext());
                resultTextArea2.setText(sb.toString());
            }
        });
    }

    private class FindTextContent {
        StringEx string;
        List<StringEx> lst = new ArrayList<StringEx>();

        public void execute(String context) {
            string = new StringEx();
            string.appendContent(context);
            CharEx[] arry = string.toCharArray();

            int startPos = -1;
            char startChar = ' ';

            for (int ii = 0; ii < arry.length; ii++) {
                CharEx c = arry[ii];
                if (startPos == -1) {
                    if (StringUtil_.hasChineseWord2("" + c.c())) {
                        B: for (int jj = ii; jj > 0; jj--) {
                            if (ArrayUtils.contains(new char[] { '\'', '\"' }, arry[jj].c())) {
                                if (jj - 1 >= 0) {
                                    if (arry[jj - 1].c() != '\\') {
                                        startPos = jj;
                                        startChar = arry[jj].c();
                                        break B;
                                    }
                                }
                            } else if (ArrayUtils.contains(new char[] { '>' }, arry[jj].c())) {
                                if (jj + 1 <= arry.length - 1) {
                                    startPos = jj + 1;
                                    startChar = arry[jj + 1].c();
                                    break B;
                                }
                            }
                        }
                    }
                } else {
                    if (ArrayUtils.contains(new char[] { startChar }, arry[ii].c())) {
                        if (ii + 1 <= arry.length - 1) {
                            if (arry[ii + 1].c() != '\\') {
                                StringEx text = string.substring(startPos, ii);
                                lst.add(text);
                                startPos = -1;
                            }
                        }
                    } else if (startChar == '>' && arry[ii].c() == '<') {
                        if (ii - 1 >= 0) {
                            StringEx text = string.substring(startPos, ii - 1);
                            lst.add(text);
                            startPos = -1;
                        }
                    }
                }
            }
        }

        public void applyToJTable() {
            DefaultTableModel model = resetResultTable();
            resultTable.setModel(model);
            for (StringEx p : lst) {
                model.addRow(new Object[] { true, p.getContext(), "", p });
            }
        }
    }

    private enum ColumnDefZ {
        Usage, Chinese, Replace, VO;
    }

    private DefaultTableModel resetResultTable() {
        DefaultTableModel model = JTableUtil.createModel(false, "使用", "捕獲中文", "取代標籤", "VO");
        resultTable.setModel(model);
        JTableUtil.setColumnWidths_Percent(resultTable, new float[] { 5, 45, 45, 5 });
        return model;
    }

    private void applyAppMenu() {
        JMenu menu1 = JMenuAppender.newInstance("child_item")//
                .addMenuItem("detail1", (ActionListener) ActionAdapter.ActionListener.create(ActionDefine.TEST_DEFAULT_EVENT.name(), getSwingUtil()))//
                .getMenu();
        JMenu mainMenu = JMenuAppender.newInstance("file")//
                .addMenuItem("item1", null)//
                .addMenuItem("item2", (ActionListener) ActionAdapter.ActionListener.create(ActionDefine.TEST_DEFAULT_EVENT.name(), getSwingUtil()))//
                .addChildrenMenu(menu1)//
                .getMenu();
        JMenuBarUtil.newInstance().addMenu(mainMenu).apply(this);
    }

    public SwingActionUtil getSwingUtil() {
        return swingUtil;
    }
}
