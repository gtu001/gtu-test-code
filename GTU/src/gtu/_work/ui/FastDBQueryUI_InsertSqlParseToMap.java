package gtu._work.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import gtu.file.FileUtil;
import gtu.string.StringUtil_;

public class FastDBQueryUI_InsertSqlParseToMap {

    public static Map<String, String> parseSql(String sqlText) {
        Map<String, String> map = new LinkedHashMap<String, String>();

        String columnText = "";
        String valuesText = "";

        sqlText = StringUtils.defaultString(sqlText);
        Pattern ptn = Pattern.compile("\\)[\\s\t]*VALUES[\\s\t]*\\(");
        Pattern ptn1 = Pattern.compile("\\(");
        Pattern ptn2 = Pattern.compile(".*(\\))");
        Matcher mth = ptn.matcher(sqlText);
        if (mth.find()) {
            int beforeEnd = mth.start();
            int afterStart = mth.end();

            Matcher mth1 = ptn1.matcher(sqlText);
            if (mth1.find()) {
                int beforeStart = mth1.start() + 1;
                columnText = StringUtils.substring(sqlText, beforeStart, beforeEnd);
            }

            Matcher mth2 = ptn2.matcher(sqlText);
            if (mth2.find()) {
                int afterEnd = mth2.start(1);
                valuesText = StringUtils.substring(sqlText, afterStart, afterEnd);
            }
        }

        String[] columns = columnText.split(",", -1);

        Pattern ptnQ = Pattern.compile("(\\\\'|')");
        Matcher mthQ = ptnQ.matcher(valuesText);

        List<Integer> commaLst = new ArrayList<Integer>();
        while (mthQ.find()) {
            String textq = mthQ.group();
            if ("\\'".equals(textq)) {
                continue;
            }
            commaLst.add(mthQ.start(1));
        }

        System.out.println(valuesText);

        List<String> values = new ArrayList<String>();
        ScannerTool mScannerTool = new ScannerTool(valuesText);
        String tempVal = null;

        String checkParseString = "";
        while (true) {
            checkParseString = mScannerTool.text.toString();

            // 'xxxxxxxx'
            // =============================================================

            if (mScannerTool.isStartWithIgnoreCase("'")) {
                tempVal = mScannerTool.cutBySingleQuote(1);
            }

            if (appendToLst(tempVal, values, mScannerTool)) {
                tempVal = null;
                continue;
            }

            // 數字 =============================================================

            if (tempVal == null) {
                if (mScannerTool.isMatchWithPattern("^\\d+")) {
                    tempVal = mScannerTool.cutByPattern("[\\-\\d\\.Ee]+", 0);
                }
            }

            if (appendToLst(tempVal, values, mScannerTool)) {
                tempVal = null;
                continue;
            }

            // 方法 =============================================================

            if (tempVal == null) {
                String methodVal = mScannerTool.cutWithMethod();
                if (StringUtils.isNotBlank(methodVal)) {
                    tempVal = methodVal;
                }
            }

            if (appendToLst(tempVal, values, mScannerTool)) {
                tempVal = null;
                continue;
            }

            // null
            // =============================================================

            if (tempVal == null) {
                String nullVal = mScannerTool.cutWithNull();
                if (StringUtils.isNotBlank(nullVal)) {
                    tempVal = nullVal;
                }
            }

            if (appendToLst(tempVal, values, mScannerTool)) {
                tempVal = null;
                continue;
            }

            // oracle sequence
            // =============================================================

            if (tempVal == null) {
                if (mScannerTool.isMatchWithPattern("\\w+\\.nextval")) {
                    tempVal = mScannerTool.cutByPattern("\\w+\\.nextval", 0);
                }
            }

            if (appendToLst(tempVal, values, mScannerTool)) {
                tempVal = null;
                continue;
            }

            // =============================================================

            if (StringUtils.isBlank(mScannerTool.text)) {
                break;
            }

            if (StringUtils.equals(checkParseString, mScannerTool.text.toString())) {
                System.out.println("InsertSQL判定失敗  : " + checkParseString);
                break;
            }
        }

        System.out.println(values);

        if (columns.length == values.size()) {
            for (int ii = 0; ii < columns.length; ii++) {
                String column = columns[ii];
                String value = values.get(ii);
                map.put(column, value);
            }
        }
        return map;
    }

    public static void main(String[] args) {
        File file = new File("C:\\Users\\wistronits\\Downloads\\insertSQL.txt");
        List<String> lst = FileUtil.loadFromFile_asList(file, "UTF8");
        String line = lst.get(0);
        Map<String, String> map = parseSql(line);
        System.out.println(map);
    }

    private static boolean appendToLst(String tempVal, List<String> values, ScannerTool mScannerTool) {
        if (tempVal != null) {
            values.add(tempVal);
            if (mScannerTool.isStartWithIgnoreCase(",")) {
                tempVal = mScannerTool.cutByPattern(",", 0);
            }
            return true;
        }
        return false;
    }

    private static class ScannerTool {

        String text;

        private ScannerTool(String text) {
            this.text = text;
        }

        boolean isStartWithIgnoreCase(String with) {
            String text1 = StringUtils.trimToEmpty(text);
            boolean result = text1.toLowerCase().startsWith(with.toLowerCase());
            // System.out.println("isStartWithIgnoreCase = " + with);
            return result;
        }

        boolean isMatchWithPattern(String with) {
            String text1 = StringUtils.trimToEmpty(text);
            Pattern ptn = Pattern.compile(with, Pattern.CASE_INSENSITIVE);
            Matcher mth = ptn.matcher(text1);
            if (mth.find()) {
                return true;
            }
            return false;
        }

        String cutWithNull() {
            text = StringUtil_.trimLeftSpace(text);
            if (text.toString().toLowerCase().startsWith("null")) {
                text = StringUtils.substring(text, 4);
                System.out.println("\tcutWithNull : " + "null");
                return "null";
            }
            return null;
        }

        String cutWithMethod() {
            text = StringUtil_.trimLeftSpace(text);
            List<Pair<Integer, Integer>> lst = StringUtil_.caculateQuoteMap(text, '(', ')');
            if (lst.isEmpty()) {
                return null;
            }
            Collections.sort(lst, new java.util.Comparator<Pair<Integer, Integer>>() {
                @Override
                public int compare(Pair<Integer, Integer> paramT1, Pair<Integer, Integer> paramT2) {
                    return new Integer(paramT1.getLeft()).compareTo(paramT2.getLeft());
                }
            });
            String rtnVal = StringUtils.substring(text, 0, lst.get(0).getRight() + 1);
            if (rtnVal.matches("[\\w\\.]+\\(.*")) {
                text = StringUtils.substring(text, lst.get(0).getRight() + 1);
                System.out.println("\tcutWithMethod : " + rtnVal);
                return rtnVal;
            }
            return null;
        }

        String cutBySingleQuote(int times) {
            Pattern ptn = Pattern.compile("(\\\\'|')", Pattern.CASE_INSENSITIVE);
            text = StringUtil_.trimLeftSpace(text);
            Matcher mth = null;

            StringBuffer prepend = new StringBuffer();
            int time = 0;
            while (true) {
                mth = ptn.matcher(text);
                if (mth.find()) {
                    if (mth.group(1).equals("\\'")) {
                        prepend.append(StringUtils.substring(text, 0, mth.end(1)));
                        text = StringUtils.substring(text, mth.end(1));
                        continue;
                    }
                    int start = mth.start();
                    String rtnVal = StringUtils.substring(text, 0, start);
                    int end = mth.end();
                    text = StringUtils.substring(text, end);
                    if (time == times) {
                        System.out.println("\tcutBySingleQuote : " + prepend + rtnVal);
                        return prepend + rtnVal;
                    }
                    time++;
                } else {
                    break;
                }
            }
            return null;
        }

        String cutByPattern(String with, int times) {
            Pattern ptn = Pattern.compile(with, Pattern.CASE_INSENSITIVE);
            text = StringUtil_.trimLeftSpace(text);
            Matcher mth = null;

            int time = 0;
            while (true) {
                mth = ptn.matcher(text);
                if (mth.find()) {
                    int start = mth.start();
                    int end = mth.end();
                    String rtnVal = StringUtils.substring(text, 0, end);
                    text = StringUtils.substring(text, end);
                    if (time == times) {
                        System.out.println("\tcutByPattern[" + with + "][" + times + "] : " + rtnVal);
                        return rtnVal;
                    }
                    time++;
                } else {
                    break;
                }
            }
            return null;
        }
    }
}
