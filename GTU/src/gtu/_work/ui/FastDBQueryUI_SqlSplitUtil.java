package gtu._work.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import gtu.file.FileUtil;

public class FastDBQueryUI_SqlSplitUtil {

    public static void main(String[] args) {
        File file = new File("C:\\Users\\wistronits\\Desktop\\新文字文件.txt");
        String content = FileUtil.loadFromFile(file, "UTF8");

        FastDBQueryUI_SqlSplitUtil mFastDBQueryUI_SqlSplitUtil = new FastDBQueryUI_SqlSplitUtil();
        List<String> sqlLst = mFastDBQueryUI_SqlSplitUtil.execute(content);
        for (String sql : sqlLst) {
            System.out.println("sql : [ " + sql + " ] ");
        }
    }

    private FastDBQueryUI_SqlSplitUtil() {
    }

    public static final FastDBQueryUI_SqlSplitUtil _INST = new FastDBQueryUI_SqlSplitUtil();

    public static FastDBQueryUI_SqlSplitUtil getInstance() {
        return _INST;
    }

    public List<String> execute(String sqlContent) {
        sqlContent = removeComment(sqlContent);
        List<Pair<Integer, Integer>> singleQuoteLst = getSingleQuoteLst(sqlContent);
        List<String> sqlLst = getSqlLst(sqlContent, singleQuoteLst);
        return sqlLst;
    }

    Pattern commentPtn = Pattern.compile("\\-{2}.*?\n", Pattern.DOTALL | Pattern.MULTILINE);
    Pattern commentPtn2 = Pattern.compile("\\/\\*(?:(.|\n)*?)\\*\\/", Pattern.DOTALL | Pattern.MULTILINE);
    Pattern commaPtn = Pattern.compile("\\;", Pattern.MULTILINE | Pattern.DOTALL);

    private String removeComment(String content) {
        content = StringUtils.defaultString(content);
        Matcher mth = commentPtn.matcher(content);
        StringBuffer sb = new StringBuffer();
        while (mth.find()) {
            mth.appendReplacement(sb, StringUtils.leftPad("", StringUtils.length(mth.group())));
        }
        mth.appendTail(sb);
        content = sb.toString();
        mth = commentPtn2.matcher(content);
        sb.setLength(0);
        while (mth.find()) {
            mth.appendReplacement(sb, StringUtils.leftPad("", StringUtils.length(mth.group())));
        }
        mth.appendTail(sb);
        return sb.toString();
    }

    private List<String> getSqlLst(String content, List<Pair<Integer, Integer>> singleQuoteLst) {
        List<String> sqlLst = new ArrayList<String>();
        content = StringUtils.defaultString(content);
        Matcher mth = commaPtn.matcher(content);
        int lastestPos = 0;
        A: while (mth.find()) {
            int pos2 = mth.start();
            for (Pair<Integer, Integer> p : singleQuoteLst) {
                if (p.getLeft() <= pos2 && pos2 <= p.getRight() + 1) {
                    continue A;
                }
            }
            String sql = StringUtils.substring(content, lastestPos, pos2);
            sqlLst.add(sql);
            lastestPos = mth.end();
        }
        String sql = StringUtils.substring(content, lastestPos);
        if (StringUtils.isNotBlank(sql)) {
            sqlLst.add(sql);
        }
        return sqlLst;
    }

    private List<Pair<Integer, Integer>> getSingleQuoteLst(String content) {
        List<Pair<Integer, Integer>> lst = new ArrayList<Pair<Integer, Integer>>();
        Stack<Integer> stack = new Stack<Integer>();
        char[] arry = StringUtils.defaultString(content).toCharArray();
        for (int ii = 0; ii < arry.length; ii++) {
            char c = arry[ii];
            if (c == '\'') {
                if (stack.isEmpty()) {
                    stack.add(ii);
                } else {
                    if (ii - 1 > 0 && arry[ii - 1] == '\\') {
                        continue;
                    } else {
                        lst.add(Pair.of(stack.pop(), ii));
                    }
                }
            }
        }
        return lst;
    }
}