package gtu.runtime;

import java.io.IOException;

import com.profesorfalken.jpowershell.PowerShell;

public class PowerShellCommandTest001 {

    /*
     * https://github.com/profesorfalken/jPowerShell
     */
    public static void main(String[] args) throws IOException {
        String command = "Get-ItemProperty " + "HKLM:\\Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\* "
                + "| Select-Object DisplayName, DisplayVersion, Publisher, InstallDate " + "| Format-Table –AutoSize";
        System.out.println(PowerShell.executeSingleCommand(command).getCommandOutput());
        System.out.println("Done");
    }
}