package gtu.runtime;

import java.io.File;
import java.io.IOException;

import gtu.file.FileUtil;

public class RunPowerShellPromptModeUtil {

    public static void main(String[] args) {
        String command = "Get-Content \"C:/Users/gtu00/AppData/Local/Unity/Editor/Editor.log\" -Wait -Tail 30";
        RunPowerShellPromptModeUtil.runAtScript(command);
    }

    /**
     * 開用管理者模式開啟 cmd
     * 輸入 Get-ExecutionPolicy
     * 輸入 Set-ExecutionPolicy Unrestricted 
     * @param scriptContent
     * @return
     */
    public static Process runAtScript(String scriptContent) {
        Process process = null;
        try {
            File tempFile = File.createTempFile("PowerShell_", ".ps1");
            FileUtil.saveToFile(tempFile, scriptContent, "UTF8");
            String command = String.format("start powershell -noexit -file \"%s\"", tempFile);
            System.out.println(command);

            RuntimeBatPromptModeUtil inst = RuntimeBatPromptModeUtil.newInstance();
            inst.command(command);
            process = inst.apply();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return process;
    }

    public static Process runCommand(String commandContent) {
        String command = String.format("start powershell -noexit \"%s\"", commandContent);
        System.out.println(command);

        RuntimeBatPromptModeUtil inst = RuntimeBatPromptModeUtil.newInstance();
        inst.command(command);
        return inst.apply();
    }
}
