package gtu.runtime;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import gtu.file.FileUtil;
import gtu.file.OsInfoUtil;
import gtu.log.JdkLoggerUtil;

public class DesktopUtil {

    private static Logger logger = Logger.getLogger(DesktopUtil.class.getSimpleName());

    private static boolean isWindows = false;

    private static char[] UNENCODE_CHARACTERS_URLENCODE = "!#$&'()*+,/:;=?@[]".toCharArray();
    private static Pattern UNENCODE_PREFIX_PTN = Pattern.compile("^(\\w+\\:\\/+)(.+)");

    static {
        JdkLoggerUtil.setupRootLogLevel(Level.INFO);
        logger.setLevel(Level.ALL);

        if (System.getProperty("os.name").startsWith("Windows")) {
            isWindows = true;
        } else if ("Linux".equals(System.getProperty("os.name"))) {
            isWindows = false;
        }
    }

    public static void main(String[] args) {
        String testURL = urlDecode("file://C:/Users/gtu00/Downloads/勇者鬥惡龍1+++", true);
        System.out.println(testURL);

        DesktopUtil.browseFileDirectory(new File("/Users/user/Desktop/workstuff/gtu-test-code/GTU/target/classes/gtu/_work/ui/DropboxTestUI_config.properties"));
        System.out.println("done...");
    }

    public static boolean isFile(String url) {
        try {
            url = URLDecoder.decode(url, "UTF-8");
            if (new File(url).exists()) {
                return true;
            }
            try {
                if (new URL(url).getProtocol().equals("file")) {
                    return true;
                }
            } catch (java.net.MalformedURLException ex) {
            }
            return false;
        } catch (Exception e) {
            throw new RuntimeException("isFile ERR : " + e.getMessage(), e);
        }
    }

    public static File getFile_ignoreFailed(String url) {
        try {
            return getFile(url);
        } catch (Throwable ex) {
            System.err.println("<getFile_ignoreFailed> WARNING : " + ex.getMessage());
            return null;
        }
    }

    public static File getFile(String url) {
        try {
            if (!isFile(url)) {
                return null;
            }
            url = urlDecode(url, true);
            File tempFile = null;
            // try 1
            if ((tempFile = new File(url)).exists()) {
                return tempFile;
            }
            if (url.startsWith("file:")) {
                // try 2
                if ((tempFile = new File(new URL(url).getFile())).exists()) {
                    return tempFile;
                }
                // try 3
                Pattern ptn = Pattern.compile("file\\:[\\/]+(.*)");
                Matcher mth = ptn.matcher(url);
                if (mth.find()) {
                    if ((tempFile = new File(mth.group(1))).exists()) {
                        return tempFile;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException("getFile ERR : " + e.getMessage(), e);
        }
    }

    public static void openDir(String url) {
        try {
            File f = getFile(url);
            openDir(f);
        } catch (Exception e) {
            throw new RuntimeException("openDir ERR : " + e.getMessage(), e);
        }
    }

    /**
     * 使用 browseFileDirectory 取代
     * 
     * @param f
     */
    @Deprecated
    public static void openDir(File f) {
        try {
            if (f == null) {
                return;
            }
            if (f.isFile()) {
                f = f.getParentFile();
            }
            if (f.isDirectory()) {
                Desktop.getDesktop().open(f);
            }
            if (!f.exists()) {
                Desktop.getDesktop().open(FileUtil.getParentFolder(f));
            }
        } catch (Exception e) {
            throw new RuntimeException("openDir ERR : " + e.getMessage(), e);
        }
    }

    public static void browseFileDirectory(File file) {
        try {
            if (OsInfoUtil.isWindows()) {
                Runtime.getRuntime().exec("explorer.exe /select,\"" + file + "\"");
            } else if (OsInfoUtil.isMac()) {
                Runtime.getRuntime().exec(new String[] { "open", "-R", file.getAbsolutePath() });
            } else {
                openDir(file);
            }
        } catch (IOException e) {
            throw new RuntimeException("browseFileDirectory ERR : " + e.getMessage(), e);
        }
    }

    public static void browse(String url) {
        try {
            URL url2 = new URL(url);
            logger.log(Level.INFO, "url : " + url2);
            if (url.startsWith("file:")) {
                try {
                    Desktop.getDesktop().browse(url2.toURI());// 嘗試正常流程
                } catch (Exception ex) {
                    logger.log(Level.WARNING, "browse try 1 : " + ex.getMessage());
                    File file = getFile(url);
                    logger.log(Level.WARNING, "file : " + file);
                    if (file == null || !file.exists()) {
                        throw new Exception("file : " + file + " not exists !!");
                    } else {
                        try {
                            if (isWindows) {
                                Desktop.getDesktop().open(file);
                            } else {
                                RuntimeBatPromptModeUtil inst = RuntimeBatPromptModeUtil.newInstance();
                                inst.command(file.getAbsolutePath());
                                inst.apply();
                            }
                        } catch (Exception ex1) {
                            logger.log(Level.WARNING, "browse try 2 : " + ex1.getMessage());
                            Runtime.getRuntime().exec(String.format("cmd /c start notepad \"%s\"", file));
                        }
                    }
                }
            } else {
                Desktop.getDesktop().browse(url2.toURI());
            }
        } catch (Exception ex) {
            throw new RuntimeException("browse ERR : " + ex.getMessage() + " --> " + url, ex);
        }
    }

    public static String urlDecode(String url, boolean isWithPrefix) {
        String prefix = "";
        if (isWithPrefix) {
            Matcher mth = UNENCODE_PREFIX_PTN.matcher(url);
            if (mth.find()) {
                prefix = mth.group(1);
                url = mth.group(2);
            }
        }
        if (!DesktopUtil.isDoneURLEncode(url)) {
            try {
                url = URLDecoder.decode(url, "UTF-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }
        if (StringUtils.isNotBlank(prefix)) {
            url = prefix + url;
        }
        return url;
    }

    public static boolean isDoneURLEncode(String url) {
        char[] arry = url.toCharArray();
        for (char a : arry) {
            for (char b : UNENCODE_CHARACTERS_URLENCODE) {
                if (a == b) {
                    return true;
                }
            }
        }
        return false;
    }
}
