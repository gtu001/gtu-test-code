package gtu.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tt289.common.json.JsonResponse;
import com.tt289.common.util.RedisUtil;
import com.tt289.data.config.RedisKey;
import com.tt289.data.entity.Members;
import com.tt289.data.util.DataConstants;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * bypass jwt auth
 * 
 * 在 HttpSecurity.addFilterBefore(mTroyChangByPassJWTAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
 * 即可
 * 
 * @author TroyChang
 *
 */
@Slf4j
@Component
public class TroyChangByPassJWTAuthenticationFilter extends OncePerRequestFilter {
//public class JWTAuthenticationFilter extends GenericFilterBean  {

	public static final int NORMAL_TOKEN = 0;
	public static final int REFRESH_TOKEN = 1;

	private static String PUBLIC_TOKEN;

	public TroyChangByPassJWTAuthenticationFilter() {
		PUBLIC_TOKEN = createToken();
	}

	@Resource
	private UserDetailsService userDetailsService;

	@Value(value = "${jwt.secret}")
	private String secret = "goss";

	@Value(value = "${jwt.expiration:4320}")
	private int expiration = 4320;

	@Value(value = "${jwt.refresh.expiration:10080}")
	private int refreshExpiration = 10080;

	/**
	 * 解密jwt
	 * @param jwt
	 * @return
	 * @throws Exception
	 */
	public Claims parseJWT(String jwt) {
		SecretKey key = generalKey();  //签名秘钥，和生成的签名的秘钥一模一样
		Claims claims = Jwts.parser()  //得到DefaultJwtParser
				.setSigningKey(key)         //设置签名的秘钥
				.parseClaimsJws(jwt).getBody();//设置需要解析的jwt
		return claims;
	}

	/**
	 * 由字符串生成加密key
	 *
	 * @return
	 */
	public SecretKey generalKey() {
		// String stringKey = "abcde";//Constant.JWT_SECRET;//本地配置文件中加密的密文7786df7fc3a34e26a61c034d5ec8245d

		byte[] encodedKey = Base64.getEncoder().encode(secret.getBytes(StandardCharsets.UTF_8));//本地的密码解码[B@152f6e2
		// System.out.println(encodedKey);//[B@152f6e2
		// System.out.println(Base64.encodeBase64URLSafeString(encodedKey));//7786df7fc3a34e26a61c034d5ec8245d
		/*
		 * 根据给定的字节数组使用AES加密算法构造一个密钥，使用 encodedKey中的始于且包含 0 到前 leng 个字节这是当然是所有。
		 */
		SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
		return key;
	}

	public String getToken(HttpServletRequest request) {
		Cookie cookie = WebUtils.getCookie(request, HttpHeaders.AUTHORIZATION);
		if (Objects.nonNull(cookie)) return cookie.getValue();

		return request.getHeader(HttpHeaders.AUTHORIZATION);
	}


	/**
	 * @param user 用戶
	 * @return token string
	 * @throws Exception
	 * @Param tokenType
	 * @description
	 * @Author Allen
	 * @CreatTime 2020年3月11日下午3:11:14
	 */
	public String createJWT(Members user, int tokenType) throws Exception {
		/*
		 *  指定签名的时候使用的签名算法，也就是header那部分，Jwt已经将这部分内容封装好了。
		 * */
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
//        long nowMillis = System.currentTid

		/*
		 * 创建payload的私有声明（根据特定的业务需要添加，如果要拿这个做验证，一般是需要和jwt的接收方提前沟通好验证方式的）
		 * */

		Map<String, Object> claims = new HashMap<>();//
//        claims.put("uid", "DSSFAWDWADAS...");
//        claims.put("username", user.getUsername());
		claims.put("uid", user.getUid());

		/*
		 *生成签名的时候使用的秘钥secret,这个方法本地封装了的，一般可以从本地配置文件中读取，切记这个秘钥不能外露。
		 *它就是你服务端的私钥，在任何场景都不应该流露出去。一旦客户端得知这个secret, 那就意味着客户端是可以自我签发jwt了。
		 **/
		SecretKey key = generalKey();
//        Date date = new Date(System.currentTimeMillis()+expiration);
		Calendar cal = Calendar.getInstance();
		if (tokenType == NORMAL_TOKEN)
			cal.add(Calendar.MINUTE, expiration);
		else
			cal.add(Calendar.MINUTE, refreshExpiration);

		JwtBuilder builder = Jwts.builder()
				.setClaims(claims)          //如果有私有声明，一定要先设置这个自己创建的私有的声明，这个是给builder的claim赋值，一旦写在标准的声明赋值之后，就是覆盖了那些标准的声明的
				// .setId(id)                //设置jti(JWT ID)：是JWT的唯一标识，根据业务需要，这个可以设置为一个不重复的值，主要用来作为一次性token,从而回避重放攻击。
				.setIssuedAt(new Date())        //iat: jwt的签发时间
				.setSubject(user.getUsername()) //sub(Subject)：代表这个JWT的主体，即它的所有人，这个是一个json格式的字符串，可以存放什么userid，roldid之类的，作为什么用户的唯一标志。
				.setExpiration(cal.getTime())
				.signWith(signatureAlgorithm, key);//设置签名使用的签名算法和签名使用的秘钥
		/*
		 *开始压缩为xxxxxxxxxxxxxx.xxxxxxxxxxxxxxx.xxxxxxxxxxxxx这样的jwt
		 **/
		return builder.compact();
	}

	public String createToken()  {
		Members vo = new Members();
		vo.setUid(996874);
		vo.setIsDelete(0);
		vo.setEnable(1);
		vo.setGudongId(0);
		vo.setZparentId(0);
		vo.setParentId(0);
		vo.setParents("996874");
		vo.setAdmin(0);
		vo.setUsername("troy");
		vo.setPhone("");
		vo.setPassword("e10adc3949ba59abbe56e057f20f883e");
		vo.setCoinPassword("fcea920f7412b5da7be0cf42b8c93759");
		vo.setCredit(0);
		vo.setType(0);
		vo.setNickname("troy001");//這裡看前後台要改 TODO
		vo.setName("troy");
		vo.setRegIP("35.187.204.224");
		vo.setRegTime(1629867296L);
		vo.setUpdateTime(java.sql.Timestamp.valueOf("2021-08-26 07:58:34.0"));
		vo.setUpdateFandian(0);
		vo.setCoin(java.math.BigDecimal.valueOf(10000199.000D));
		vo.setFanDian(java.math.BigDecimal.valueOf(0.5D));
		vo.setJnfanDian(java.math.BigDecimal.valueOf(0.5D));
		vo.setDzfanDian(java.math.BigDecimal.valueOf(0.5D));
		vo.setTufanDian(java.math.BigDecimal.valueOf(0.5D));
		vo.setQpfanDian(java.math.BigDecimal.valueOf(0.5D));
		vo.setDjfanDian(java.math.BigDecimal.valueOf(0.5D));
		vo.setBufanDian(java.math.BigDecimal.valueOf(0.0D));
		vo.setEmail("");
		vo.setQq("");
		//vo.setWeChat("");
		vo.setPhonearea("");
		vo.setSrc("");
		vo.setTestFlag(0);
		vo.setGrade(0);
		vo.setScore(java.math.BigDecimal.valueOf(0L));
		vo.setScoreTotal(java.math.BigDecimal.valueOf(0L));
		vo.setMemo("");
		vo.setBgclose(1);
		vo.setYqmId(0);
		vo.setGroupId(0);
		vo.setWithdrawLimit(java.math.BigDecimal.valueOf(200.000D));
		vo.setTransferSwitch(0);
		vo.setAutoFund(0);
		vo.setIdCardNumber("");
		vo.setIsNew(-1);
		vo.setLoginCount(35);
		vo.setRechargeVol(java.math.BigDecimal.valueOf(200.000D));
		vo.setRechargeCount(2);
		vo.setCashVol(java.math.BigDecimal.valueOf(0.000D));
		vo.setCashCount(0);
		vo.setDnsTrueCount(0);
		vo.setDnsFalseCount(35);
		vo.setCity("");
		vo.setProvince("");
		vo.setUuid("b11ca322-4410-489d-82c4-487f45186e59");
		vo.setRisk(0);
		vo.setBirthday(java.sql.Date.valueOf("2003-08-30"));
		vo.setAllWithdrawLimit(java.math.BigDecimal.valueOf(200.000D));
		vo.setTuWithdrawLimit(java.math.BigDecimal.valueOf(100.000D));
		vo.setPasswordRing(null);
		vo.setWithdrawRing(null);
		vo.setRechargeRing(null);
		vo.setVipRing(null);
		vo.setLevelUpTime(null);
		vo.setStatus(-1);
		vo.setAgentGroupId(0L);
		vo.setAgentLevel(null);
		try{
			String token = createJWT(vo, NORMAL_TOKEN);
			return token;
		}catch(Exception ex) {
			throw new RuntimeException("[createToken] ERR : " + ex.getMessage(), ex);
		}
	}

	// JWT驗證方法
	public Authentication getAuthentication(HttpServletRequest request) {
		String token = getToken(request);

		token = PUBLIC_TOKEN;
//        if(this.isTokenExpired(token)){
//            return null;
//        }
		if (StringUtils.isNotEmpty(token)) {
			// 解析 Token
			try {
				Claims claims = parseJWT(token);

                /*
                Date expiredDate = claims.getExpiration();

                if (expiredDate.before(new Date())) {
                    return null;
                }
                */

				//check redis
				String uid = claims.get("uid").toString();

				//前台
				RedisUtil.RedisKeyGen redisKey_gaming = RedisUtil.keyGen(RedisKey.MEMBER, uid);
				//後台
				RedisUtil.RedisKeyGen redisKey_admin = RedisUtil.keyGen(RedisKey.ADMIN_MEMBER, uid);

				RedisUtil.RedisKeyGen redisKey = redisKey_admin;//這裡看前後台要改 TODO

				// TODO 硬寫入Redis token
				RedisUtil.set(redisKey, token);

				String tokenInRedis = RedisUtil.get(redisKey)
						.orElseThrow(() -> new JwtException(String.valueOf(DataConstants.FORCE_LOG_OUT)))
						.toString();
				if ((StringUtils.isNotEmpty(tokenInRedis) && !tokenInRedis.equalsIgnoreCase(token))
						|| ((tokenInRedis == null && !request.getRequestURI().contains("login")))) {
					throw new JwtException(String.valueOf(DataConstants.ALREADY_OTHER_LOGIN));//TODO 正式要打開
				}

				// 拿用户名
				String user = claims.getSubject();
				UserDetails members = userDetailsService.loadUserByUsername(user);

				// 得到權限
				List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList((String) claims.get("authorize"));
				// 返回Token

				return user != null ? new UsernamePasswordAuthenticationToken(members, null, authorities) : null;
			} catch (JwtException ex) {
				// ex.printStackTrace();
				throw ex;
			}
		}
		return null;
	}

	private void resetAuthenticationAfterRequest() {
		SecurityContextHolder.getContext().setAuthentication(null);
	}

	public static class AddParamsToHeader extends HttpServletRequestWrapper {
		public AddParamsToHeader(HttpServletRequest request) {
			super(request);
		}

		public String getHeader(String name) {
			if(StringUtils.equals(name, HttpHeaders.AUTHORIZATION)) {
				return PUBLIC_TOKEN;
			}
			String header = super.getHeader(name);
			return (header != null) ? header : super.getParameter(name); // Note: you can't use getParameterValues() here.
		}

		public Enumeration getHeaderNames() {
			List<String> names = Collections.list(super.getHeaderNames());
			names.addAll(Collections.list(super.getParameterNames()));
			return Collections.enumeration(names);
		}
	}

//	@Resource
//	private AdminMemberService adminMemberService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		try {
			Authentication authentication = getAuthentication(httpServletRequest);
			//將獲得的認證存入context

			SecurityContextHolder.getContext().setAuthentication(authentication);

			filterChain.doFilter(new AddParamsToHeader(request), response);

			this.resetAuthenticationAfterRequest();
		} catch (ExpiredJwtException e) {
			httpServletResponse.sendError(300, "expired");
//			 AdminResponse errorResponse = new AdminResponse(-1,eje.toString());
//			 servletResponse.getWriter().write(errorResponse.getMessage());
//			 PrintWriter out = servletResponse.getWriter();
//			 out.print(errorResponse);
//			 out.flush();
//			 return;
			((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);

			log.error(e.getLocalizedMessage());
			if(e instanceof JwtException) {
				JwtException eje = (JwtException)e;
				JsonResponse errorResponse = null;
				if (eje instanceof io.jsonwebtoken.SignatureException) {
					errorResponse = new JsonResponse(300, "token 不合法");
				}else if (eje instanceof io.jsonwebtoken.ExpiredJwtException) {
					errorResponse = new JsonResponse(300, "token 已過期");
				}else {
					errorResponse = new JsonResponse(301, Integer.parseInt(eje.getMessage()), eje.getMessage());
				}


				ObjectMapper mapper = new ObjectMapper();
				//			response.setContentType(MediaType.APPLICATION_JSON_VALUE);
				httpServletResponse.setContentType("application/json;charset=utf-8");

//			        response.setHeader("Access-Control-Allow-Origin", "*");
//			        response.setHeader("Access-Control-Allow-Methods", "GET,POST,DELETE,PUT,OPTIONS");
//			        response.setHeader("Access-Control-Allow-Headers", "*");
//			        response.setHeader("Access-Control-Allow-Credentials", "true");
//			        response.setHeader("Access-Control-Max-Age", "180");


				PrintWriter printWriter = httpServletResponse.getWriter();
				printWriter.write(mapper.writeValueAsString(errorResponse));
				printWriter.flush();
				printWriter.close();
			}
		}
	}
}
