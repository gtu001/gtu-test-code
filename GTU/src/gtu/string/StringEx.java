package gtu.string;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

public class StringEx {

    String context;
    List<CharEx> lst = new ArrayList<CharEx>();

    public List<StringEx> findAll(String findStr) {
        List<StringEx> result = new ArrayList<StringEx>();
        char[] arry = findStr.toCharArray();
        for (int ii = 0; ii < lst.size(); ii++) {
            boolean findOk = true;
            String uuidStart = "";
            String uuidEnd = "";
            int endPos = -1;

            B: for (int jj = 0; jj < arry.length; jj++) {
                if (jj + ii <= lst.size() - 1) {
                    endPos = jj + ii;
                    CharEx c1 = lst.get(endPos);
                    char c2 = arry[jj];
                    System.out.println("---->" + ii + " / " + jj + "\t" + c1.c + " / " + c2);
                    if (c1.c != c2) {
                        findOk = false;
                        break B;
                    } else {
                        if (jj == 0) {
                            System.out.println("=====start");
                            uuidStart = c1.uuid;
                        } else {
                            System.out.println("=====end");
                            uuidEnd = c1.uuid;
                        }
                    }
                }
            }

            if (findOk && StringUtils.isNotBlank(uuidStart) && StringUtils.isNotBlank(uuidEnd)) {
                StringEx stringex = this.substring(uuidStart, uuidEnd);
                System.out.println(">>>>>> append === " + stringex);
                result.add(stringex);
                ii = endPos;
            }
        }
        return result;
    }

    public boolean replace(StringEx strEx, String replaceTo) {
        String start = "";
        String end = "";
        if (strEx.lst.size() == 1) {
            start = strEx.lst.get(0).uuid;
            end = strEx.lst.get(0).uuid;
        } else if (strEx.lst.size() >= 2) {
            start = strEx.lst.get(0).uuid;
            end = strEx.lst.get(strEx.lst.size() - 1).uuid;
        }
        if (StringUtils.isBlank(start)) {
            return false;
        }
        int startPos = -1;
        int endPos = -1;
        for (int ii = 0; ii < lst.size(); ii++) {
            CharEx c = lst.get(ii);
            if (StringUtils.equals(start, c.uuid)) {
                startPos = ii;
                break;
            }
        }
        for (int ii = 0; ii < lst.size(); ii++) {
            CharEx c = lst.get(ii);
            if (StringUtils.equals(end, c.uuid)) {
                if (ii + 1 <= lst.size() - 1) {
                    endPos = ii + 1;
                    break;
                }
            }
        }
        List<CharEx> l1 = lst.subList(0, startPos);
        List<CharEx> l2 = lst.subList(endPos, lst.size());
        List<CharEx> result = new ArrayList<CharEx>();
        result.addAll(l1);
        result.addAll(new StringEx(replaceTo).lst);
        result.addAll(l2);
        lst = result;
        return true;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (CharEx c : lst) {
            sb.append(c.c);
        }
        return sb.toString();
    }

    public StringEx() {
    }

    public StringEx(List<CharEx> lst) {
        this.lst = lst;
        StringBuffer sb = new StringBuffer();
        for (CharEx c : lst) {
            sb.append(c.c);
        }
        this.context = sb.toString();
    }

    public StringEx(String content) {
        this.appendContent(content);
    }

    public void appendContent(String content) {
        this.context = StringUtils.defaultString(content);
        char[] arry = context.toCharArray();
        for (char c : arry) {
            CharEx c2 = new CharEx();
            c2.c = c;
            c2.uuid = UUID.randomUUID().toString();
            lst.add(c2);
        }
    }

    public void append(CharEx charEx) {
        lst.add(charEx);
    }

    public CharEx[] toCharArray() {
        return lst.toArray(new CharEx[0]);
    }

    public StringEx substring(String start, String end) {
        int startPos = -1;
        int endPos = -1;
        for (int ii = 0; ii < lst.size(); ii++) {
            CharEx c = lst.get(ii);
            if (StringUtils.equals(start, c.uuid)) {
                startPos = ii;
                break;
            }
        }
        for (int ii = 0; ii < lst.size(); ii++) {
            CharEx c = lst.get(ii);
            if (StringUtils.equals(end, c.uuid)) {
                endPos = ii;
                break;
            }
        }
        List<CharEx> lst2 = lst.subList(startPos, endPos + 1);
        return new StringEx(lst2);
    }

    public StringEx substring(int start, int end) {
        StringEx string = new StringEx();
        for (int ii = start; ii <= end; ii++) {
            CharEx ex = lst.get(ii);
            string.append(ex);
        }
        return string;
    }

    public String getContext() {
        StringBuffer sb = new StringBuffer();
        for (CharEx c : lst) {
            sb.append(c.c);
        }
        return sb.toString();
    }

    public static class CharEx {
        String uuid;
        char c;

        public char c() {
            return c;
        }

        public String getUuid() {
            return uuid;
        }

        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        public char getC() {
            return c;
        }

        public void setC(char c) {
            this.c = c;
        }

        public String uuid() {
            return uuid;
        }
    }
}
