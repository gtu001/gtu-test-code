package gtu.binary;

import org.apache.commons.lang3.StringUtils;

/**
 * 修正中文在記事本上顯示長度
 */
public class StringUtil4FullChar_4NotePad {

    public static String rightPad(String text, int length) {
        return rightPad(text, length, ' ');
    }

    public static String leftPad(String text, int length) {
        return leftPad(text, length, ' ');
    }

    public static String rightPad(String text, int length, char padChar) {
        int length2 = length(text);
        int len = (length - length2);
        StringBuffer sb = new StringBuffer(text);
        for (int ii = 0; ii < len; ii++) {
            sb.append(padChar);
        }
        return sb.toString();
    }

    public static String leftPad(String text, int length, char padChar) {
        int length2 = length(text);
        int len = (length - length2);
        StringBuffer sb = new StringBuffer();
        for (int ii = 0; ii < len; ii++) {
            sb.append(padChar);
        }
        sb.append(text);
        return sb.toString();
    }

    public static int length(String text) {
        int total = 0;
        text = StringUtils.defaultString(text);
        char[] arry = text.toCharArray();
        for (char c : arry) {
            if (("" + c).getBytes().length == 1) {
                total += 1;
            } else {
                total += 2;
            }
        }
        return total;
    }
}