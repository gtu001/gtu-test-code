package gtu.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class FutureTest {

    public static void main(String[] args) {
        FutureTest mFutureTest = new FutureTest();
        mFutureTest.test002_better();
    }

    private void test001() {
        Callable<Integer> func = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                System.out.println("inside callable start");
                Thread.sleep(1000);
                System.out.println("inside callable sleep 1sec ok");
                return new Integer(8);
            }
        };

        FutureTask<Integer> futureTask = new FutureTask<Integer>(func);
        Thread newTread = new Thread(futureTask);
        newTread.start();

        try {
            System.out.println("blocking here");
            Integer result = futureTask.get();
            System.out.println("get result : " + result);
        } catch (InterruptedException ingored) {
        } catch (ExecutionException ingored) {
        }
    }

    public void test002_better() {
        List<Callable<Object>> tasks = new ArrayList<Callable<Object>>();
        tasks.add(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("-------1");
                System.out.println("-------1-end");
                return "ok";
            }
        });
        tasks.add(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("-------2");
                if (true) {
                    // Thread.currentThread().interrupt();
                    throw new RuntimeException("----err");
                }
                System.out.println("-------2-end");
                return "ok";
            }
        });
        tasks.add(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                System.out.println("-------3");
                System.out.println("-------3-end");
                return "ok";
            }
        });

        ExecutorService service = Executors.newFixedThreadPool(5);

        List<Future<Object>> lst = null;
        try {
            lst = service.invokeAll(tasks);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int ii = 0; ii < lst.size(); ii++) {
            Future<Object> fu = lst.get(ii);
            if (fu.isCancelled()) {
                System.out.println(ii + "\t" + "canceled");
                try {
                    System.out.println(ii + "\t : Result[" + fu.get() + "]");
                    // 針對
                    // fu.get()會產生exception
                    // 需要重作
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (fu.isDone()) {
                System.out.println(ii + "\t" + "done");
                try {
                    System.out.println(ii + "\t : Result[" + fu.get() + "]");
                    // 針對
                    // fu.get()會產生exception
                    // 需要重作
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("all--done....");
    }
}
