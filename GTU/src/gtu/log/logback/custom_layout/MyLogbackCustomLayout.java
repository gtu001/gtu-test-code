package gtu.log.logback.custom_layout;

import java.util.Map;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.LayoutBase;

public class MyLogbackCustomLayout extends LayoutBase<ILoggingEvent> {

    public String doLayout(ILoggingEvent event) {
        Map<String, String> mdcMap = event.getMdc();
        String moduleName = ""; // contoller name
        String funtionName = ""; // controller function name

        StackTraceElement[] stacks = event.getCallerData();
        if (stacks.length > 0) {
            moduleName = stacks[0].getClassName();
            funtionName = stacks[0].getMethodName();
        }
        String message = event.getFormattedMessage();
        String resultString = "<" + moduleName + "." + funtionName + "> " + message + "\r\n";
        return resultString;
    }
}