

package gtu.unit_test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tt289.admin.form.AdminLoginForm;
import com.tt289.admin.form.ManualBindPromotionForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("local_dev")
public class UnitTest_BindPromotionCtrlTest {

    private String token;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void init() throws Exception {
        //須先登入 拿token
        AdminLoginForm form = new AdminLoginForm();
        form.setPassword("123456");
        form.setUsername("gary");

        System.out.println("=====================================");
        System.out.println("JSON == " + JSON.toJSONString(form));
        System.out.println("=====================================");

        // Execute the GET request
        ResultActions actions = mockMvc.perform(post("/adminmember/adminlogin")
                .content(JSON.toJSONString(form))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //先取的登入token
        JSONObject json = JSON.parseObject(actions.andReturn().getResponse().getContentAsString());
        JSONObject dataGridJson = json.getJSONObject("dataGrid");
        token = dataGridJson.getString("token");
        System.out.println(" token = " + token);
    }


    @Test
    @DisplayName("GET /bind/promotion/log success")
    void testGetBindPromotionSetting() throws Exception {
        // Execute the GET request
        MvcResult result = mockMvc.perform(get("/bind/promotion/log")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        String content = result.getResponse().getContentAsString();

        System.out.println("==========================================");
        System.out.println("取得json結果 = " + content);
        System.out.println("==========================================");

        mockMvc.perform(get("/bind/promotion/log")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(0)));
    }

    @Test
    @Rollback
    @Transactional
    @DisplayName("PUT /bind/promotion/log fail")
    void testManualSendSuccess() throws Exception {

        ManualBindPromotionForm form = new ManualBindPromotionForm();
        form.setId(1);
        form.setPlatformId(1);
        form.setRemark("test");
        // Execute the POST request
        mockMvc.perform(post("/bind/promotion/log")
                .header("Authorization", token)
                .content(JSON.toJSONString(form))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(-1)));

    }

}
