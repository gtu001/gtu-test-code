

package gtu.unit_test;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tt289.admin.form.AdminLoginForm;
import com.tt289.admin.form.BindPromotionSettingForm;
import com.tt289.data.enums.BindPromotionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("local_dev")
public class UnitTest_BindPromotionSettingCtrlTest {

    private String token;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void init() throws Exception {
        //須先登入 拿token
        AdminLoginForm form = new AdminLoginForm();
        form.setPassword("123456");
        form.setUsername("gary");

        // Execute the GET request
        ResultActions actions = mockMvc.perform(post("/adminmember/adminlogin")
                .content(JSON.toJSONString(form))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        //先取的登入token
        JSONObject json = JSON.parseObject(actions.andReturn().getResponse().getContentAsString());
        JSONObject dataGridJson = json.getJSONObject("dataGrid");
        token = dataGridJson.getString("token");

    }


    @Test
    @DisplayName("GET /bind/promotion/setting success")
    void testGetBindPromotionSetting() throws Exception {
        // Execute the GET request
        mockMvc.perform(get("/bind/promotion/setting")
                .header("Authorization", token))

                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(0)));
    }

    @Test
    @Rollback
    @Transactional
    @DisplayName("PUT /bind/promotion/setting success")
    void testUpdateBindPromotionSettingSuccess() throws Exception {

        BindPromotionSettingForm settingForm = new BindPromotionSettingForm();
        settingForm.setId(1);
        settingForm.setBouns(new BigDecimal("100"));
        settingForm.setBetRate(new BigDecimal("100"));
        settingForm.setStatus(false);
        settingForm.setJoinCondition(BindPromotionType.JoinConditionType.getDefaultJoinConditionList());
        settingForm.setExcludeCondition(BindPromotionType.ExcludeConditionType.getDefaultExcludeConditionList());
        settingForm.setVendors(List.of(1, 2, 3, 4, 5));

        // Execute the POST request
        mockMvc.perform(put("/bind/promotion/setting")
                .header("Authorization", token)
                .content(JSON.toJSONString(settingForm))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(0)));
    }



}
