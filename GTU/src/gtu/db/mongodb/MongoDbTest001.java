package gtu.db.mongodb;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Query;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class MongoDbTest001 {

    public static void main(String args[]) {

        String username = "root";
        String password = "pass12345";
        String databaseName = "admin";// <--這個不能改（說是帳號認證必須經由database=admin）

        // option 1
//        MongoCredential credential = MongoCredential.createCredential(username, databaseName, password.toCharArray());
//        MongoClient mongo = new MongoClient(new ServerAddress("localhost", 27017), Arrays.asList(credential));
//        System.out.println("Credentials ::" + credential);

        // option 2
//        MongoClientURI uri = new MongoClientURI("mongodb://" + username + ":" + password + "@localhost:27017/" + databaseName);
//        MongoClient mongo = new MongoClient(uri);

        String connectionStr = "mongodb://" + username + ":" + password + "@localhost:27017";

        ConnectionString connectionString = new ConnectionString(connectionStr);
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()//
                .applyConnectionString(connectionString).build();

        MongoClient mongo = MongoClients.create(mongoClientSettings);

        // Accessing the database
        MongoDatabase database = mongo.getDatabase("fruit");// test

//        database.createCollection("my_collection_002");

        MongoOperations mongoOps = new MongoTemplate(MongoClients.create(), "fruit");
        mongoOps.insert(new Person("Joe", 34));

        Query query = new Query(where("name").is("Joe"));
        mongoOps.findOne(query, Person.class);
        mongoOps.dropCollection("person");
    }

    public static class Person {

        private String id;
        private String name;
        private int age;

        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        @Override
        public String toString() {
            return "Person [id=" + id + ", name=" + name + ", age=" + age + "]";
        }
    }
}
