package gtu.db.mongodb.springboot_001;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = { "gtu.db.mongodb.springboot_001" })
public class MongoDBSpringBootTest001 implements CommandLineRunner {

    public static void main(String[] args) {
        // 使用指定設定檔
        // --spring.config.location=classpath:/default.properties,classpath:/override.properties
        // --spring.config.location="file:/D:/workstuff/gtu-test-code/GTU/src/gtu/spring/boot/admin_line/application.yml"
        // -Dspring.config.location="file:/D:/workstuff/gtu-test-code/GTU/src/gtu/spring/boot/admin_line/application.yml"

        System.setProperty("spring.profiles.active", "dev");
        System.setProperty("spring.config.location", "classpath:/gtu/db/mongodb/springboot_001/application.yml");

        SpringApplication.run(MongoDBSpringBootTest001.class, args);
    }

    @Autowired
    private CustomerRepository repository;

    @Override
    public void run(String... args) throws Exception {
        repository.deleteAll();

        // save a couple of customers
        repository.save(new Customer("Alice", "Smith"));
        repository.save(new Customer("Bob", "Smith"));

        // fetch all customers
        System.out.println("Customers found with findAll():");
        System.out.println("-------------------------------");
        for (Customer customer : repository.findAll()) {
            System.out.println(customer);
        }
        System.out.println();

        // fetch an individual customer
        System.out.println("Customer found with findByFirstName('Alice'):");
        System.out.println("--------------------------------");
        System.out.println(repository.findByFirstName("Alice"));

        System.out.println("Customers found with findByLastName('Smith'):");
        System.out.println("--------------------------------");
        for (Customer customer : repository.findByLastName("Smith")) {
            System.out.println(customer);
        }
    }
}
