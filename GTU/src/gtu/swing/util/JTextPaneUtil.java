package gtu.swing.util;

import java.awt.EventQueue;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.StyledDocument;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

public class JTextPaneUtil {
    private JTextComponent textArea;

    public static JTextPaneUtil newInstance(JTextComponent textArea) {
        return new JTextPaneUtil(textArea);
    }

    public JTextPaneUtil(JTextComponent textArea) {
        this.textArea = textArea;
    }

    public void removeStyles() {
        MutableAttributeSet mas = ((JTextPane) textArea).getInputAttributes();
        System.out.println("before: " + mas);
        mas.removeAttributes(mas);
        System.out.println("after: " + mas);
    }

    public AtomicBoolean insertStart(final String text) {
        final AtomicBoolean success = new AtomicBoolean(false);
        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    Document doc = textArea.getDocument();
                    doc.insertString(0, text, null);
                    success.set(true);
                } catch (Exception e) {
                    success.set(true);
                    e.printStackTrace();
                }
            }
        };
        EventQueue.invokeLater(runnable);
        return success;
    }

    public AtomicBoolean append(final String text, final AttributeSet[] attrSet, final int[] attrSetType) {
        final AtomicBoolean success = new AtomicBoolean(false);
        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    StyledDocument doc = ((JTextPane) textArea).getStyledDocument();
                    int offset = textArea.getDocument().getLength();
                    doc.insertString(offset, text, null);
                    for (int ii = 0; ii < attrSet.length; ii++) {
                        AttributeSet attr = attrSet[ii];
                        int style = attrSetType[ii];
                        switch (style) {
                        case 0:
                            doc.setParagraphAttributes(offset, StringUtils.length(text), attr, false);
                            break;
                        case 1:
                            doc.setCharacterAttributes(offset, StringUtils.length(text), attr, false);
                            break;
                        }
                    }
                } catch (Exception e) {
                    success.set(true);
                    e.printStackTrace();
                }
            }
        };
        EventQueue.invokeLater(runnable);
        return success;
    }

    public AtomicBoolean append(final String text) {
        final AtomicBoolean success = new AtomicBoolean(false);
        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    Document doc = textArea.getDocument();
                    doc.insertString(doc.getLength(), text, null);
                    success.set(true);
                } catch (Exception e) {
                    success.set(true);
                    e.printStackTrace();
                }
            }
        };
        EventQueue.invokeLater(runnable);
        return success;
    }

    public AtomicBoolean remove(final int start, final int length) {
        final AtomicBoolean success = new AtomicBoolean(false);
        Runnable runnable = new Runnable() {
            public void run() {
                try {
                    Document doc = textArea.getDocument();
                    doc.remove(start, length);
                    success.set(true);
                } catch (Exception e) {
                    success.set(true);
                    e.printStackTrace();
                }
            }
        };
        SwingUtilities.invokeLater(runnable);
        return success;
    }

    public AtomicBoolean appendAndCheck(final String text, final long bufferedSize) {
        final AtomicBoolean success = new AtomicBoolean(false);
        Runnable runnable = new Runnable() {
            public void run() {
                LineNumberReader reader = null;
                try {
                    Document doc = textArea.getDocument();
                    while (textArea.getDocument().getLength() > bufferedSize) {
                        String content = textArea.getText();
                        reader = new LineNumberReader(new StringReader(content));
                        A: for (String line = null; (line = reader.readLine()) != null;) {
                            doc.remove(0, StringUtils.length(line) + 1);
                            break A;
                        }
                        reader.close();
                    }
                    doc.insertString(doc.getLength(), text, null);
                    success.set(true);
                    // System.out.println("buffersize = " +
                    // textArea.getDocument().getLength());
                    textArea.repaint();
                } catch (Exception e) {
                    success.set(true);
                    e.printStackTrace();
                } finally {
                    try {
                        if (reader != null)
                            reader.close();
                    } catch (IOException e) {
                    }
                }
            }
        };
        EventQueue.invokeLater(runnable);
        return success;
    }

    public AtomicBoolean removeLineNumber(final int linePosStart0) {
        final AtomicBoolean success = new AtomicBoolean(false);
        Runnable runnable = new Runnable() {
            public void run() {
                LineNumberReader reader = null;
                try {
                    Pair<Integer, Integer> linePos = null;
                    String content = textArea.getText();
                    reader = new LineNumberReader(new StringReader(content));
                    int startPos = 0;
                    for (String line = null; (line = reader.readLine()) != null;) {
                        int endPos = StringUtils.length(line) + 1;
                        linePos = Pair.of(startPos, endPos);
                        startPos += endPos;
                        if (linePosStart0 == reader.getLineNumber() - 1) {
                            break;
                        }
                    }
                    if (linePos != null) {
                        Document doc = textArea.getDocument();
                        doc.remove(linePos.getLeft(), linePos.getRight());
                        success.set(true);
                    }
                } catch (Exception e) {
                    success.set(true);
                    e.printStackTrace();
                } finally {
                    try {
                        if (reader != null)
                            reader.close();
                    } catch (IOException e) {
                    }
                }
            }
        };
        SwingUtilities.invokeLater(runnable);
        return success;
    }

    public AtomicBoolean clear() {
        return remove(0, StringUtils.length(textArea.getText()));
    }
}
