package gtu.swing.util;

import java.awt.event.MouseEvent;

import javax.swing.JTabbedPane;

public class JTabbedPaneUtil {

    private JTabbedPane tabbedPane;

    private JTabbedPaneUtil(JTabbedPane tabbedPane) {
        this.tabbedPane = tabbedPane;
    }

    public static JTabbedPaneUtil newInst(JTabbedPane tabbedPane) {
        return new JTabbedPaneUtil(tabbedPane);
    }

    public static void applyCommonSetting(JTabbedPane tabbedPane) {
        tabbedPane = new JTabbedPane(JTabbedPane.TOP);// DraggableTabbedPane
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    }

    public boolean isSelectedTitle(String title, boolean ignoreCase) {
        return getTitleIndex(title, ignoreCase) == tabbedPane.getSelectedIndex();
    }

    public int getTitleIndex(String title, boolean ignoreCase) {
        title = title != null ? title.trim() : "";
        for (int ii = 0; ii < tabbedPane.getTabCount(); ii++) {
            if (ignoreCase && title.equalsIgnoreCase(tabbedPane.getTitleAt(ii))) {
                return ii;
            } else if (!ignoreCase && title.equals(tabbedPane.getTitleAt(ii))) {
                return ii;
            }
        }
        return -1;
    }

    public void setSelectedIndexByTitle(String title) {
        int index = this.getTitleIndex(title, true);
        if (index != -1) {
            if (tabbedPane.getSelectedIndex() != index) {
                tabbedPane.setSelectedIndex(index);
            }
        } else {
            throw new RuntimeException("找不到title:" + title);
        }
    }

    public int getSelectTabIndex() {
        return tabbedPane.getSelectedIndex();
    }

    public void setSelectTabIndex(int idx) {
        tabbedPane.setSelectedIndex(idx);
    }

    public int getTabCount() {
        return tabbedPane.getTabCount();
    }

    public String getTabTitle(Integer idx) {
        if (idx == null) {
            idx = tabbedPane.getSelectedIndex();
        }
        return tabbedPane.getTitleAt(idx);
    }

    public void setTabTitle(Integer idx, String title) {
        if (idx == null) {
            idx = tabbedPane.getSelectedIndex();
        }
        tabbedPane.setTitleAt(idx, title);
    }

    public int getClickTabIndex(MouseEvent e) {
        return tabbedPane.indexAtLocation(e.getX(), e.getY());
    }
}
