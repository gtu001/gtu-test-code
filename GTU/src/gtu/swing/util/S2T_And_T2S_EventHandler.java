package gtu.swing.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JMenuItem;
import javax.swing.text.JTextComponent;

import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;

import gtu.binary.Base64JdkUtil;
import gtu.date.DateUtil;
import taobe.tec.jcc.JChineseConvertor;

public class S2T_And_T2S_EventHandler {

    JTextComponent input;

    public S2T_And_T2S_EventHandler(JTextComponent input) {
        this.input = input;
    }

    final Transformer transfor_STChinese = new Transformer() {
        public Object transform(final Object _input) {
            String text = input.getText();
            try {
                boolean s2t = (Boolean) _input;
                if (StringUtils.isNotBlank(input.getSelectedText())) {
                    String before = StringUtils.substring(text, 0, input.getSelectionStart());
                    String middle = input.getSelectedText();
                    if (s2t) {
                        middle = JChineseConvertor.getInstance().s2t(middle);
                    } else {
                        middle = JChineseConvertor.getInstance().t2s(middle);
                    }
                    String after = StringUtils.substring(text, input.getSelectionEnd());
                    return before + middle + after;
                } else {
                    if (s2t) {
                        text = JChineseConvertor.getInstance().s2t(text);
                    } else {
                        text = JChineseConvertor.getInstance().t2s(text);
                    }
                    return text;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return text;
        }
    };

    final Transformer transfer_EncodeDecodeBase64 = new Transformer() {
        public Object transform(final Object _input) {
            String text = input.getText();
            try {
                boolean isEncode = (Boolean) _input;
                if (StringUtils.isNotBlank(input.getSelectedText())) {
                    String before = StringUtils.substring(text, 0, input.getSelectionStart());
                    String middle = input.getSelectedText();
                    if (isEncode) {
                        middle = Base64JdkUtil.encode(middle);
                    } else {
                        middle = Base64JdkUtil.decodeToString(middle);
                    }
                    String after = StringUtils.substring(text, input.getSelectionEnd());
                    return before + middle + after;
                } else {
                    if (isEncode) {
                        text = Base64JdkUtil.encode(text);
                    } else {
                        text = Base64JdkUtil.decodeToString(text);
                    }
                    return text;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return text;
        }
    };

    final Transformer transfer_UpperLowerCase = new Transformer() {
        public Object transform(final Object _input) {
            String text = input.getText();
            boolean isUpperCase = (Boolean) _input;
            try {
                if (isUpperCase) {
                    if (StringUtils.isNotBlank(input.getSelectedText())) {
                        String before = StringUtils.substring(text, 0, input.getSelectionStart());
                        String middle = input.getSelectedText();
                        middle = middle.toUpperCase();
                        String after = StringUtils.substring(text, input.getSelectionEnd());
                        return before + middle + after;
                    } else {
                        return text.toUpperCase();
                    }
                } else {
                    if (StringUtils.isNotBlank(input.getSelectedText())) {
                        String before = StringUtils.substring(text, 0, input.getSelectionStart());
                        String middle = input.getSelectedText();
                        middle = middle.toLowerCase();
                        String after = StringUtils.substring(text, input.getSelectionEnd());
                        return before + middle + after;
                    } else {
                        return text.toLowerCase();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return text;
        }
    };

    final Transformer transfer_USADateString = new Transformer() {
        private String process(String text, boolean isUsingBeforeText) {
            text = StringUtils.defaultString(text);
            if (isUsingBeforeText) {
                return DateUtil.filterUSADateString(text);
            } else {
                return DateUtil.filterUSADateString_Reverse(text);
            }
        }

        public Object transform(final Object _input) {
            String text = input.getText();
            boolean isUpperCase = (Boolean) _input;
            try {
                if (isUpperCase) {
                    if (StringUtils.isNotBlank(input.getSelectedText())) {
                        String before = StringUtils.substring(text, 0, input.getSelectionStart());
                        String middle = input.getSelectedText();
                        // ====================process start
                        middle = process(middle, isUpperCase);
                        // ====================process end
                        String after = StringUtils.substring(text, input.getSelectionEnd());
                        return before + middle + after;
                    } else {
                        return text.toUpperCase();
                    }
                } else {
                    if (StringUtils.isNotBlank(input.getSelectedText())) {
                        String before = StringUtils.substring(text, 0, input.getSelectionStart());
                        String middle = input.getSelectedText();
                        // ====================process start
                        middle = process(middle, isUpperCase);
                        // ====================process end
                        String after = StringUtils.substring(text, input.getSelectionEnd());
                        return before + middle + after;
                    } else {
                        return text.toLowerCase();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return text;
        }
    };

    public JMenuItem getMenuItem_USADateString(final boolean turnStandard) {
        JMenuItem item = new JMenuItem(turnStandard ? "美式轉標準日期" : "標準轉美式日期");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input.setText((String) transfer_USADateString.transform(turnStandard));
            }
        });
        return item;
    }

    public JMenuItem getMenuItem_UpperLowerCase(final boolean isUpperCase) {
        JMenuItem item = new JMenuItem(isUpperCase ? "轉大寫" : "轉小寫");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input.setText((String) transfer_UpperLowerCase.transform(isUpperCase));
            }
        });
        return item;
    }

    public JMenuItem getMenuItem_STChinese(final boolean isS2t) {
        JMenuItem item = new JMenuItem(isS2t ? "簡轉繁" : "繁轉簡");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input.setText((String) transfor_STChinese.transform(isS2t));
            }
        });
        return item;
    }

    public JMenuItem getMenuItem_EncodeDecode(final boolean isEncode) {
        JMenuItem item = new JMenuItem(isEncode ? "Encode" : "Decode");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input.setText((String) transfer_EncodeDecodeBase64.transform(isEncode));
            }
        });
        return item;
    }

    public MouseAdapter getEvent() {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    final JTextComponent input = (JTextComponent) e.getSource();
                    if (JMouseEventUtil.buttonRightClick(1, e)) {

                        JPopupMenuUtil.newInstance(input)//
                                .addJMenuItem("繁轉簡", new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        input.setText((String) transfor_STChinese.transform(false));
                                    }
                                })//
                                .addJMenuItem("簡轉繁", new ActionListener() {

                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        input.setText((String) transfor_STChinese.transform(true));
                                    }
                                })//
                                .addJMenuItem("轉大寫", new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        input.setText((String) transfer_UpperLowerCase.transform(true));
                                    }
                                })//
                                .addJMenuItem("轉小寫", new ActionListener() {

                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        input.setText((String) transfer_UpperLowerCase.transform(false));
                                    }
                                })//
                                .addJMenuItem("Encode", new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        input.setText((String) transfer_EncodeDecodeBase64.transform(true));
                                    }
                                })//
                                .addJMenuItem("Decode", new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        input.setText((String) transfer_EncodeDecodeBase64.transform(false));
                                    }
                                })

                                .addJMenuItem("美式轉標準日期", new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        input.setText((String) transfer_EncodeDecodeBase64.transform(true));
                                    }
                                })//
                                .addJMenuItem("標準轉美式日期", new ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        input.setText((String) transfer_EncodeDecodeBase64.transform(false));
                                    }
                                })

                                //
                                .applyEvent(e).show();
                    }
                } catch (Exception ex1) {
                    JCommonUtil.handleException(ex1);
                }
            }
        };
    }
}