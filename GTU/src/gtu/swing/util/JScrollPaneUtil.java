package gtu.swing.util;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

public class JScrollPaneUtil {

    public static Dimension getVisibleSize(JScrollPane scrollPane) {
        int height = scrollPane.getViewport().getSize().height;
        int width = scrollPane.getViewport().getSize().width;
        return new Dimension(width, height);
    }

    public static final int NONE = 0, TOP = 1, VCENTER = 2, BOTTOM = 4, LEFT = 8, HCENTER = 16, RIGHT = 32;
    private static final int OFFSET = 100; // Required for hack (see below).

    /**
     * Scroll to specified location. e.g. <tt>scroll(component, BOTTOM);</tt>.
     *
     * @param c
     *            JComponent to scroll.
     * @param part
     *            Location to scroll to. Should be a bit-wise OR of one or moe
     *            of the values: NONE, TOP, VCENTER, BOTTOM, LEFT, HCENTER,
     *            RIGHT.
     */
    public static void scroll(JComponent c, int part) {
        scroll(c, part & (LEFT | HCENTER | RIGHT), part & (TOP | VCENTER | BOTTOM));
    }

    /**
     * Scroll to specified location. e.g.
     * <tt>scroll(component, LEFT, BOTTOM);</tt>.
     *
     * @param c
     *            JComponent to scroll.
     * @param horizontal
     *            Horizontal location. Should take the value: LEFT, HCENTER or
     *            RIGHT.
     * @param vertical
     *            Vertical location. Should take the value: TOP, VCENTER or
     *            BOTTOM.
     */
    public static void scroll(JComponent c, int horizontal, int vertical) {
        Rectangle visible = c.getVisibleRect();
        Rectangle bounds = c.getBounds();

        switch (vertical) {
        case TOP:
            visible.y = 0;
            break;
        case VCENTER:
            visible.y = (bounds.height - visible.height) / 2;
            break;
        case BOTTOM:
            visible.y = bounds.height - visible.height + OFFSET;
            break;
        }

        switch (horizontal) {
        case LEFT:
            visible.x = 0;
            break;
        case HCENTER:
            visible.x = (bounds.width - visible.width) / 2;
            break;
        case RIGHT:
            visible.x = bounds.width - visible.width + OFFSET;
            break;
        }

        // When scrolling to bottom or right of viewport, add an OFFSET value.
        // This is because without this certain components (e.g. JTable) would
        // not scroll right to the bottom (presumably the bounds calculation
        // doesn't take the table header into account. It doesn't matter if
        // OFFSET is a huge value (e.g. 10000) - the scrollRectToVisible method
        // still works correctly.

        c.scrollRectToVisible(visible);
    }

    public static void scrollV(JScrollPane scroll, int vertical) {
        switch (vertical) {
        case SwingConstants.TOP:
            scroll.getVerticalScrollBar().setValue(0);
            break;
        case SwingConstants.CENTER:
            scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
            scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getValue() / 2);
            break;
        case SwingConstants.BOTTOM:
            scroll.getVerticalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
            break;
        }
    }

    public static void scrollH(JScrollPane scroll, int vertical) {
        switch (vertical) {
        case SwingConstants.LEFT:
            scroll.getHorizontalScrollBar().setValue(0);
            break;
        case SwingConstants.CENTER:
            scroll.getHorizontalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
            scroll.getHorizontalScrollBar().setValue(scroll.getVerticalScrollBar().getValue() / 2);
            break;
        case SwingConstants.RIGHT:
            scroll.getHorizontalScrollBar().setValue(scroll.getVerticalScrollBar().getMaximum());
            break;
        }
    }
}
