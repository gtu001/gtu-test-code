package gtu.swing.util;

import java.io.File;

import org.apache.commons.lang.StringUtils;

import gtu.file.OsInfoUtil;
import gtu.properties.PropertiesUtil;
import gtu.properties.PropertiesUtilBean;

public class JSwingCommonConfigUtil {

    public static PropertiesUtilBean checkTestingPropertiesUtilBean(PropertiesUtilBean config, Class<?> mainFrameClz, String cfgSimpleClassName) {
        File dir = null;
        if (!PropertiesUtil.isClassInJar(mainFrameClz)) {
            if (OsInfoUtil.isWindows()) {
                dir = new File("D:/my_tool");
            } else if(OsInfoUtil.isLinux()) {
                dir = new File("/media/gtu001/OLD_D/my_tool/");
            }
        }
        if (dir == null || !dir.exists()) {
            dir = PropertiesUtil.getJarCurrentPath(mainFrameClz);
        }
        if (dir.exists()) {
            if (StringUtils.isBlank(cfgSimpleClassName)) {
                cfgSimpleClassName = mainFrameClz.getSimpleName();
            }
            return new PropertiesUtilBean(dir, cfgSimpleClassName);
        }
        return config;
    }

    public static File getJarCurrentPath(Class clz) {
        String folder = clz.getSimpleName();
        File file2 = null;
        if (OsInfoUtil.isWindows()) {
            file2 = new File("D:/my_tool", folder);
        } else if(OsInfoUtil.isLinux()){
            file2 = new File("/media/gtu001/OLD_D/my_tool/", folder);
        }
        if (file2 != null && file2.exists()) {
            return file2;
        }
        if (OsInfoUtil.isWindows()) {
            file2 = new File("D:/my_tool");
        } else if(OsInfoUtil.isLinux()){
            file2 = new File("/media/gtu001/OLD_D/my_tool/");
        }
        if (file2 != null && file2.exists()) {
            return file2;
        }
        return PropertiesUtil.getJarCurrentPath(clz);
    }

    public static File filterCustomFile(File configFile) {
        String filename = configFile.getName();
        String path = configFile.getAbsolutePath();
        File file2 = null;
        if (OsInfoUtil.isWindows()) {
            if (!path.startsWith("D:\\my_tool")) {
                file2 = new File("D:/my_tool", filename);
            }
        } else if(OsInfoUtil.isLinux()){
            if (!path.startsWith("/media/gtu001/OLD_D/my_tool/")) {
                file2 = new File("/media/gtu001/OLD_D/my_tool/", filename);
            }
        }
        if (file2 != null && file2.exists()) {
            return file2;
        }
        return configFile;
    }

    public static PropertiesUtilBean checkTestingPropertiesUtilBean_diffConfig(PropertiesUtilBean config, Class<?> mainFrameClz, String middleDir) {
        String simpleName = mainFrameClz.getSimpleName();
        if (OsInfoUtil.isWindows()) {
            simpleName = simpleName + "_win10";
        } else if(OsInfoUtil.isLinux()){
            simpleName = simpleName + "_linux";
        } else if(OsInfoUtil.isMac()) {
        	simpleName = simpleName + "_linux";
        }
        middleDir = StringUtils.trimToEmpty(middleDir);
        return checkTestingPropertiesUtilBean(config, mainFrameClz, middleDir + simpleName);
    }
}
