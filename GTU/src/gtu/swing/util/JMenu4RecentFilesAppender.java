package gtu.swing.util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;

import gtu._work.ui.JMenuBarUtil;
import gtu._work.ui.JMenuBarUtil.JMenuAppender;
import gtu.properties.PropertiesUtil;
import gtu.swing.JFrameTest;

public class JMenu4RecentFilesAppender {

    File configFile;
    Properties prop;
    ActionListener choiceFileListener;
    JMenu menu;

    SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    private JMenu4RecentFilesAppender(File configFile) {
        init(configFile);
    }

    public void init(File configFile) {
        this.configFile = configFile;
        prop = new Properties();
        if (!this.configFile.exists()) {
            try {
                this.configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        PropertiesUtil.loadProperties(configFile, prop, true);
    }

    public void appendFileToRecent(File targetFile) {
        this.prop.setProperty(SDF.format(new Date()), targetFile.getAbsolutePath());
        PropertiesUtil.storeProperties(prop, configFile, SDF.format(new Date()));
        this.addOpenFileListener(choiceFileListener);
    }

    public JMenu getMenu() {
        return menu;
    }

    public static JMenu4RecentFilesAppender newInstance(File configFile) {
        return new JMenu4RecentFilesAppender(configFile);
    }

    public void addOpenFileListener(final ActionListener choiceFileListener) {
        this.choiceFileListener = choiceFileListener;
        List<String> keys = new ArrayList<String>();
        for (Enumeration enu = prop.keys(); enu.hasMoreElements();) {
            String key = (String) enu.nextElement();
            keys.add(key);
        }

        Collections.sort(keys);
        Collections.reverse(keys);

        for (int ii = 30; ii <= keys.size(); ii++) {
            prop.remove(keys.get(ii));
        }

        JMenuAppender appender = JMenuAppender.newInstance("最近開啟");
        if (this.menu != null) {
            this.menu.removeAll();
            appender.setMenu(menu);
        }

        Set<String> checkExists = new HashSet<String>();

        for (String key : keys) {
            if (!prop.containsKey(key)) {
                continue;
            }
            String value = prop.getProperty(key);
            final File targetFile = new File(value);
            if (!checkExists.contains(value)) {
                checkExists.add(value);
                appender.addMenuItem(targetFile.getName(), new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (choiceFileListener != null) {
                            choiceFileListener.actionPerformed(new ActionEvent(targetFile, -1, targetFile.getAbsolutePath()));
                        }
                    }
                });
            }
        }
        this.menu = appender.getMenu();
    }

    public static void main(String[] args) {
        JFrame frame = JFrameTest.simpleTestComponent(new JLabel());
        File file = new File(PropertiesUtil.getJarCurrentPath(JMenu4RecentFilesAppender.class), JMenu4RecentFilesAppender.class.getSimpleName() + "_recent.properties");
        final JMenu4RecentFilesAppender appender = JMenu4RecentFilesAppender.newInstance(file);
        appender.addOpenFileListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File file = (File) e.getSource();
                appender.appendFileToRecent(file);
            }
        });
        JMenu mainMenu = JMenuAppender.newInstance("file")//
                .addJMenuItem("open file or directory", new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        File file = JCommonUtil._jFileChooser_selectFileAndDirectory();
                        if (file == null || !file.exists()) {
                            JCommonUtil._jOptionPane_showMessageDialog_error("請選擇正確檔案");
                            return;
                        }
                        appender.appendFileToRecent(file);
                    }
                })
                // .addMenuItem("item1", null)//
                .addChildrenMenu(appender.getMenu())//
                .getMenu();
        JMenuBarUtil.newInstance().addMenu(mainMenu).apply(frame);
    }

}
