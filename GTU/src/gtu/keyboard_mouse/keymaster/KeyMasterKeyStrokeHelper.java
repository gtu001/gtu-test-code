package gtu.keyboard_mouse.keymaster;

import javax.swing.KeyStroke;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;

public class KeyMasterKeyStrokeHelper {
    public static KeyMasterKeyStrokeHelper newInstance() {
        return new KeyMasterKeyStrokeHelper();
    }

    /**
     * 不需要加VK_
     * 
     * @param latestKey
     * @return
     */
    public KeyStroke getKeyStroke(String latestKey) {
        Validate.notBlank(eventType, "eventType不可為空");
        Validate.notBlank(latestKey, "latestKey不可為空");
        latestKey = StringUtils.trimToEmpty(latestKey).toUpperCase();
        String command = String.format(" %s %s %s ", maskType, eventType, latestKey);
        KeyStroke keyStroke = KeyStroke.getKeyStroke(command);
        System.out.println("[getKeyStroke] : " + keyStroke);
        return keyStroke;
    }

    String eventType = "";
    String maskType = "";

    public KeyMasterKeyStrokeHelper eventReleased() {
        eventType = "released";
        return this;
    }

    public KeyMasterKeyStrokeHelper eventPressed() {
        eventType = "pressed";
        return this;
    }

    public KeyMasterKeyStrokeHelper eventTyped() {
        eventType = "typed";
        return this;
    }

//        {button2=2048, button3=4096, ctrl=130, meta=260, shift=65, altGraph=8224, alt=520, control=130, button1=1024}
    public KeyMasterKeyStrokeHelper maskButton2() {
        maskType = "button2";
        return this;
    }

    public KeyMasterKeyStrokeHelper maskButton1() {
        maskType = "button1";
        return this;
    }

    public KeyMasterKeyStrokeHelper maskButton3() {
        maskType = "button3";
        return this;
    }

    public KeyMasterKeyStrokeHelper maskCtrl() {
        maskType = "ctrl";
        return this;
    }

    public KeyMasterKeyStrokeHelper maskMeta() {
        maskType = "meta";
        return this;
    }

    public KeyMasterKeyStrokeHelper maskShift() {
        maskType = "shift";
        return this;
    }

    public KeyMasterKeyStrokeHelper maskAltGraph() {
        maskType = "altGraph";
        return this;
    }

    public KeyMasterKeyStrokeHelper maskAlt() {
        maskType = "alt";
        return this;
    }

    public KeyMasterKeyStrokeHelper maskControl() {
        maskType = "control";
        return this;
    }
}