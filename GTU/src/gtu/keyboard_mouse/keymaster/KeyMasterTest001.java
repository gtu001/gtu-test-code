package gtu.keyboard_mouse.keymaster;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import com.tulskiy.keymaster.common.HotKey;
import com.tulskiy.keymaster.common.HotKeyListener;
import com.tulskiy.keymaster.common.Provider;

import gtu.swing.JFrameTest;

public class KeyMasterTest001 {

    public static void main(String[] args) throws IOException, ParseException {
        Provider provider = Provider.getCurrentProvider(true);

        JTextField jText = new JTextField();
        jText.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                char c = e.getKeyChar();
                jText.setText("" + c);

//                KeyStroke keyStroke = KeyMasterKeyStrokeHelper.newInstance().eventPressed().getKeyStroke("" + c);
                KeyStroke keyStroke = KeyStroke.getKeyStrokeForEvent(e);

                System.out.println("Capture KeyStroke : " + keyStroke);

                provider.register(keyStroke, new HotKeyListener() {

                    @Override
                    public void onHotKey(HotKey arg0) {
                        System.out.println("------ click : " + arg0.keyStroke);
                    }
                });
            }
        });
        JFrame frame = JFrameTest.simpleTestComponent(jText);
        System.out.println("done...");
    }
}
