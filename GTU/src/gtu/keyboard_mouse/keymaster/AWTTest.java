package gtu.keyboard_mouse.keymaster;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import com.tulskiy.keymaster.common.HotKeyListener;
import com.tulskiy.keymaster.common.MediaKey;
import com.tulskiy.keymaster.common.Provider;

/**
 * Author: Denis Tulskiy
 * Date: 6/6/11
 */
public class AWTTest {
    public static final List<Integer> MODIFIERS = Arrays.asList(KeyEvent.VK_ALT, KeyEvent.VK_CONTROL, KeyEvent.VK_SHIFT, KeyEvent.VK_META);

    public static void main(String[] args) {
        final JFrame frame = new JFrame();
        final Provider provider = Provider.getCurrentProvider(true);

        if (provider == null) {
            System.exit(1);
        }

        frame.add(new JLabel("Press hotkey combination:"), BorderLayout.NORTH);

        final JTextField textField = new JTextField();
        textField.setFont(textField.getFont().deriveFont(Font.BOLD, 15f));
        textField.setEditable(false);
        textField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (MODIFIERS.contains(e.getKeyCode())) {
                    textField.setText("");
                } else {
                    textField.setText(KeyStroke.getKeyStrokeForEvent(e).toString().replaceAll("pressed ", ""));
                }
            }
        });
        frame.add(textField, BorderLayout.CENTER);
        JPanel box = new JPanel();
        JButton grab = new JButton("Grab");
        JButton ungrab = new JButton("Ungrab");
        JButton reset = new JButton("Reset All");
        box.add(grab);
        box.add(ungrab);

        final HotKeyListener listener =
                hotKey -> JOptionPane.showMessageDialog(frame, "Hooray: " + hotKey);
        grab.addActionListener(e -> {
            String text = textField.getText();
            System.out.println(">>>>" + text);
            if (text != null && text.length() > 0) {
                provider.register(KeyStroke.getKeyStroke(text), listener);
            }
        });

        ungrab.addActionListener(e -> {
            String text = textField.getText();
            if (text != null && text.length() > 0) {
                // error unknow why ???? TODO
//                provider.unregister(KeyStroke.getKeyStroke(text));
            }
        });

        reset.addActionListener(e -> provider.reset());

        JButton grabMedia = new JButton("Grab media keys");
        grabMedia.addActionListener(e -> {
            provider.register(MediaKey.MEDIA_NEXT_TRACK, listener);
            provider.register(MediaKey.MEDIA_PLAY_PAUSE, listener);
            provider.register(MediaKey.MEDIA_PREV_TRACK, listener);
            provider.register(MediaKey.MEDIA_STOP, listener);
        });

        box.add(grabMedia);

        box.add(reset);

        frame.add(box, BorderLayout.PAGE_END);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                provider.reset();
                provider.stop();
                System.exit(0);
            }
        });

        frame.setSize(500, 150);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}