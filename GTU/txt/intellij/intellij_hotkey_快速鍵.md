intellij_hotkey_快速鍵.md
---

	option + command + < >  		前一動後一動
	command + \  				查restful method (RestfulToolkit-fix需安裝)
	F4 							查看方法內容
	
	option + F8 				debug看值
		F2	 					debug設值(須先開上面的)

	F8 							debug下一行 與 返回
	F7 							debug進去

	command + shift + F 		全局找
	command + shift + R 		全局替換
	command + R 				本文搜尋

	command + G 				找下一個
	command + shift + G 		找上一個
	command + control + G 		一樣的選起來
	
	command + option + L 		format code

	command + o 				以檔名找
	double shift 				以檔名找

	command + F12 				顯示method

	