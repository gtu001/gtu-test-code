springboot_build_execute_jar_打包可執行jar.md


	(1)
		<packaging>jar</packaging><!-- war -->

	(2)
	<build>
		<finalName>FoodMenu</finalName>
		<sourceDirectory>src</sourceDirectory>

		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<!-- 設定repackage goal -->
				<executions>
					<execution>
						<goals>
							<goal>repackage</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>

		<resources>
			<resource>
				<directory>src/main/resources</directory>
			</resource>
			<resource>
				<directory>src/main/java</directory>
				<includes>
					<include>idv/matt/mapper/*.xml</include>
				</includes>
			</resource>
		</resources>
	</build>


	(3) 在target找到jar
		java  -jar  -Dfile.encoding=utf-8  FoodMenu.jar