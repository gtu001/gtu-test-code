sqlite_create_table.md




    
    create table IF NOT EXISTS  test_table1 (
        pk1  char(30)   PRIMARY KEY    not null,
        name text  not null,
        num  INT,
        SALARY    REAL, 
        create_date  datetime default current_timestamp,
        update_date  datetime 
    )
    
    
    



	CREATE TABLE IF NOT EXISTS "albums"
	(
	    [AlbumId] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	    [Title] NVARCHAR(160)  NOT NULL,
	    [ArtistId] INTEGER  NOT NULL,
	    FOREIGN KEY ([ArtistId]) REFERENCES "artists" ([ArtistId])
	                ON DELETE NO ACTION ON UPDATE NO ACTION
	);
	CREATE INDEX [IFK_AlbumArtistId] ON "albums" ([ArtistId]);