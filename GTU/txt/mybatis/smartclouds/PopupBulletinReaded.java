package com.tt289.data.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("lottery_popup_bulletin_readed")
public class PopupBulletinReaded implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    @TableField("bulletin_id")
    private java.math.BigDecimal bulletinId;
    @TableField("adminId")
    private java.math.BigDecimal adminId;
    @TableField("insertTime")
    @JsonFormat(timezone = "GMT", pattern = "yyyy-MM-dd HH:mm:ss")
    private java.sql.Timestamp insertTime;
}
