package com.tt289.admin.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tt289.admin.form.PopupBulletinInsertForm;
import com.tt289.admin.form.PopupBulletinQueryForm;
import com.tt289.admin.util.GameUtil;
import com.tt289.common.exception.BusinessException;
import com.tt289.common.login.LoginAdminUserDetail;
import com.tt289.data.config.CustomMessageSource;
import com.tt289.data.entity.AdminPermissionNew;
import com.tt289.data.entity.PopupBulletin;
import com.tt289.data.mapper.FeedbackMapper;
import com.tt289.data.mapper.MailContentMapper;
import com.tt289.data.mapper.MailUserMapper;
import com.tt289.data.mapper.PopupBulletinMapper;
import com.tt289.data.util.DataConstants;
import com.tt289.data.util.HttpReqRespUtils;
import com.tt289.data.vo.PopupBulletinQueryVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class PopupBulletinService {

    @Resource
    private PopupBulletinMapper popupBulletinMapper;

    @Resource
    private MailContentMapper mailContentMapper;

    @Resource
    private FeedbackMapper feedbackMapper;

    @Resource
    private MailUserMapper mailUserMapper;

    @Resource
    private MemberService memberService;

    @Resource
    private FeedbackService feedbackService;

    @Resource
    CustomMessageSource customMessageSource;

    @Resource
    private AdminLogService adminLogService;

    @Resource
    private AdminMemberService adminMemberService;

    private Timestamp getTimeFromString(String value) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = sdf.parse(value);
            return new Timestamp(d.getTime());
        } catch (ParseException e) {
            throw new RuntimeException("日期格式取得錯誤：" + e.getMessage(), e);
        }
    }

    public Page<PopupBulletin> getPopupBulletinAll(PopupBulletinQueryForm form) {
        PopupBulletinQueryVO vo = new PopupBulletinQueryVO();
        BeanUtils.copyProperties(form, vo);
        if (StringUtils.isNotEmpty(form.getCurrentDate())) {
            vo.setCurrentDate(getTimeFromString(form.getCurrentDate()));
        }
        if (StringUtils.isNotEmpty(form.getStartTime())) {
            vo.setStartTime(getTimeFromString(form.getStartTime()));
        }
        if (StringUtils.isNotEmpty(form.getEndTime())) {
            vo.setEndTime(getTimeFromString(form.getEndTime()));
        }
        if ("all".equalsIgnoreCase(form.getPlatform())) {
            vo.setPlatform(null);
        }
        if (form.getIsTop() != null) {
            int val = form.getIsTop().intValue();
            if (val != 0 || val != 1) {
                vo.setIsTop(null);
            }
        }
        vo.setIsDelete(BigDecimal.ZERO);
        vo.setEnable(null);
        Page<PopupBulletinQueryVO> page = new Page<PopupBulletinQueryVO>(form.getPage(), form.getPageSize());
        return popupBulletinMapper.queryList(vo, page);
    }

    //補上新版本 ＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    //後台
    public Page<PopupBulletin> getPopupBulletinAll(PopupBulletinQueryForm form) {
        QueryWrapper<PopupBulletin> qw = new QueryWrapper<>();
        if (form.getDateFrom() != null && form.getDateTo() != null) {
            qw.between("t.insertTime", form.getDateFrom(), form.getDateTo());
        }
        if (StringUtils.isNotBlank(form.getPlatform()) && !StringUtils.equalsIgnoreCase("all", form.getPlatform())) {
            qw.eq("t.platform", form.getPlatform());
        }
        if (form.getIsTop().intValue() == 0 || form.getIsTop().intValue() == 1) {
            qw.eq("t.isTop", form.getIsTop());
        }
        qw.eq("t.isDelete", BigDecimal.ZERO);
        Page<PopupBulletin> page = new Page<PopupBulletin>(form.getPage(), form.getPageSize());
        return popupBulletinMapper.getAllPopupBulletins(page, qw);
    }

    //前台 
    public Page<PopupBulletin> getPopupBulletinAll(PopupBulletinQueryForm form, LoginGamingUserDetail loginGamingUserDetail) {
        QueryWrapper<PopupBulletin> qw = new QueryWrapper<>();
        if (form.getDateFrom() != null && form.getDateTo() != null) {
            qw.between("t.insertTime", form.getDateFrom(), form.getDateTo());
        }
        if (StringUtils.isNotBlank(form.getPlatform()) && !StringUtils.equalsIgnoreCase("all", form.getPlatform())) {
            qw.eq("t.platform", form.getPlatform());
        }
        if (form.getIsTop().intValue() == 0 || form.getIsTop().intValue() == 1) {
            qw.eq("t.isTop", form.getIsTop());
        }
        qw.eq("t.isDelete", BigDecimal.ZERO);
        qw.eq("t.enable", BigDecimal.ONE);
        qw.apply(" (a.adminId is null or a.adminId = {0} ) ", loginGamingUserDetail.getMembers().getUid());// 前面會補上 and

        Page<PopupBulletin> page = new Page<PopupBulletin>(1, 9999999);
        Page<PopupBulletin> result = popupBulletinMapper.getAllPopupBulletins_formGamingSide(page, qw);

        Timestamp currenttime = new Timestamp(System.currentTimeMillis());
        for(PopupBulletin vo : result.getRecords()) {
            PopupBulletinReaded entity = new PopupBulletinReaded();
            entity.setBulletinId(vo.getId());
            entity.setAdminId(new BigDecimal(loginGamingUserDetail.getMembers().getUid()));
            entity.setInsertTime(currenttime);
            popupBulletinReadedMapper.insert(entity);
        }
        return result;
    }

    //前台 ver2
    public Page<PopupBulletin> getPopupBulletinAll(PopupBulletinQueryForm form, LoginGamingUserDetail loginGamingUserDetail) {
        QueryWrapper<PopupBulletin> qw = new QueryWrapper<>();
        if (form.getDateFrom() != null && form.getDateTo() != null) {
            qw.between("t.insertTime", form.getDateFrom(), form.getDateTo());
        }
        if (StringUtils.isNotBlank(form.getPlatform()) && !StringUtils.equalsIgnoreCase("all", form.getPlatform())) {
            qw.eq("t.platform", form.getPlatform());
        }
        if (form.getIsTop().intValue() == 0 || form.getIsTop().intValue() == 1) {
            qw.eq("t.isTop", form.getIsTop());
        }
        qw.eq("t.isDelete", BigDecimal.ZERO);
        qw.eq("t.enable", BigDecimal.ONE);
        qw.apply(" NOW() between t.startTime and t.endTime ");

        int uid = loginGamingUserDetail.getMembers().getUid();

        Page<PopupBulletin> page = new Page<PopupBulletin>(1, 9999999);
        Page<PopupBulletin> result = popupBulletinMapper.getAllPopupBulletins_formGamingSide(page, uid, qw);

        Timestamp currenttime = new Timestamp(System.currentTimeMillis());
        for (PopupBulletin vo : result.getRecords()) {
            PopupBulletinReaded entity = new PopupBulletinReaded();
            entity.setBulletinId(vo.getId());
            entity.setAdminId(new BigDecimal(loginGamingUserDetail.getMembers().getUid()));
            entity.setInsertTime(currenttime);
            popupBulletinReadedMapper.insert(entity);
        }
        return result;
    }

    public BigDecimal insertServiceNew(PopupBulletinInsertForm form, LoginAdminUserDetail admin) {
        try {
            PopupBulletin entity = new PopupBulletin();
            BeanUtils.copyProperties(form, entity);
            entity.setStartTime(getTimeFromString(form.getStartTime()));
            entity.setEndTime(getTimeFromString(form.getEndTime()));
            if (entity.getStartTime().after(entity.getEndTime())) {
                throw new BusinessException(DataConstants.CREATE_FAILED);
            }
            entity.setEnable(BigDecimal.ONE);
            entity.setIsDelete(BigDecimal.ZERO);
            entity.setInsertTime(new Timestamp(System.currentTimeMillis()));
            entity.setAdminId(new BigDecimal(admin.getUid()));
            int result = popupBulletinMapper.insertPopupBulletin(entity);
            if (result != 1) {
                throw new BusinessException(DataConstants.CREATE_FAILED);
            }
            return entity.getId();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BusinessException(DataConstants.CREATE_FAILED, ex);
        }
    }

    public BigDecimal updateServiceNew(PopupBulletinInsertForm form, LoginAdminUserDetail admin) {
        try {
            PopupBulletin entity = popupBulletinMapper.findByPk(form.getId().intValue());
            if (entity == null) {
                throw new BusinessException(DataConstants.SYSTEM_ERROR);
            }

            Timestamp insertTime = entity.getInsertTime();
            BeanUtils.copyProperties(form, entity);
            entity.setStartTime(getTimeFromString(form.getStartTime()));
            entity.setEndTime(getTimeFromString(form.getEndTime()));
            if (entity.getStartTime().after(entity.getEndTime())) {
                throw new BusinessException(DataConstants.UPDATE_FAILED);
            }
//            entity.setEnable(BigDecimal.ONE);
//            entity.setIsDelete(BigDecimal.ZERO);
            entity.setInsertTime(insertTime);
            entity.setAdminId(new BigDecimal(admin.getUid()));
            int result = popupBulletinMapper.updatePopupBulletin(entity);
            if (result != 1) {
                throw new BusinessException(DataConstants.UPDATE_FAILED);
            }
            return entity.getId();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BusinessException(DataConstants.UPDATE_FAILED, ex);
        }
    }

    public BigDecimal updateEnableNew(PopupBulletinInsertForm form, LoginAdminUserDetail admin) {
        try {
            PopupBulletin entity = popupBulletinMapper.findByPk(form.getId().intValue());
            if (entity == null) {
                throw new BusinessException(DataConstants.SYSTEM_ERROR);
            }

            entity.setEnable(form.getEnable());
            int result = popupBulletinMapper.updatePopupBulletin(entity);
            if (result != 1) {
                throw new BusinessException(DataConstants.UPDATE_FAILED);
            }
            return entity.getId();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BusinessException(DataConstants.UPDATE_FAILED, ex);
        }
    }

    public void deleteServiceNew(int id, LoginAdminUserDetail admin, HttpServletRequest request) {
//      JSONObject jsonObject = JSONObject.parseObject(jsonString);
//      int id = jsonObject.getIntValue("mailId");

        PopupBulletin entity = popupBulletinMapper.findByPk(id);
        if (entity == null) {
            throw new BusinessException(DataConstants.SYSTEM_ERROR);
        }

        entity.setIsDelete(BigDecimal.ONE);
        popupBulletinMapper.updatePopupBulletin(entity);

        AdminPermissionNew ap = adminMemberService.getAdminPermissionNewMap().get("/letter/delete");
        String adminIP = GameUtil.getIpAddr(request);
        if (StringUtils.isEmpty(adminIP)) {
            adminIP = HttpReqRespUtils.getClientIpAddressIfServletRequestExist();
        }
        adminLogService.writeLog(admin.getUid(), admin.getUsername(), 70204, id, adminIP, ap.getDescription(), "刪除彈窗公告:" + entity.getTitle());
    }

    //補上新版本 ＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    //後台
    public void deleteServiceNew(int id, LoginAdminUserDetail admin, HttpServletRequest request) {
//      JSONObject jsonObject = JSONObject.parseObject(jsonString);
//      int id = jsonObject.getIntValue("mailId");

        PopupBulletin entity = popupBulletinMapper.selectById(id);
        if (entity == null) {
            throw new BusinessException(DataConstants.SYSTEM_ERROR);
        }

        entity.setIsDelete(BigDecimal.ONE);
        QueryWrapper<PopupBulletin> qw = new QueryWrapper<>();
        qw.eq("id", entity.getId());
//            int result = popupBulletinMapper.updatePopupBulletin(entity);
        int result = popupBulletinMapper.update(entity, qw);

        AdminPermissionNew ap = adminMemberService.getAdminPermissionNewMap().get("/letter/delete");
        String adminIP = GameUtil.getIpAddr(request);
        if (StringUtils.isEmpty(adminIP)) {
            adminIP = HttpReqRespUtils.getClientIpAddressIfServletRequestExist();
        }
        adminLogService.writeLog(admin.getUid(), admin.getUsername(), 70204, id, adminIP, ap.getDescription(), "刪除彈窗公告:" + entity.getTitle());
    }



    // ==================================================================================================

    *******************************
        重要 參考 QueryWrapper
    *******************************
   public Page<MailContentVO> getLetters(StationLetterForm form) {
       QueryWrapper<MailContentVO> queryWrapper = new QueryWrapper<MailContentVO>();
       queryWrapper.eq(!StringUtils.isEmpty(form.getTitle()), "title", form.getTitle());
       queryWrapper.eq(!StringUtils.isEmpty(form.getUsername()), "username", form.getUsername());
       queryWrapper.between("actionTime", form.getDateFrom(), form.getDateTo());

       queryWrapper.groupBy("lottery_mail_content.id");
       queryWrapper.orderByDesc("lottery_mail_content.actionTime");
       Page<MailContentVO> page = new Page<MailContentVO>(form.getPage(), form.getPageSize());

       return mailContentMapper.getMailContentList(page, queryWrapper);
   }

//    public MailContentDetailVO getDetail(int id) {
//        List<MailContentDetailVO> list = mailContentMapper.getDetail(id);
//        MailContentDetailVO returnVo = new MailContentDetailVO();
//
//        if (list.size() > 0) {
//            returnVo = list.get(0);
//            if (returnVo.getToAll() == 1) returnVo.setUsername("全體");
//            else if (!StringUtils.isEmpty(returnVo.getGroupName()))
//                returnVo.setUsername("群組：" + returnVo.getGroupName());
//            else {
//                StringBuffer sb = new StringBuffer();
//                for (MailContentDetailVO mailContentDetailVO : list) {
//                    sb.append(mailContentDetailVO.getUsername()).append(",");
//                }
//                returnVo.setUsername(sb.substring(0, sb.length() - 1));
//            }
//        }
//
//        return returnVo;
//
//
////		 returnVo.setContent(mailContentDetailVO.getContent());
////		 returnVo.setTitle(mailContentDetailVO.getTitle());
////		 for (MailContentDetailVO mailContentDetailVO : list) {
////			if(mailContentDetailVO.getToAll()==1) {
////				returnVo.setUsername("全體");
////				return returnVo;
////			}
////			if
////		}
////		 return null;
//    }
//
//    public int deleteMailContent(int id) {
//        return mailContentMapper.deleteById(id);
//    }
//
//    public int deleteMailUser(int id) {
//        return mailUserMapper.deleteById(id);
//    }
//
//    public int insertMailContent(MailContent m) {
//        mailContentMapper.insertMailContent(m);
//        return m.getId();
//    }
//
//    public int insertMailUser(MailUser user) {
//        return mailUserMapper.insert(user);
//    }
//
//    public List<MailUser> getMailContent(int mailId) {
//        QueryWrapper<MailUser> queryWrapper = new QueryWrapper<MailUser>();
//        queryWrapper.eq("mailId", mailId);
//        return mailUserMapper.selectList(queryWrapper);
//    }
//
//
//    public void deleteService(int id, LoginAdminUserDetail admin, HttpServletRequest request) {
////		JSONObject jsonObject = JSONObject.parseObject(jsonString);
////		int id = jsonObject.getIntValue("mailId");
//        MailContentDetailVO detail = getDetail(id);
//        if (detail == null) throw new BusinessException(DataConstants.SYSTEM_ERROR);
//
//        // 刪除信件本身
//        deleteMailContent(id);
//        // 刪除被通知者
//        deleteMailUser(id);
//
//        AdminPermissionNew ap = adminMemberService.getAdminPermissionNewMap().get("/letter/delete");
//        String adminIP = GameUtil.getIpAddr(request);
//        if (StringUtils.isEmpty(adminIP)) adminIP = HttpReqRespUtils.getClientIpAddressIfServletRequestExist();
//        adminLogService.writeLog(admin.getUid(), admin.getUsername(), 70204, id, adminIP, ap.getDescription(), "刪除站內信:" + detail.getTitle());
//    }
//
//    @Transactional
//    public void insertService(StationLetterInsertForm form, LoginAdminUserDetail admin) {
//        int type = form.getType();
//        List<Members> userList = new ArrayList<Members>();
//        if (type == 1) { // 一般，可寄給多個user
//            List<String> mailUserNames = new ArrayList<String>();
//            mailUserNames = Arrays.asList(StringUtils.split(form.getMailTo(), ","));
//            for (String username : mailUserNames) {
//                Members members = memberService.getMemberByUserName(username);
//                if (members == null)
//                    throw new BusinessException(DataConstants.USERNAME_NOT_EXIST);
//                userList.add(members);
//            }
//        }
//
//        if (form.getFid() > 0) {
//            boolean success = feedbackService.updateFeedBack(form.getFid());
//            if (!success)
//                throw new BusinessException(DataConstants.CREATE_FAILED);
//        }
//
//        MailContent m = new MailContent();
//        m.setAdminId(admin.getUid());
//        m.setTitle(form.getTitle());
//        m.setContent(form.getContent());
//        m.setFid(form.getFid());
//
//        if (type == 1) { // insertMail
//            m.setToAll(0);
//            m.setGroup(-1);
//            m.setFid(form.getFid());
//
//            int contentId = insertMailContent(m);
//            if (contentId < 0)
//                throw new BusinessException(DataConstants.CREATE_FAILED);
//
////				List<String> mailUserNames = Arrays.asList(StringUtils.split(form.getMailTo(), ","));
//            for (Members members : userList) {
//                MailUser user = new MailUser();
//                user.setMailId(contentId);
//                user.setUserId(members.getUid());
//                user.setUsername(members.getUsername());
//                insertMailUser(user);
//            }
//
////		}else if(type==2) { //insertMailToAll
////			m.setToAll(1);
////			m.setGroup(-1);
//
//        } else { // insertMailToGroup
//            m.setToAll(0);
//            List<Integer> groups = form.getGroupId();
//            for (Integer groupid : groups) {
//                m.setGroup(groupid);
//                int contentId = insertMailContent(m);
//                if (contentId < 0)
//                    throw new BusinessException(DataConstants.CREATE_FAILED);
//            }
////			m.setGroup(form.getGroupId());
//        }
//
////		adminLogService.writeLog(admin.getUid(), admin.getUsername(), 72, contentId, GameUtil.getIPInt(form.getIp()), "新增站内信", " create new stationLetter | title:" +form.getTitle());
//
//    }
//
//    public void sendWithdrawSuccessLetter(MemberCash cash, LoginAdminUserDetail admin) {
//        Locale locale = new Locale("vi", "Vietnam");
//
//        StationLetterInsertForm form = new StationLetterInsertForm();
//        //{"type":1,"title":"title","mailTo":"allen123","content":"<p>content</p>"}
//        form.setType(1);
//        form.setTitle(customMessageSource.get(DataConstants.WITHDRAW_SUCCESS_NOTICE, locale));
//        Members member = memberService.getMemberByUid(cash.getUid());
//        form.setMailTo(member.getUsername());
//        form.setContent(customMessageSource.get(DataConstants.WITHDRAW_AMOUNT, locale) + " " + cash.getAmount().setScale(0, RoundingMode.DOWN) + " " + customMessageSource.get(DataConstants.AMOUNT_ARRIVED, locale));
//
//        insertService(form, admin);
//    }
}
