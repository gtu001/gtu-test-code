package com.tt289.data.vo;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class PopupBulletinQueryVO implements Serializable {

    private java.math.BigDecimal adminId;
    private String title;
    private String content;
    private String platform;
    private Timestamp startTime;
    private Timestamp endTime;
    private java.math.BigDecimal isTop;
    private java.math.BigDecimal isDelete;
    private java.sql.Timestamp currentDate;
    private java.math.BigDecimal enable;
}
