package com.tt289.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tt289.data.entity.PopupBulletinReaded;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author TroyChang
 * @since 2021-9-7
 */
public interface PopupBulletinReadedMapper extends BaseMapper<PopupBulletinReaded> {

}
