package com.tt289.admin.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tt289.admin.form.PopupBulletinInsertForm;
import com.tt289.admin.form.PopupBulletinQueryForm;
import com.tt289.admin.service.PopupBulletinService;
import com.tt289.common.login.LoginAdminUserDetail;
import com.tt289.data.entity.PopupBulletin;
import com.tt289.data.util.DataConstants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@Api(tags = "彈窗公告")
@RequestMapping("/popup_bulletin")
public class PopupBulletinCtrl extends BaseCtrl {
	
	@Resource
	private PopupBulletinService popupBulletinService;

	@PostMapping(value = "/list")
	@ApiOperation(value = "彈窗公告列表", response = PopupBulletin.class, //
			notes = "currentDate查詢日期（yyyy-MM-dd HH:mm:ss）\r\n" +
					"platform平台(all為全部)\r\n" +
					"isTop(1=置頂,0=非置頂,其他=all)\r\n")
	public ResponseEntity<?> getListNew(@Valid @RequestBody PopupBulletinQueryForm form, Authentication authentication) {
		Page<PopupBulletin> allBulletins = popupBulletinService.getPopupBulletinAll(form);
		return apiSuccess(DataConstants.OPERATION_SUCCESS, allBulletins);
	}

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	@ApiOperation(value = "彈窗公告新增", notes = "")
	public ResponseEntity<?> insertNew(@Valid @RequestBody PopupBulletinInsertForm form, Authentication authentication) {
		LoginAdminUserDetail admin = getAdmin(authentication);
		popupBulletinService.insertServiceNew(form, admin);
		return apiSuccess(DataConstants.CREATE_SUCCESS);
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ApiOperation(value = "彈窗公告修改", notes = "")
	public ResponseEntity<?> updateNew(@Valid @RequestBody PopupBulletinInsertForm form, Authentication authentication) {
		LoginAdminUserDetail admin = getAdmin(authentication);
		popupBulletinService.updateServiceNew(form, admin);
		return apiSuccess(DataConstants.UPDATE_SUCCESS);
	}

	@RequestMapping(value = "/enable", method = RequestMethod.POST)
	@ApiOperation(value = "彈窗公告啟用", notes = "")
	public ResponseEntity<?> enableNew(@Valid @RequestBody PopupBulletinInsertForm form, Authentication authentication) {
		LoginAdminUserDetail admin = getAdmin(authentication);
		popupBulletinService.updateEnableNew(form, admin);
		return apiSuccess(DataConstants.UPDATE_SUCCESS);
	}

	@DeleteMapping(value = "/delete/{id}")
	@ApiOperation(value = "彈窗公告刪除")
	public ResponseEntity<?> deleteNew(@PathVariable(value = "id", required = true)int id, Authentication authentication, HttpServletRequest request) {
		LoginAdminUserDetail admin = getAdmin(authentication);
		popupBulletinService.deleteServiceNew(id, admin, request);
		return apiSuccess(DataConstants.DELETE_SUCCESS);
	}

	//補上前台 ＝＝＝＝＝＝
	@PostMapping(value = "/list")
    @ApiOperation(value = "彈窗公告列表", response = PopupBulletin.class, //
            notes = "currentDate查詢日期（yyyy-MM-dd HH:mm:ss）\r\n" +
                    "platform平台(all為全部)\r\n" +
                    "isTop(1=置頂,0=非置頂,其他=all)\r\n") //
    public ResponseEntity<?> getListNew(@Valid @RequestBody PopupBulletinQueryForm form, Authentication authentication) {
        LoginGamingUserDetail loginAdminUserDetail = (LoginGamingUserDetail) authentication.getPrincipal();
        Page<PopupBulletin> allBulletins = popupBulletinService.getPopupBulletinAll(form, loginAdminUserDetail);
        return apiSuccess(DataConstants.OPERATION_SUCCESS, allBulletins);
    }

	//==========================================================================

//	@PostMapping(value = "/list_XXX")
//	@ApiOperation(value = "站內信列表", response = MailContentVO.class)
//	public ResponseEntity<?> getList(@Valid @RequestBody StationLetterForm form,Authentication authentication) {
//		Page<MailContentVO> letters = popupBulletinService.getLetters(form);
//
//		return apiSuccess(DataConstants.OPERATION_SUCCESS,letters);
//	}
//
//	@RequestMapping(value = "/detail_XXX", method = RequestMethod.POST)
//	@ApiOperation(value = "站內信查看", notes = "傳入id即可", response = MailContentDetailVO.class)
//	public ResponseEntity<?> getDetail(@RequestBody @ApiParam(name = "id", value = "ID", required = true) String jsonString, Authentication authentication) {
//		JSONObject jsonObject = JSONObject.parseObject(jsonString);
//
//		MailContentDetailVO detail = popupBulletinService.getDetail(jsonObject.getIntValue("id"));
//		if(detail==null) return apiError(DataConstants.PARAMETER_EXCEPTION);
//
//		return apiSuccess(DataConstants.OPERATION_SUCCESS,detail);
//	}
//
//	@DeleteMapping(value = "/delete_XXX/{id}")
//	@ApiOperation(value = "站內信刪除")
//	public ResponseEntity<?> delete(@PathVariable(value = "id", required = true)int id, Authentication authentication, HttpServletRequest request) {
//		LoginAdminUserDetail admin = getAdmin(authentication);
//		popupBulletinService.deleteService(id, admin, request);
//		return apiSuccess(DataConstants.DELETE_SUCCESS);
//	}
//
//	@RequestMapping(value = "/insert_XXX", method = RequestMethod.POST)
//	@ApiOperation(value = "站內信新增", notes = "")
//	public ResponseEntity<?> insert(@Valid @RequestBody StationLetterInsertForm form, Authentication authentication) {
//
//		LoginAdminUserDetail admin = getAdmin(authentication);
//		popupBulletinService.insertService(form, admin);
//		return apiSuccess(DataConstants.CREATE_SUCCESS);
//
//	}
//
//	@PostMapping(value = "/receiver_XXX")
//	@ApiOperation(value = "收件人查詢",notes = "傳入mailId", response = MailUser.class)
//	public ResponseEntity<?> getMailContent(@RequestBody @ApiParam(name = "mailId", value = "ID", required = true) String jsonString) {
//		JSONObject jsonObject = JSONObject.parseObject(jsonString);
//		int mailId = jsonObject.getIntValue("mailId");
//		List<MailUser> mailContent = popupBulletinService.getMailContent(mailId);
//		return apiSuccess(0,mailContent);
//	}
	
}
