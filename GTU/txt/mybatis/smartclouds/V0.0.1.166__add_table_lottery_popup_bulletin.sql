use hh892;
start transaction ;

DROP TABLE IF EXISTS `lottery_popup_bulletin`;
CREATE TABLE `lottery_popup_bulletin` (
                                                  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'bulletin id',
                                                  `adminId` int(10) NOT NULL COMMENT 'create bulletin admin uid',
                                                  `title` varchar(256)  NOT NULL,
                                                  `content` mediumtext,
                                                  `platform` varchar(20) DEFAULT 'ALL' NOT NULL,
                                                  `insertTime` datetime DEFAULT CURRENT_TIMESTAMP,
                                                  `startTime` datetime,
                                                  `endTime` datetime,
                                                  `enable` tinyint(8) DEFAULT '1',
                                                  `isTop` smallint(3) DEFAULT '0' COMMENT '1=置頂',
                                                  `isDelete` smallint(3) DEFAULT '0' COMMENT '1=刪除',
                                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
;

DROP TABLE IF EXISTS `lottery_popup_bulletin_readed`;
CREATE TABLE `lottery_popup_bulletin_readed` (
                                                 `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
                                                 `bulletin_id` int(10) NOT NULL  COMMENT 'bulletin id',
                                                 `adminId` int(10) NOT NULL COMMENT 'create bulletin admin uid',
                                                 `insertTime` datetime DEFAULT CURRENT_TIMESTAMP,
                                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8
;

commit ;

