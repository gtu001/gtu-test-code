package com.tt289.admin.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ApiModel(value = "PopupBulletinInsertForm", description = "修改機率的表單")
public class PopupBulletinInsertForm extends BaseForm implements Serializable {

//    @NotNull(message = "id不可空白")
    @ApiModelProperty(value="id", name="id", dataType="java.math.BigDecimal", example="632")
    private java.math.BigDecimal id;

    @ApiModelProperty(value="帳號", name="adminId", dataType="java.math.BigDecimal", example="111")
    private java.math.BigDecimal adminId;

    @NotNull(message = "title不可空白")
    @ApiModelProperty(value="標題", name="title", dataType="String", example="test1", required = true)
    private String title;

    @ApiModelProperty(value="內文", name="content", dataType="String", example="test_content")
    private String content;

    @NotNull(message = "platform不可空白")
    @ApiModelProperty(value="平台", name="platform", dataType="String", example="ALL", required = true)
    private String platform;

//    @ApiModelProperty(value="insertTime", name="insertTime", dataType="java.sql.Timestamp", example="2021-09-01 17:56:23.0")
//    private java.sql.Timestamp insertTime;

    @ApiModelProperty(value="開始時間", name="startTime", dataType="String", example="2021-09-01 17:56:23")
    private String startTime;

    @ApiModelProperty(value="結束時間", name="endTime", dataType="String", example="2021-09-15 17:56:23")
    private String endTime;

//    @ApiModelProperty(value="enable", name="enable", dataType="java.math.BigDecimal", example="1")
//    private java.math.BigDecimal enable;

    @ApiModelProperty(value="置頂", name="isTop", dataType="java.math.BigDecimal", example="1")
    private java.math.BigDecimal isTop;

//    @ApiModelProperty(value="isDelete", name="isDelete", dataType="java.math.BigDecimal", example="0")
//    private java.math.BigDecimal isDelete;
}
