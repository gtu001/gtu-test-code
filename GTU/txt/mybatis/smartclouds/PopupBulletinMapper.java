package com.tt289.data.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tt289.data.entity.PopupBulletin;
import com.tt289.data.vo.PopupBulletinQueryVO;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author TroyChang
 * @since 2021-9-1
 */
public interface PopupBulletinMapper extends BaseMapper<PopupBulletin> {

	@Select("select * " +
			"from lottery_popup_bulletin t " +
			"  ${ew.customSqlSegment} ")
	public Page<PopupBulletin> getAllPopupBulletins(Page<PopupBulletin> page, //
													@Param("ew") QueryWrapper<PopupBulletin> queryWrapper);//

	@Select("             select * from lottery_popup_bulletin t                             \n" + //
            "             where t.id in (                                                    \n" + //
            "                 select t.id                                                        \n" +//
            "                 from lottery_popup_bulletin t                                      \n" +//
            "                 left join lottery_popup_bulletin_readed a on t.id = a.bulletin_id  \n" +//
            "                 ${ew.customSqlSegment}                                             \n" +//
            "             )                                                                      \n" + //
            "")
    public Page<PopupBulletin> getAllPopupBulletins_formGamingSide(Page<PopupBulletin> page, //
                                                                   @Param("ew") QueryWrapper<PopupBulletin> queryWrapper);

    // 新版本
    @Select("             SELECT *                                          \n"+//
            "             FROM  lottery_popup_bulletin t                    \n"+//
            "                 ${ew.customSqlSegment}                           \n"+//
            "                 and t.id NOT IN (                               \n"+//
            "                     select g.bulletin_id                      \n"+//
            "                     from lottery_popup_bulletin_readed g      \n"+//
            "                     where 1=1                                 \n"+//
            "                      and g.adminId = #{adminId}               \n"+//
            "                     )                                         \n"+//
            "             order by t.isTop desc, t.insertTime desc          \n"+//
            "")
    public Page<PopupBulletin> getAllPopupBulletins_formGamingSide(Page<PopupBulletin> page, @Param("adminId") int adminId, //
                                                                   @Param("ew") QueryWrapper<PopupBulletin> queryWrapper);


	@SelectKey(statement = "SELECT LAST_INSERT_ID()", resultType = BigDecimal.class, keyProperty = "id", before = false)
	@Insert("insert into lottery_popup_bulletin \n" +
			"(id,adminId,title,content,platform,insertTime,startTime,endTime,enable,isTop,isDelete) values \n" +
			"(#{id},#{adminId},#{title},#{content},#{platform},#{insertTime},#{startTime},#{endTime},#{enable},#{isTop},#{isDelete})")
	public int insertPopupBulletin(PopupBulletin m);

	@Select("SELECT * \r\n" +
			"FROM lottery_popup_bulletin t \r\n" +
			"WHERE t.id = #{id}")
	public PopupBulletin findByPk(@Param("id") int id);

	public Page<PopupBulletin> queryList(@Param("vo")PopupBulletinQueryVO vo, Page<PopupBulletinQueryVO> page);


	@Update("update lottery_popup_bulletin set \n" +
			"adminId = #{adminId}, \n" +
			"title = #{title}, \n" +
			"content = #{content}, \n" +
			"platform = #{platform}, \n" +
			"insertTime = #{insertTime}, \n" +
			"startTime = #{startTime}, \n" +
			"endTime = #{endTime}, \n" +
			"enable = #{enable}, \n" +
			"isTop = #{isTop}, \n" +
			"isDelete = #{isDelete} \n" +
			" where 1=1 \n" +
			" and id = #{id} \n")
	public int updatePopupBulletin(PopupBulletin m);


	//=======================================================================================

	@Select("SELECT " + " lottery_mail_content.id, " + " lottery_mail_content.title, "
			+ " lottery_mail_content.actionTime, " + " lottery_mail_content.id AS mailId, "
			+ "  (SELECT COUNT(id) FROM lottery_mail_user WHERE mailId = lottery_mail_content.id AND STATUS IN (1,2)) AS readCount, "
			+ "   IFNULL (IF(lottery_mail_content.group != -1, bb.groupcount, aa.usercount),'0') as sentCount "
			+ "    FROM lottery_mail_content  "
			+ "    LEFT JOIN lottery_mail_user ON lottery_mail_content.id = lottery_mail_user.mailId  "
			+ "    LEFT JOIN  " + "(SELECT lottery_mail_content.id, COUNT(*) usercount "
			+ "  FROM lottery_mail_content "
			+ "  JOIN lottery_mail_user ON lottery_mail_content.id = lottery_mail_user.mailid "
			+ "  GROUP BY lottery_mail_content.id " + ") aa ON aa.id = lottery_mail_content.id " + " LEFT JOIN "
			+ "(SELECT lottery_mail_content.id, COUNT(*) groupcount " + "  FROM lottery_mail_content "
			+ "  JOIN lottery_members ON lottery_mail_content.group = lottery_members.groupid "
			+ "  GROUP BY lottery_mail_content.id " + ") bb ON bb.id = lottery_mail_content.id ${ew.customSqlSegment}")

	public Page<MailContentVO> getMailContentList(Page<MailContentVO> page,
			@Param("ew") QueryWrapper<MailContentVO> queryWrapper);

	@Select("SELECT username,title, content, toAll," +
			"  IF(`group`=-1,'', lottery_member_group.`name`) AS groupname\r\n" +
			"FROM lottery_mail_content \r\n" +
			"  LEFT JOIN lottery_mail_user \r\n" +
			"    ON lottery_mail_user.`mailId` = lottery_mail_content.id \r\n" +
			"  LEFT JOIN lottery_member_group\r\n" +
			"	ON lottery_mail_content.`group`= lottery_member_group.id\r\n" +
			"WHERE lottery_mail_content.id = #{id}")
	public List<MailContentDetailVO> getDetail(@Param("id") int id);

	@SelectKey(statement = "SELECT LAST_INSERT_ID()", resultType = Integer.class, keyProperty = "id", before = false)
	@Insert("insert into lottery_mail_content(adminId,title,content,toAll,`group`,fid) values(#{adminId},#{title},#{content},#{toAll},#{group},#{fid})")
	public int insertMailContent(MailContent m);

	@Select("select a.id,a.mailId,a.userId,a.username,a.status,b.actionTime,b.title,b.content from lottery_mail_user a "
			+ "LEFT JOIN lottery_mail_content b ON a.mailId = b.id where a.userId = #{uid} and status in (0, 1) "
			+ "union all select 0 as id,c.mailId,#{uid} as userId,#{username} as username,0 as status,"
			+ "c.actionTime,c.title,c.content from (select b.id as mailId,b.actionTime,b.title,b.toAll,b.content "
			+ "from lottery_mail_content b where (toAll = 1 and `group` = -1) or ( toAll = 0 and `group` = #{groupId})) c "
			+ "where c.mailId not in (select mailId from lottery_mail_user where status IN (1,2) && userId = #{uid} ) order by actionTime desc")
	Page<MailListVo> getMailList(Page<MailListVo> page, int uid, int groupId, String username);

	@Select("select adminId,title,content,actionTime,#{id} as id,toAll,`group` from lottery_mail_content WHERE id = #{id}")
	MailDetailVo getMailDetail(int id);
}
