mybatis_mapper_java_使用方式.md
---

mapper.xml
===

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.tt289.data.mapper.PopupBulletinMapper">

    <resultMap id="resultMap" type="com.tt289.data.entity.PopupBulletin">
    </resultMap>

    <!--com.tt289.data.vo.PopupBulletinQueryVO-->
    <select id="queryList" parameterType="com.tt289.data.vo.PopupBulletinQueryVO" resultMap="resultMap">
        SELECT *
        FROM lottery_popup_bulletin t
        <where>
            <if test="_parameter.vo.currentDate != null">
                and #{_parameter.vo.currentDate} between t.startTime and t.endTime
            </if>
        </where>
    </select>
</mapper>



對應mapper java
===
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.tt289.data.entity.MailContent;
import com.tt289.data.entity.PopupBulletin;
import com.tt289.data.vo.*;
import org.apache.ibatis.annotations.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author TroyChang
 * @since 2021-9-1
 */
public interface PopupBulletinMapper extends BaseMapper<PopupBulletin> {

	@Select("select * " +
			"from lottery_popup_bulletin t " +
			"where 1=1 ")
	public Page<PopupBulletin> getAllPopupBulletins(Page<PopupBulletin> page, //
													@Param("ew") QueryWrapper<PopupBulletin> queryWrapper);//

	@SelectKey(statement = "SELECT LAST_INSERT_ID()", resultType = BigDecimal.class, keyProperty = "id", before = false)
	@Insert("insert into lottery_popup_bulletin \n" +
			"(id,adminId,title,content,platform,insertTime,startTime,endTime,enable,isTop,isDelete) values \n" +
			"(#{id},#{adminId},#{title},#{content},#{platform},#{insertTime},#{startTime},#{endTime},#{enable},#{isTop},#{isDelete})")
	public int insertPopupBulletin(PopupBulletin m);

	@Select("SELECT * \r\n" +
			"FROM lottery_popup_bulletin t \r\n" +
			"WHERE t.id = #{id}")
	public PopupBulletin findByPk(@Param("id") int id);

	********************************************************
		注意 參數使用方式 這個是對應 mapper xml
	********************************************************
	public Page<PopupBulletin> queryList(@Param("vo")PopupBulletinQueryVO vo, Page<PopupBulletinQueryVO> page);


	@Update("update lottery_popup_bulletin set \n" +
			"adminId = #{adminId}, \n" +
			"title = #{title}, \n" +
			"content = #{content}, \n" +
			"platform = #{platform}, \n" +
			"insertTime = #{insertTime}, \n" +
			"startTime = #{startTime}, \n" +
			"endTime = #{endTime}, \n" +
			"enable = #{enable}, \n" +
			"isTop = #{isTop}, \n" +
			"isDelete = #{isDelete} \n" +
			" where 1=1 \n" +
			" and id = #{id} \n")
	public int updatePopupBulletin(PopupBulletin m);
}
