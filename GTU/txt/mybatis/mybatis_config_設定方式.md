mybatis_config_設定方式.md
---



<dependency>
	<groupId>com.baomidou</groupId>
	<artifactId>dynamic-datasource-spring-boot-starter</artifactId>
	<version>3.1.1</version>
</dependency>




# master
spring.datasource.dynamic.datasource.master.driver-class-name=com.mysql.jdbc.Driver

spring.datasource.dynamic.datasource.master.url=jdbc:mysql://localhost:3306/test?useSSL=false&serverTimezone=GMT%2B8&characterEncoding=UTF-8

spring.datasource.dynamic.datasource.master.username=root

spring.datasource.dynamic.datasource.master.password=123456



# slave
spring.datasource.dynamic.datasource.slave.driver-class-name=com.mysql.jdbc.Driver

spring.datasource.dynamic.datasource.slave.url=jdbc:mysql://localhost:3306/test1?useSSL=false&serverTimezone=GMT%2B8&characterEncoding=UTF-8

spring.datasource.dynamic.datasource.slave.username=root

spring.datasource.dynamic.datasource.slave.password=123456





========

spring.datasource.dynamic.datasource.master.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.dynamic.datasource.master.url=jdbc:mysql://34.87.104.35:3306/hh892?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull
spring.datasource.dynamic.datasource.master.username=root
spring.datasource.dynamic.datasource.master.password=s7-M.xCj78xRsUqr8u.TjiBG


