tomcat_使用_https.md

step1
	JAVA_HOME\bin> keytool -genkey -alias tomcat -keyalg RSA

	弄完產生檔案於 ‪C:\Users\gtu00\.keystore

step2
	編輯 tomcat/conf/server.xml
	加入
	<!-- Define an SSL Coyote HTTP/1.1 Connector on port 8443 -->
	<Connector
           protocol="org.apache.coyote.http11.Http11NioProtocol"
           port="8443" maxThreads="200"
           scheme="https" secure="true" SSLEnabled="true"
           keystoreFile="${user.home}/.keystore" keystorePass="gtu001"
           clientAuth="false" sslProtocol="TLS"/>
    keystorePass 等於你 step1 設定的值

step3:
	WEB-INF/web.xml
	加入
	<security-constraint>
		<web-resource-collection>
			<web-resource-name>NgrokServlet_https</web-resource-name>
			<url-pattern>/NgrokServlet</url-pattern>
		</web-resource-collection>
		<user-data-constraint>
			<transport-guarantee>CONFIDENTIAL</transport-guarantee>
		</user-data-constraint>
	</security-constraint>
	


step4:
	若原本為 http://localhost:8080/FoodMenu/NgrokServlet
	要改為 https://localhost:8443/FoodMenu/NgrokServlet
	首頁會變成 https://localhost:8443/FoodMenu/
