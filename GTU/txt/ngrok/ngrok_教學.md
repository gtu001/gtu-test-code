設定檔位置
	C:\Users\gtu00\.ngrok2\ngrok.yml

設定文黨
	https://ngrok.com/docs

執行指令 
	ngrok start demo


Webhook 網址設定
	如果你 web為 http://localhost:8080/myapp
	那你 webhook 網址為 http://54de4053.ngrok.io/myapp

	Ex : http://localhost:8080/FoodMenu/NgrokServlet
		 https://55117b22c275.ngrok.io/FoodMenu/NgrokServlet



ngrok.yml
-----------
authtoken: 1umrm5aybpIAG2qf9PNHN9PYYKb_3syqwj1cLSx5yrfouqo3
region: us
console_ui: true
http_proxy: false
inspect_db_size: 50000000
log_level: info
log_format: term #json
log: C:\Users\gtu00\.ngrok2\ngrok.log
matadata: '{"serial": "00012xa", "comment":"For customer gtu001@gmail.com"}'
root_cas: trusted
#socks5_proxy: "socks5://localhost:9150"
update: true
update_channel: stable
web_addr: localhost:4040

tunnels:
  demo:
    proto: http
    addr: 8080
    #subdomain: gtu001
    #hostname: localhost
    inspect: false
    #auth: "gtu001:1234"
    bind_tls: true
    host_header: "myapp.dev"

  e2etls:
    addr: 9000
    proto: tls
    hostname: myapp.example.com
    #crt: example.crt
    #key: example.key

  ssh-access:
    addr: 22
    proto: tcp
    remote_addr: 1.tcp.ngrok.io:12345