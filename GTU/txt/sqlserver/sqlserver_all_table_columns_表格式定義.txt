
select 
	TABLE_CATALOG + '.' + TABLE_SCHEMA + '.' + TABLE_NAME,
	COLUMN_NAME,
	IS_NULLABLE,
	DATA_TYPE,
	CHARACTER_MAXIMUM_LENGTH,
	NUMERIC_PRECISION,
	COLUMN_DEFAULT,
	ORDINAL_POSITION
from information_schema.columns t 
where 1=1
[ and lower(t.TABLE_NAME) like '%' + lower(:tableName_like) + '%']
[ and lower(t.TABLE_NAME) = lower(:tableName_eq) ]
[ and lower(t.COLUMN_NAME) like '%' + lower(:columnName_like) + '%']
[ and lower(t.COLUMN_NAME) = lower(:columnName_eq) ]