PowerShell_PSSecurityException_about_Execution_Policies_修改.md

    Get-ExecutionPolicy
    	取得當前政策
    Get-ExecutionPolicy -List
    	列出所有Scope政策
    Get-ExecutionPolicy -Scope CurrentUser
    	抓當前使用者政策


    ---
    政策有以下三種
	    Unrestricted : 允許所有scripts 	
		RemoteSigned : 允許本地和遠端簽屬scripts 	
		AllSigned : 只允許簽署scripts 	

    設定全局政策
	    Set-ExecutionPolicy -ExecutionPolicy  Unrestricted
	設定政策對針對當前使用者
	    Set-ExecutionPolicy -ExecutionPolicy  Undefined     -Scope CurrentUser