<#import "/lib.ftl" as my>  



<#function getColumnDef(col2)>
	<#assign rtnObj = '' />
    <#list columnsDef as map>
    	<#list map?keys as key>
	        <#if key == col2>
	            <#assign rtnObj = map[key] />
	        </#if>
	    </#list>
    </#list>
    <#return rtnObj>
</#function>

<#function getColumnJava(col2)>
	<#assign rtnObj = '' />
    <#list columnsJava as map>
    	<#list map?keys as key>
	        <#if key == col2>
	            <#assign rtnObj = map[key] />
	        </#if>
	    </#list>
    </#list>
    <#return rtnObj>
</#function>

<#function isPK(col2)>
	<#assign rtnObj = false />
    <#list pk as ppk>
        <#if ppk == col2>
            <#assign rtnObj = true />
        </#if>
    </#list>
    <#return rtnObj>
</#function>

<#function getColParamArgsLst>
    <#local rtn = "">
    <#local lst = []>
    <#list columns as col>
		<#assign varData = "${col}" />
        <#local lst = lst + [ varData ]>
    </#list>
    <#local rtn = my.listJoin(lst, "  ")>
    <#return rtn>
</#function>

<#list columns as col>
	${col}		${getColumnDef(col)}		${getColumnJava(col)}		${isPK(col)?string}
</#list>



${getColParamArgsLst()}