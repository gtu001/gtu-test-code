package com.example.englishtester.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import com.example.englishtester.MainActivity;
import com.example.englishtester.common.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gtu001 on 2017/6/27.
 */

public class PermissionUtil {

    private static final String TAG = PermissionUtil.class.getSimpleName();

    /**
     * 檢查Permission是否可用
     */
    public static boolean checkPermission(Context context, String permission) {
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static boolean checkPermissionWriteExternalStorage(Context context) {
        PackageManager pm = context.getPackageManager();
        int hasPerm = pm.checkPermission(
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                context.getPackageName());
        if (hasPerm == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static boolean verifyAllPermissions(Activity activity, String[] permissions) {
        // Check if we have write permission
        List<Integer> permissionLst = new ArrayList<Integer>();
        List<String> permissionReqLst = new ArrayList<String>();
        for (String permission : permissions) {
            int val = ActivityCompat.checkSelfPermission(activity, permission);
            permissionLst.add(val);
            if (val != PackageManager.PERMISSION_GRANTED) {
                permissionReqLst.add(permission);
            }
            Log.v(TAG, " permission " + permission + " ------- " + val + " / " + (PackageManager.PERMISSION_GRANTED == val));
        }
        int REQUEST_EXTERNAL_STORAGE = 1;
        if (!permissionReqLst.isEmpty()) {
            // We don't have permission so prompt the user
            Log.v(TAG, "Go request permission ------- " + permissionReqLst);
            ActivityCompat.requestPermissions(
                    activity,
                    permissionReqLst.toArray(new String[0]),
                    REQUEST_EXTERNAL_STORAGE
            );
            return true;
        }
        return false;
    }

    public static void verifyWriteSettingsPermission(Activity context, int requestCode) {
        boolean permission;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permission = Settings.System.canWrite(context);
        } else {
            permission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_SETTINGS) == PackageManager.PERMISSION_GRANTED;
        }
        if (permission) {
            //do your code
        } else {
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + context.getPackageName()));
                context.startActivityForResult(intent, requestCode);
//            } else {
//                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.WRITE_SETTINGS}, requestCode);
//            }
        }
    }
}
