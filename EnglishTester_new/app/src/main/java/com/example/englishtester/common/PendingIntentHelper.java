package com.example.englishtester.common;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import com.example.englishtester.R;
import com.example.englishtester.TxtReaderActivity;

import java.util.Random;

public class PendingIntentHelper {

    private static final String TAG = PendingIntentHelper.class.getSimpleName();

    public static void sendIntent(Intent intent, String title, String text, Context context) {
        PendingIntent pi = PendingIntent.getActivity(context, new Random().nextInt(), intent, PendingIntent.FLAG_UPDATE_CURRENT);//getBroadcast
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification.Builder notif = new Notification.Builder(context);
        notif.setSmallIcon(R.mipmap.ic_launcher);
        int notificationId = new Random().nextInt();
        notif.setContentTitle(title);
        notif.setContentText(text);
        Uri path = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notif.setSound(path);
        notif.setContentIntent(pi);
        notif.setVibrate(new long[]{1000, 1000});
        notif.setAutoCancel(true);
        notif.setOnlyAlertOnce(true);
        notif.setOngoing(true);

        nm.notify(notificationId, notif.build());
    }

    public static void sendIntent2(Intent intent, Context context) {
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        long milis = 0;
        manager.set(AlarmManager.RTC_WAKEUP, milis, pi);
    }
}
