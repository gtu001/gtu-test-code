package com.example.englishtester;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.englishtester.common.Log;
import com.example.englishtester.common.pdf.base.PdfViewerMainHandler;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecentBookHistoryActivity extends ListActivity {

    private static final String TAG = RecentBookHistoryActivity.class.getSimpleName();

    private RecentBookHistoryService mRecentBookHistoryService;
    private List<Map<String, Object>> pathList;

    private EditText searchTextView;
    private TextView filePathLabel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        System.out.println("# onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recent_book_list);

        searchTextView = (EditText) findViewById(R.id.searchTextView);
        filePathLabel = (TextView) findViewById(R.id.filePathLabel);

        initServices();

        initListView();
    }

    private void initServices() {
        mRecentBookHistoryService = new RecentBookHistoryService(this);
    }

    private void initListView() {
        pathList = new ArrayList<>();

        List<RecentBookOpenHistoryDAO.RecentBookOpenHistory> lst = mRecentBookHistoryService.queryBookList();
        if (lst == null) {
            lst = new ArrayList<>();
        }

        for (RecentBookOpenHistoryDAO.RecentBookOpenHistory vo : lst) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("file_name", vo.bookName);
            map.put("file_detail", "次數：" + vo.openTimes + ",時間：" + DateFormatUtils.format(vo.latestOpenDatetime, "yyyy/MM/dd HH:mm:ss"));
            map.put("vo", vo);
            pathList.add(map);
        }

        SimpleAdapter fileList = new SimpleAdapter(this, pathList,// 資料來源
                R.layout.subview_propview_subdetail, //
                new String[]{"icon", "file_name", "file_detail"}, //
                new int[]{R.id.ItemImage, R.id.ItemTitle, R.id.ItemDetail});

        setListAdapter(fileList);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        RecentBookOpenHistoryDAO.RecentBookOpenHistory vo = (RecentBookOpenHistoryDAO.RecentBookOpenHistory) pathList.get(position).get("vo");
        openFileSpec(vo.filePath, vo.subName);
    }

    public void openFileSpec(String filePath, String subName) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(RecentBookHistoryService.RECENT_OPEN_BOOK, filePath);
        switch (StringUtils.trimToEmpty(subName)) {
            case "epub":
                intent.setClass(RecentBookHistoryActivity.this, EpubReaderEpubActivity.class);
                break;
            case "pdf":
                intent.setClass(RecentBookHistoryActivity.this, PdfReaderPdfActivity.class);
                break;
            case "mobi":
                intent.setClass(RecentBookHistoryActivity.this, MobiReaderMobiActivity.class);
                break;
            default:
                intent.setClass(RecentBookHistoryActivity.this, TxtReaderBufferActivity.class);
                break;
        }
        intent.putExtras(bundle);
        startActivityForResult(intent, 9999);
    }

    // --------------------------------------------------------------------

    static int REQUEST_CODE = 5566;
    static int MENU_FIRST = Menu.FIRST;

    enum TaskInfo {
        LOAD_CONTENT_FROM_FILE_RANDOM("開啟電子檔(epub|pef|mobi|txt)", MENU_FIRST++, REQUEST_CODE++, FileFind4EpubActivity.class) {
            protected void onActivityResult(RecentBookHistoryActivity activity, Intent intent, Bundle bundle) {
                File file = FileFind4EpubActivity.FileFindActivityStarter.getFile(intent);
                String path = file.getAbsolutePath();
                String subName = StringUtils.substring(path, StringUtils.lastIndexOf(path, ".") + 1);
                activity.openFileSpec(path, subName);
            }

            protected void onOptionsItemSelected(RecentBookHistoryActivity activity, Intent intent, Bundle bundle) {
                bundle.putString(FileFind4EpubActivity.FILE_PATTERN_KEY, "(epub|pdf|mobi|txt)");
                if (BuildConfig.DEBUG) {
                    bundle.putStringArray(FileFind4EpubActivity.FILE_START_DIRS, new String[]{"/storage/1D0E-2671/Android/data/com.ghisler.android.TotalCommander/My Documents/"});
                }
                super.onOptionsItemSelected(activity, intent, bundle);
            }
        }, //
        ;

        final String title;
        final int option;
        final int requestCode;
        final Class<?> clz;
        final boolean debugOnly;

        TaskInfo(String title, int option, int requestCode, Class<?> clz) {
            this(title, option, requestCode, clz, false);
        }

        TaskInfo(String title, int option, int requestCode, Class<?> clz, boolean debugOnly) {
            this.title = title;
            this.option = option;
            this.requestCode = requestCode;
            this.clz = clz;
            this.debugOnly = debugOnly;
        }

        protected void onOptionsItemSelected(RecentBookHistoryActivity activity, Intent intent, Bundle bundle) {
            intent.setClass(activity, clz);
            intent.putExtras(bundle);
            activity.startActivityForResult(intent, requestCode);
        }

        protected void onActivityResult(RecentBookHistoryActivity activity, Intent intent, Bundle bundle) {
            Log.v(TAG, "onActivityResult TODO!! = " + this.name());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v(TAG, "# onOptionsItemSelected");
        super.onOptionsItemSelected(item);

        //預處理
        if (true) {
            //TODO
        }

        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        for (RecentBookHistoryActivity.TaskInfo task : RecentBookHistoryActivity.TaskInfo.values()) {
            if (item.getItemId() == task.option) {
                task.onOptionsItemSelected(this, intent, bundle);
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v(TAG, "# onActivityResult");
        Bundle bundle_ = new Bundle();
        if (data != null) {
            bundle_ = data.getExtras();
        }
        final Bundle bundle = bundle_;
        Log.v(TAG, "requestCode = " + requestCode);
        Log.v(TAG, "resultCode = " + resultCode);
        for (RecentBookHistoryActivity.TaskInfo t : RecentBookHistoryActivity.TaskInfo.values()) {
            if (requestCode == t.requestCode) {
                switch (resultCode) {
                    case RESULT_OK:
                        t.onActivityResult(this, data, bundle);
                        break;
                }
                break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.v(TAG, "# onCreateOptionsMenu");
        for (RecentBookHistoryActivity.TaskInfo e : RecentBookHistoryActivity.TaskInfo.values()) {

            //純測試
            if (!BuildConfig.DEBUG && e.debugOnly == true) {
                continue;
            }

            menu.add(0, e.option, 0, e.title);
        }
        return true;
    }
}
