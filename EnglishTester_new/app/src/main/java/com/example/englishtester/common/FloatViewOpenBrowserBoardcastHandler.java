package com.example.englishtester.common;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.example.englishtester.FloatViewService;
import com.example.englishtester.MainActivity;

public class FloatViewOpenBrowserBoardcastHandler {

    private static final String TAG = FloatViewOpenBrowserBoardcastHandler.class.getSimpleName();

    public static final String OPEN_BROWSER = FloatViewOpenBrowserBoardcastHandler.class.getName() + ".open_browser";
    public static final String OPEN_GOOGLE_SEARCH = FloatViewOpenBrowserBoardcastHandler.class.getName() + ".open_google_search";

    private static FloatViewOpenBrowserBoardcastHandler _INST = new FloatViewOpenBrowserBoardcastHandler();
    private Activity activityHolder;

    private FloatViewOpenBrowserBoardcastHandler() {
    }

    public static FloatViewOpenBrowserBoardcastHandler getInstance() {
        return _INST;
    }

    public void onStart(Activity activity) {
        IntentFilter f = new IntentFilter(OPEN_GOOGLE_SEARCH);
        f.addAction(OPEN_BROWSER);
//        LocalBroadcastManager.getInstance(activity)
//                .registerReceiver(onEvent, f);
        activity.registerReceiver(onEvent, f);
        activityHolder = activity;
    }

    public void onStop(Activity activity) {
//        LocalBroadcastManager.getInstance(activity)
//                .unregisterReceiver(onEvent);
        activity.unregisterReceiver(onEvent);
    }

    private BroadcastReceiver onEvent = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent i) {
            String url = i.getStringExtra("content");
            Log.v(TAG, "===================================================");
            Log.v(TAG, "action : " + i.getAction());
            Log.v(TAG, "content : " + url);
            Log.v(TAG, "activity : " + activityHolder);
            Log.v(TAG, "===================================================");
            if (OPEN_BROWSER.equals(i.getAction())) {
                openMainProgram(activityHolder);
                GoogleSearchHandler.openWithBrowser(url, activityHolder);
            }
            if (OPEN_GOOGLE_SEARCH.equals(i.getAction())) {
                openMainProgram(activityHolder);
                GoogleSearchHandler.search(activityHolder, url);
            }
        }
    };

    //==================================================================================

    public static void openBrowser(String url, Context context) {
        Intent intent = new Intent(OPEN_BROWSER);
        intent.putExtra("content", url);
//        LocalBroadcastManager.getInstance(context)
//                .sendBroadcast(intent);
        context.sendBroadcast(intent);
        PendingIntentHelper.sendIntent(intent, "查詢字典", url, context);
    }

    public static void openGoogleSearch(String content, Context context) {
        Intent intent = new Intent(OPEN_GOOGLE_SEARCH);
        intent.putExtra("content", content);
//        LocalBroadcastManager.getInstance(context)
//                .sendBroadcast(intent);
        context.sendBroadcast(intent);
        PendingIntentHelper.sendIntent(intent, "查詢字典", content, context);
    }

    /**
     * 開啟主程式
     */
    private void openMainProgram(Context context) {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setClass(context, MainActivity.class);
        Bundle bundle = new Bundle();
        intent.putExtras(bundle);
        context.startActivity(intent);
        PendingIntentHelper.sendIntent(intent, "查詢字典", "開啟主程式", context);
    }
}
